<?php

namespace App\Http\Controllers;

use Alchemy\Zippy\Zippy;
use App\Models\BaiViet;
use App\Models\ChiTietHoaDon;
use App\Models\HauMai;
use App\Models\Helps;
use App\Models\Hinh;
use App\Models\HoaDon;
use App\Models\LoaiSanPham;
use App\Models\SanPham;
use App\Models\TaiKhoan;
use App\Models\Theme;
use App\Service\FileUploader;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Session;

require __DIR__ . '/../../../vendor/autoload.php';

/**
 * Class HomeController
 * @package App\Http\Controllers
 */
class HomeController extends BaseController
{
    /**
     * @var FileUploader
     */
    private $fileUploader;

    /**
     * @var string
     */
    private $urlBase;

    const NUMBERS_OPTIMIZE = 20;

    public function __construct(FileUploader $fileUploader, Request $request)
    {
        $referer = app('Illuminate\Routing\UrlGenerator')->previous();
        $refererShort = last(explode('/', $referer));
        $this->urlBase = URL::to('/');
        $USER_INFO = Session::get('USER_INFO', []);
        if (empty($USER_INFO) && $refererShort != 'admin' && explode("?", $request->getRequestUri())[0] != '/optimze-imgs-all') {
            if ($request->header('x-requested-with', null) != 'XMLHttpRequest' ||
                $request->getRequestUri() != '/authen') {
                echo '<h1 style="color: red">Not authorized.</h1>';
                die;
            }
        }

        parent::__construct();
        $this->fileUploader = $fileUploader;
    }

    public function loaiSanPham(Request $request)
    {
        $this->fileUploader->setTargetDir('images/category');
        $MD = new LoaiSanPham();
        $id = $request->get('id');
        switch ($request->method()) {
            case 'GET':
                if (empty($id)) {
                    $this->data = $MD->get(['isdeleted' => 0, ['id', '<>', 0]]);
                    return $this->responseData('get list of categories');
                }
                break;
            case 'POST':
                $file = $request->file('FILE');
                $data = $request->request->all();
                if (!empty($file)) {
                    $fileName = $this->fileUploader->upload($file);
                    $data['hinh'] = '/' . $this->fileUploader->getTargetDir() . '/' . $fileName;
                }
                $MD->edit($data);
                return $this->responseData("edit category success");
                break;
            case 'DELETE':
                $fileName = last(explode('/', $MD->find($id)->hinh));
                $this->fileUploader->removeFile($fileName);
                $MD->remove($id, false);

                return $this->responseData("delete category success");
        }
    }

    public function authen(Request $request)
    {
        $USER_INFO = Session::get('USER_INFO', []);
        if ($request->getRequestUri() != '/authen' && (!isset($USER_INFO['vaitro']) || $USER_INFO['vaitro'] != 1)) {
            echo '<h1 style="color: red">Not authorized.</h1>';
            die;
        }
        $MD = new TaiKhoan();
        switch ($request->method()) {
            case 'POST':
                $USER_INFO = $MD->get($request->request->all());
                if (count($USER_INFO) > 0) {
                    Session::put('USER_INFO', $USER_INFO[0]);
                    return $this->responseData("authen success");
                }
                return $this->responseData("authen fail", false, 405);
                break;
            case 'DELETE':
                Session::flush();
                return $this->responseData("logout success");
        }
    }

    public function taiKhoan(Request $request)
    {
        $USER_INFO = Session::get('USER_INFO', []);
        $MD = new TaiKhoan();
        $id = $request->get('id');
        switch ($request->method()) {
            case 'GET':
                if (empty($id)) {
                    $this->data = $MD->get();
                    return $this->responseData('get list of account');
                }
                break;
            case 'POST':
                $data = $request->request->all();
                $MD->edit($data);
                return $this->responseData("edit account success");
                break;
            case 'DELETE':
                $MD->remove($id, true);
                return $this->responseData("delete category success");
        }
    }

    public function hauMai(Request $request)
    {
        $MD = new HauMai();
        $id = $request->get('id');
        switch ($request->method()) {
            case 'GET':
                if (empty($id)) {
                    $this->data = $MD->get();
                    return $this->responseData('get list of discounts');
                }
                break;
            case 'POST':
                $data = $request->request->all();
                if (false == isset($data['id'])) {
                    $data['mahm'] = crypt(uniqid(rand(), true), uniqid(rand(), true));
                }
                $MD->edit($data);
                return $this->responseData("edit discount success");
                break;
            case 'DELETE':
                $MD->remove($id, true);
                return $this->responseData("delete discount success");
        }
    }

    public function baiViet(Request $request)
    {
        $this->fileUploader->setTargetDir('images/blog');
        $MD = new BaiViet();
        $id = $request->get('id');
        switch ($request->method()) {
            case 'GET':
                if (empty($id)) {
                    $this->data = $MD->get();
                    return $this->responseData('get list of blogs');
                }
                break;
            case 'POST':
                $file = $request->file('FILE');
                $data = $request->request->all();
                if (!empty($file)) {
                    $fileName = $this->fileUploader->upload($file);
                    $data['duongdan'] = '/' . $this->fileUploader->getTargetDir() . '/' . $fileName;
                }
                $MD->edit($data);
                return $this->responseData("edit blog success");
                break;
            case 'DELETE':
                $MD->remove($id, true);
                return $this->responseData("delete blog success");
        }
    }

    public function uploadFileExtend(Request $request)
    {
        $this->fileUploader->setTargetDir('images/extend');
        $file = $request->file('FILE');
        if (!empty($file)) {
            $fileName = $this->fileUploader->upload($file);
            $this->data['duongdan'] = '/' . $this->fileUploader->getTargetDir() . '/' . $fileName;
            return $this->responseData("upload success");
        }
        return $this->responseData("upload fail", false);
    }

    public function chiTietHoaDon(Request $request)
    {
        $MD = new ChiTietHoaDon();
        switch ($request->method()) {
            case 'GET':
                $mahd = $request->get('mahd');
                $this->data = $MD->getListByMahd($mahd);
                return $this->responseData("get list of detail bills by bill id: $mahd");
                break;
            case 'POST':
                $data = $request->request->all();
                $MD->edit($data);
                return $this->responseData("edit detail bill success");
                break;
            case 'DELETE':
                $id = $request->get('id');
                $MD->remove($id, false);
                return $this->responseData("delete detail bill success");
        }
    }

    public function hoaDon(Request $request)
    {
        $MD = new HoaDon();
        $id = $request->get('id');
        switch ($request->method()) {
            case 'GET':
                if (empty($id)) {
                    $this->data = $MD->get(['isdeleted' => 0]);
                    return $this->responseData('get list of bills');
                }
                break;
            case 'POST':
                $data = $request->request->all();
                $MD->edit($data);
                return $this->responseData("edit bill success");
                break;
            case 'DELETE':
                $MD->remove($id, false);
                return $this->responseData("delete bill success");
        }
    }

    public function sanPham(Request $request)
    {
        $this->fileUploader->setTargetDir('images/product');
        $MD = new SanPham();
        $id = $request->get('id');
        switch ($request->method()) {
            case 'GET':
                if (empty($id)) {
                    $this->data = $MD->getList();
                    return $this->responseData('get list of products');
                }
                break;
            case 'POST':
                $file = $request->file('FILE');
                $data = $request->request->all();
                if (!empty($file)) {
                    $fileName = $this->fileUploader->upload($file);
                    $data['hinh'] = '/' . $this->fileUploader->getTargetDir() . '/' . $fileName;
                }
                $id = $MD->edit($data);
                $this->data = ['id' => $id];
                return $this->responseData("edit product success");
                break;
            case 'DELETE':
                $fileName = last(explode('/', $MD->find($id)->hinh));
                $this->fileUploader->removeFile($fileName);
                $MD->remove($id, false);
                $H = new Hinh();
                foreach ($H->get(['masp' => $id]) as $value) {
                    $fileName = $value['tenhinh'];
                    $this->fileUploader->removeFile($fileName);
                    $H->remove($id, true);
                }

                return $this->responseData("delete product success");
        }
    }

    public function hinh(Request $request)
    {
        $this->fileUploader->setTargetDir('images/product');
        if ($request->get('practical', 0) == 1) {
            $this->fileUploader->setTargetDir('images/practical');
        }
        $MD = new Hinh();
        switch ($request->method()) {
            case 'GET':
                $masp = $request->get('masp');
                $this->data = $MD->get(['masp' => $masp]);
                return $this->responseData("get list of images by product id: $masp");
                break;
            case 'POST':
                $files = $request->files->all();
                $data = $request->request->all();
                if (!empty($files)) {
                    foreach ($files as $key => $value) {
                        $fileName = $this->fileUploader->upload($value);
                        if (empty($fileName)) {
                            return $this->responseData("can not save image: $value->originalName"
                                , false, 500);
                        } else {
                            $data['tenhinh'] = $fileName;
                            $data['duongdan'] = '/' . $this->fileUploader->getTargetDir() . '/' . $fileName;
                            $MD->upload($data);
                        }
                    }
                }
                return $this->responseData("edit product success");
                break;
            case 'DELETE':
                $id = $request->get('id');
                $fileName = $MD->find($id)->tenhinh;
                $this->fileUploader->removeFile($fileName);
                $MD->remove($id, true);
                $this->data = ['delete' => true];
                return $this->responseData("delete product success");
        }
    }

    public function gioiThieu(Request $request)
    {
        switch ($request->method()) {
            case 'GET':
                $content = preg_replace("/\s+/", " ", view('user/intro/content')->render());
                $this->data['content'] = $content;

                return $this->responseData("get content intro");
                break;
            case 'POST':
                $content = $request->request->get('content');
                $myfile = fopen(resource_path() . '/views/user/intro/content.twig', "w") or die("Unable to open file!");
                fwrite($myfile, preg_replace("/\s+/", " ", $content));
                fclose($myfile);

                return $this->responseData("edit content intro success");
                break;
        }
    }

    public function veChungToi(Request $request)
    {
        switch ($request->method()) {
            case 'GET':
                $content = preg_replace("/\s+/", " ", view('user/about/content')->render());
                $this->data['content'] = $content;

                return $this->responseData("get content aboutus");
                break;
            case 'POST':
                $content = $request->request->get('content');
                $myfile = fopen(resource_path() . '/views/user/about/content.twig', "w") or die("Unable to open file!");
                fwrite($myfile, preg_replace("/\s+/", " ", $content));
                fclose($myfile);

                return $this->responseData("edit content aboutus success");
                break;
        }
    }

    public function chinhSach(Request $request)
    {
        switch ($request->method()) {
            case 'GET':
                $content = preg_replace("/\s+/", " ", view('user/policy/content')->render());
                $this->data['content'] = $content;

                return $this->responseData("get content policy");
                break;
            case 'POST':
                $content = $request->request->get('content');
                $myfile = fopen(resource_path() . '/views/user/policy/content.twig', "w") or die("Unable to open file!");
                fwrite($myfile, preg_replace("/\s+/", " ", $content));
                fclose($myfile);

                return $this->responseData("edit content policy success");
                break;
        }
    }

    public function theme(Request $request)
    {
        $this->fileUploader->setTargetDir('images/theme');
        $MD = new Theme();
        $id = $request->get('id');
        switch ($request->method()) {
            case 'GET':
                if (empty($id)) {
                    $this->data = $MD->get();
                    return $this->responseData('get list of themes');
                }
                break;
            case 'POST':
                $file = $request->file('FILE');
                $data = $request->request->all();
                $data['noidung'] = preg_replace("/\s+/", " ", $data['noidung']);
                if (!empty($file)) {
                    $fileName = $this->fileUploader->upload($file);
                    $data['hinh'] = '/' . $this->fileUploader->getTargetDir() . '/' . $fileName;
                }
                $MD->edit($data);
                $this->data = ['id' => $id];
                return $this->responseData("edit theme success");
                break;
            case 'DELETE':
                $fileName = last(explode('/', $MD->find($id)->hinh));
                $this->fileUploader->removeFile($fileName);
                $MD->remove($id, true);
                return $this->responseData("delete theme success");
        }
    }

    public function sanPhamThucTe(Request $request)
    {
        $this->fileUploader->setTargetDir('images/practical');
        $MD = new SanPham();
        $id = $request->get('id');
        switch ($request->method()) {
            case 'GET':
                if (empty($id)) {
                    $this->data = $MD->get(['maloaisp' => 0]);
                    return $this->responseData('get list of practical products');
                }
                break;
            case 'POST':
                $file = $request->file('FILE');
                $data = $request->request->all();
                if (!empty($file)) {
                    $fileName = $this->fileUploader->upload($file);
                    $data['hinh'] = '/' . $this->fileUploader->getTargetDir() . '/' . $fileName;
                }
                $id = $MD->edit($data);
                $this->data = ['id' => $id];
                return $this->responseData("edit practical product success");
                break;
            case 'DELETE':
                $MD->remove($id, true);
                $H = new Hinh();
                foreach ($H->get(['masp' => $id]) as $value) {
                    $fileName = $value['tenhinh'];
                    $this->fileUploader->removeFile($fileName);
                    $H->remove($id, true);
                }
                return $this->responseData("delete practical product success");
        }
    }

    public function dichVu(Request $request)
    {
        switch ($request->method()) {
            case 'GET':
                $keyApi = Storage::get('keyApi.txt');
                try {
                    \Tinify\setKey($keyApi);
                    \Tinify\validate();
                    $this->data['keyApi'] = $keyApi;
                    $this->data['compressionsThisMonth'] = \Tinify\compressionCount();
                    return $this->responseData('get list of services');
                } catch (\Tinify\Exception $e) {
                    // Validation of API key failed.
                    return $this->responseData('Key api not correctly or times out of date.', false);
                }
                break;
            case 'POST':
                $keyApi = $request->request->get('keyApi');
                try {
                    \Tinify\setKey($keyApi);
                    \Tinify\validate();
                    Storage::put('keyApi.txt', $keyApi);
                    return $this->responseData('Key api correctly');
                } catch (\Tinify\Exception $e) {
                    // Validation of API key failed.
                    return $this->responseData('Key api not correctly', false);
                }
        }
    }

    public function optimizeAll(Request $request)
    {
        $mode = $request->get('mode');
        $offset = $request->get('offset');
        $keyApi = Storage::get('keyApi.txt');
        switch ($mode) {
            case 'theme':
                $path = public_path() . '/images/theme';
                break;
            case 'product':
                $path = public_path() . '/images/product';
                break;
            case 'category':
                $path = public_path() . '/images/category';
                break;
            case 'practical':
                $path = public_path() . '/images/practical';
                break;
            case 'blog':
                $path = public_path() . '/images/blog';
                break;
            case 'extend':
                $path = public_path() . '/images/extend';
                break;
            default :
                return '<h1 style="color: red;text-align: center;">Missing Parameter</h1>';
        }
        $arrFiles = scandir($path);
        if ($arrFiles[0] == '.') {
            unset($arrFiles[0]);
        }
        if ($arrFiles[1] == '..') {
            unset($arrFiles[1]);
        }
        sort($arrFiles);

        return $this->optimize(array_slice($arrFiles, $offset, self::NUMBERS_OPTIMIZE), $path, $keyApi);
    }

    private function optimize($arrFiles, $pathDir, $keyApi = '')
    {
        if (count($arrFiles) > 0) {
            try {
                \Tinify\setKey($keyApi);
                \Tinify\validate();
                foreach ($arrFiles as $fileName) {
                    $pathFile = $pathDir . '/' . $fileName;
                    try {
                        \Tinify\fromFile($pathFile)->toFile($pathFile);
                        echo "<h3 style=\"color: green;\">$fileName was optimized</h3>";
                    } catch (\Tinify\Exception $e) {
                        // Validation of API key failed.
                        echo "<h3 style=\"color: red;\">$fileName can not optimize</h3>";
                    }
                }
            } catch (\Tinify\Exception $e) {
                // Validation of API key failed.
                echo '<h1 style="color: red;text-align: center;">=====Key api not correctly or times out of date.=====</h1>';
            }
        }
    }

    public function removeUnusedImgs(Request $request)
    {
        echo "<h1 style='color: green'>THEME</h1>";
        $this->fileUploader->setTargetDir('images/theme');
        $path = public_path() . '/images/theme';
        $renderData['theme'] = scandir($path);
        foreach (array_slice($renderData['theme'], 2) as $file_name) {
            $item = DB::selectOne("select id from theme where hinh like \"%/$file_name%\";\r\n");
            if (!$item) {
                echo "<h5 style='color: red'>$file_name</h5>";
                $this->fileUploader->removeFile($file_name);
            }
        }

        echo "<h1 style='color: green'>CATEGORY</h1>";
        $this->fileUploader->setTargetDir('images/category');
        $path = public_path() . '/images/category';
        $renderData['category'] = scandir($path);
        foreach (array_slice($renderData['category'], 2) as $file_name) {
            $item = DB::selectOne("select id from loaisanpham where hinh like \"%/$file_name%\";\r\n");
            if (!$item) {
                echo "<h5 style='color: red'>$file_name</h5>";
                $this->fileUploader->removeFile($file_name);
            }
        }

        echo "<h1 style='color: green'>BLOG</h1>";
        $this->fileUploader->setTargetDir('images/blog');
        $path = public_path() . '/images/blog';
        $renderData['blog'] = scandir($path);
        foreach (array_slice($renderData['blog'], 2) as $file_name) {
            $item = DB::selectOne("select id from baiviet where duongdan like \"%/$file_name%\";\r\n");
            if (!$item) {
                echo "<h5 style='color: red'>$file_name</h5>";
                $this->fileUploader->removeFile($file_name);
            }
        }

        echo "<h1 style='color: green'>PRODUCT</h1>";
        $this->fileUploader->setTargetDir('images/product');
        $path = public_path() . '/images/product';
        $renderData['product'] = scandir($path);
        foreach (array_slice($renderData['product'], 2) as $file_name) {
            $item = DB::selectOne("select s.id
            from sanpham s left join hinh h on s.id = h.masp
            where s.maloaisp <> 0 and h.duongdan like \"%/$file_name%\" or s.hinh like \"%/$file_name%\";\r\n");
            if (!$item) {
                echo "<h5 style='color: red'>$file_name</h5>";
                $this->fileUploader->removeFile($file_name);
            }
        }

        echo "<h1 style='color: green'>PRACTICAL</h1>";
        $this->fileUploader->setTargetDir('images/practical');
        $path = public_path() . '/images/practical';
        $renderData['practical'] = scandir($path);
        foreach (array_slice($renderData['practical'], 2) as $file_name) {
            $item = DB::selectOne("select s.id 
            from sanpham s left join hinh h on s.id = h.masp 
            where s.maloaisp = 0 and h.duongdan like \"%/$file_name%\" or s.hinh like \"%/$file_name%\";\r\n");
            if (!$item) {
                echo "<h5 style='color: red'>$file_name</h5>";
                $this->fileUploader->removeFile($file_name);
            }
        }

        echo "<h1 style='color: green'>ARCHIVE</h1>";
        $this->fileUploader->setTargetDir('images/archive');
        $path = public_path() . '/images/archive';
        $renderData['archive'] = scandir($path);
        foreach (array_slice($renderData['archive'], 2) as $file_name) {
            $this->fileUploader->removeFile($file_name);
        }
    }


    public function optimizeImgs(Request $request)
    {
        /*
        $yourfile = public_path('archive.zip');
        $fileNameZip = basename($yourfile);
        header("Content-Type: application/zip");
        header("Content-Disposition: attachment; filename=$fileNameZip");
        header("Content-Length: " . filesize($yourfile));
        readfile($yourfile);
        exit;*/
        $fileNameZip = 'archive.zip';
        $pathZip = public_path() . '/' . $fileNameZip;
        if (file_exists($pathZip)) {
            unlink($pathZip);
        }
        $this->fileUploader->setTargetDir('images/archive');
        $path = public_path() . '/images/archive';
        $renderData['archive'] = scandir($path);
        foreach (array_slice($renderData['archive'], 2) as $file_name) {
            $this->fileUploader->removeFile($file_name);
        }

        $arrFiles = $request->files->all();
        if (count($arrFiles) > 0) {
            $keyApi = Storage::get('keyApi.txt');
            try {
                \Tinify\setKey($keyApi);
                \Tinify\validate();
                foreach ($arrFiles as $file) {
                    $fileName = $this->fileUploader->upload($file);
                    if ($fileName) {
                        $pathFile = public_path() . '/' . $this->fileUploader->getTargetDir() . '/' . $fileName;
                        \Tinify\fromFile($pathFile)->toFile($pathFile);
                    }

                }
            } catch (\Tinify\Exception $e) {
                // Validation of API key failed.
                return $this->responseData('Zip fail', false);
            }
        }

        // Get real path for our folder
        $rootPath = realpath($path);

        // Initialize archive object
        $zip = new \ZipArchive();
        $zip->open($fileNameZip, \ZipArchive::CREATE | \ZipArchive::OVERWRITE);

        // Create recursive directory iterator
        /** @var SplFileInfo[] $files */
        $files = new \RecursiveIteratorIterator(
            new \RecursiveDirectoryIterator($rootPath),
            \RecursiveIteratorIterator::LEAVES_ONLY
        );

        foreach ($files as $name => $file) {
            // Skip directories (they would be added automatically)
            if (!$file->isDir()) {
                // Get real and relative path for current file
                $filePath = $file->getRealPath();
                $relativePath = substr($filePath, strlen($rootPath) + 1);

                // Add current file to archive
                $zip->addFile($filePath, $relativePath);
            }
        }
        // Zip archive will be created only after closing object
        $zip->close();

        $filesize = filesize($pathZip); // bytes
        $filesize = round($filesize / 1024 / 1024, 1);

        $this->data['urlZip'] = $fileNameZip;
        $this->data['fileSize'] = $filesize;
        return $this->responseData('Zip success');
    }


    public function helps(Request $request)
    {
        $this->fileUploader->setTargetDir('images/help');
        $MD = new Helps();
        $id = $request->get('id');
        switch ($request->method()) {
            case 'GET':
                if (empty($id)) {
                    $this->data = $MD->get();
                    return $this->responseData('get list of helps');
                }
                break;
            case 'POST':
                $file = $request->file('FILE');
                $data = $request->request->all();
                $data['noidung'] = preg_replace("/\s+/", " ", $data['noidung']);
                if (!empty($file)) {
                    $fileName = $this->fileUploader->upload($file);
                    $data['hinh'] = '/' . $this->fileUploader->getTargetDir() . '/' . $fileName;
                }
                $MD->edit($data);
                $this->data = ['id' => $id];
                return $this->responseData("edit theme help");
                break;
        }
    }
}
