<?php

namespace App\Http\Controllers;

use App\Models\BaiViet;
use App\Models\ChiTietHoaDon;
use App\Models\HauMai;
use App\Models\Helps;
use App\Models\Hinh;
use App\Models\HoaDon;
use App\Models\LoaiSanPham;
use App\Models\SanPham;
use App\Models\Theme;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Mail;

class UserController extends BaseController
{
    const HOME_PAGE = "user/home/index";
    const CAT_PAGE = "user/category/index";
    const DISCOUNT_PAGE = "user/discount/index";
    const PRODUCT_PAGE = "user/product/index";
    const CART_PAGE = "user/cart/index";
    const POLICY_PAGE = "user/policy/index";
    const INTRO_PAGE = "user/intro/index";
    const ABOUTUS_PAGE = "user/about/index";
    const CHECKOUT_PAGE = "user/checkout/index";
    const SEARCH_PAGE = "user/search/index";
    const PRACTICAL_PAGE = "user/practical/index";
    const PRACTICAL_DETAIL_PAGE = "user/practical/detail";
    const BLOG_PAGE = "user/blog/index";

    public function __construct(Request $request)
    {
        parent::__construct();
        if ($request->header('x-requested-with', null) != 'XMLHttpRequest') {
            $LSP = new LoaiSanPham();
            $listLoaiSP = $LSP->get(['isdeleted' => 0, ['id', '<>', 0]]);
            $list = array();
            $i = 0;
            foreach ($listLoaiSP as $value) {
                $value['cat_name'] = str_replace(' ', '-', $this->change_alias($value['tenloaisp'])) . '-' . $value['id'];
                $list[$value['tenloaisp0']][$i] = $value;
                $i++;
            }
            $this->data['loaisanpham'] = $list;
            $this->data['base_url'] = URL::to('/') . '/';
            $T = new Theme();
            $this->data['meta_image'] = $T->select('hinh')->where(array('hienthi' => 1))->orderBy('id', 'asc')->first()->hinh;
        }
    }

    public function change_alias($alias)
    {
        $str = $alias;
        $str = preg_replace('/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ|À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ/', "a", $str);
        $str = preg_replace('/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ|È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ/', "e", $str);
        $str = preg_replace('/ì|í|ị|ỉ|ĩ|Ì|Í|Ị|Ỉ|Ĩ/', "i", $str);
        $str = preg_replace('/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ|Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ/', "o", $str);
        $str = preg_replace('/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ|Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ/', "u", $str);
        $str = preg_replace('/ỳ|ý|ỵ|ỷ|ỹ|Ỳ|Ý|Ỵ|Ỷ|Ỹ/', "y", $str);
        $str = preg_replace('/đ|Đ/', "d", $str);
        $str = preg_replace('/!|@|%|\^|\*|\(|\)|\+|\=|\<|\>|\?|\/|,|\.|\:|\;|\'|\"|\&|\#|\[|\]|~|\$|_|`|-|{|}|\||\\//', " ", $str);
        $str = preg_replace('/ + /', " ", $str);
        $str = trim($str);
        $str = strtolower($str);

        return $str;
    }

    public function home(Request $request)
    {
        $view = 'user/layout';
        if (count($this->data) == 0) {
            $view = self::HOME_PAGE;
            $this->data['is_Ajax'] = true;
        }
        $SP = new SanPham();
        $BV = new BaiViet();
        $T = new Theme();
        $Hepls = new Helps();

        $this->data['helps'] = $Hepls->orderBy('id', 'asc')->get()->toArray();

        $this->data['blogs'] = $BV->getTopNew();
        foreach ($this->data['blogs'] as &$value) {
            $arr = explode(' ', $value['noidung']);
            if (count($arr) > 50) {
                $value['noidung'] = implode(' ', array_slice($arr, 0, 50)) . '...';
            }
        }

        foreach ($this->data['blogs'] as &$value) {
            $value['tieude_slug'] = str_replace(' ', '-', $this->change_alias($value['tieude'])) . '-' . $value['id'];
        }
        $this->data['themes'] = $T->where(array('hienthi' => 1))->get()->toArray();

        $this->data['topsales'] = $SP->getTop('soluongban');
        foreach ($this->data['topsales'] as &$value) {
            $value['pro_name'] = str_replace(' ', '-', $this->change_alias($value['tenloaisp'] . '-' . $value['tensp'])) . '-' . $value['id'];
        }
        $this->data['topviews'] = $SP->getTop('soluongviews');
        foreach ($this->data['topviews'] as &$value) {
            $value['pro_name'] = str_replace(' ', '-', $this->change_alias($value['tenloaisp'] . '-' . $value['tensp'])) . '-' . $value['id'];
        }
        $this->data['topnew'] = $SP->getTop('created');
        foreach ($this->data['topnew'] as &$value) {
            $value['pro_name'] = str_replace(' ', '-', $this->change_alias($value['tenloaisp'] . '-' . $value['tensp'])) . '-' . $value['id'];
        }

        if (!isset($this->data['loaisanpham'])) {
            $LSP = new LoaiSanPham();
            $listLoaiSP = $LSP->get(['isdeleted' => 0, ['id', '<>', 0]]);
            $list = array();
            $i = 0;
            foreach ($listLoaiSP as $value) {
                $value['cat_name'] = str_replace(' ', '-', $this->change_alias($value['tenloaisp'])) . '-' . $value['id'];
                $list[$value['tenloaisp0']][$i] = $value;
                $i++;
            }
            $this->data['loaisanpham'] = $list;
        }

        $this->data['title_head'] = 'Trang chủ';
        return preg_replace("/\s+/", " ", view($view)->with(
            [
                'renderData' => $this->data,
                'container' => self::HOME_PAGE
            ]
        )->render());
    }

    public function category(Request $request, $cat_name, $page)
    {
        $view = 'user/layout';
        if (count($this->data) == 0) {
            $view = self::CAT_PAGE;
            $this->data['is_Ajax'] = true;
        }

        $catID = last(explode('-', $cat_name));
        $SP = new SanPham();
        $loaiSP = new LoaiSanPham();
        $tenLoaiSP = $loaiSP->getName($catID);
        if (empty($tenLoaiSP)) {
            return response()->view('error', ['code' => 404, 'msg' => 'WAS NOT FOUND'], 404);
        }
        $this->data['products'] = $SP->getByCatID($catID, $page);
        foreach ($this->data['products']['data'] as &$value) {
            $value['pro_name'] = str_replace(' ', '-', $this->change_alias($value['tenloaisp'] . '-' . $value['tensp'])) . '-' . $value['id'];
        }
        $this->data['catID'] = $cat_name;
        $LSP = new LoaiSanPham();
        $item = $LSP->find($catID);
        $this->data['categoryName'] = $item->tenloaisp;
        $this->data['categoryParentName'] = $item->tenloaisp0;

        $this->data['title_head'] = $tenLoaiSP;
        return preg_replace("/\s+/", " ", view($view)->with(
            [
                'renderData' => $this->data,
                'container' => self::CAT_PAGE
            ]
        )->render());
    }

    public function product(Request $request, $pro_name)
    {
        $view = 'user/layout';
        if (count($this->data) == 0) {
            $view = self::PRODUCT_PAGE;
            $this->data['is_Ajax'] = true;
        }
        $proName = $pro_name;
        $id = last(explode('-', $proName));
        $SP = new SanPham();
        $this->data['product'] = $SP->getByIDOrName($id, $proName);

        if (!$this->data['product']) {
            return response()->view('error', ['code' => 404, 'msg' => 'WAS NOT FOUND'], 404);
        }

        $this->data['topviews'] = $SP->getTop('soluongviews');
        foreach ($this->data['topviews'] as &$value) {
            $value['pro_name'] = str_replace(' ', '-', $this->change_alias($value['tenloaisp'] . '-' . $value['tensp'])) . '-' . $value['id'];
        }
        $H = new Hinh();
        $this->data['imgs'] = $H->get(['masp' => $id]);
        $this->data['lists'] = array_merge($SP->getTop('maloaisp'), $SP->getTop('soluongviews'), $SP->getTop('soluongban'));

        foreach ($this->data['lists'] as &$value) {
            $value['pro_name'] = str_replace(' ', '-', $this->change_alias($value['tenloaisp'] . '-' . $value['tensp'])) . '-' . $value['id'];
        }
        $this->data['product']['pro_name'] = str_replace(' ', '-', $this->change_alias($this->data['product']['tenloaisp'] . '-' . $this->data['product']['tensp'])) . '-' . $this->data['product']['id'];
        $this->data['product']['tenloaisp_slug'] = str_replace(' ', '-', $this->change_alias($this->data['product']['tenloaisp'])) . '-' . $this->data['product']['maloaisp'];

        $this->data['title_head'] = $this->data['product']['tenloaisp'] . ' - ' . $this->data['product']['tensp'];
        return preg_replace("/\s+/", " ", view($view)->with(
            [
                'renderData' => $this->data,
                'container' => self::PRODUCT_PAGE
            ]
        )->render());
    }

    public function cart(Request $request)
    {
        $view = 'user/layout';
        if (count($this->data) == 0) {
            $view = self::CART_PAGE;
            $this->data['is_Ajax'] = true;
        }

        $this->data['title_head'] = 'Giỏ hàng của bạn';
        return preg_replace("/\s+/", " ", view($view)->with(
            [
                'renderData' => $this->data,
                'container' => self::CART_PAGE
            ]
        )->render());
    }

    public function policy(Request $request)
    {
        $view = 'user/layout';
        if (count($this->data) == 0) {
            $view = self::POLICY_PAGE;
            $this->data['is_Ajax'] = true;
        }

        $this->data['title_head'] = 'Chính sách và quy định';
        return preg_replace("/\s+/", " ", view($view)->with(
            [
                'renderData' => $this->data,
                'container' => self::POLICY_PAGE
            ]
        )->render());
    }

    public function intro(Request $request)
    {
        $view = 'user/layout';
        if (count($this->data) == 0) {
            $view = self::INTRO_PAGE;
            $this->data['is_Ajax'] = true;
        }

        $this->data['title_head'] = 'Giới thiệu';
        return preg_replace("/\s+/", " ", view($view)->with(
            [
                'renderData' => $this->data,
                'container' => self::INTRO_PAGE
            ]
        )->render());
    }

    public function aboutus(Request $request)
    {
        $view = 'user/layout';
        if (count($this->data) == 0) {
            $view = self::ABOUTUS_PAGE;
            $this->data['is_Ajax'] = true;
        }

        $this->data['title_head'] = 'Về chúng tôi';
        return preg_replace("/\s+/", " ", view($view)->with(
            [
                'renderData' => $this->data,
                'container' => self::ABOUTUS_PAGE
            ]
        )->render());
    }

    public function checkout(Request $request)
    {
        $view = 'user/layout';
        if (count($this->data) == 0) {
            $view = self::CHECKOUT_PAGE;
            $this->data['is_Ajax'] = true;
        }

        $this->data['title_head'] = 'Đặt hàng';
        return preg_replace("/\s+/", " ", view($view)->with(
            [
                'renderData' => $this->data,
                'container' => self::CHECKOUT_PAGE
            ]
        )->render());
    }

    public function checkCoupon(Request $request)
    {
        $mahm = $request->get('mahm');
        $hmMD = new HauMai();
        $sales = $hmMD->checkVaild($mahm);
        if (empty($sales)) {
            return $this->responseData('coupon is invalid', false);
        }
        $this->data['sales'] = $sales;

        return $this->responseData('coupon is valid');
    }

    public function execute_checkout(Request $request)
    {
        $HD = new HoaDon();
        $hmMD = new HauMai();
        $data = $request->all();
        $listCart = json_decode($data['list-cart'], true);
        unset($data['list-cart']);
        $mahm = $request->get('mahm');
        $phantram = 0;
        if ($mahm) {
            $sales = $hmMD->checkVaild($mahm);
            if (count($sales) > 0) {
                $phantram = $sales['phantram'];
                $hmMD->edit(array('id' => $sales['id'], 'dasudung' => 1));
            } else {
                unset($data['mahm']);
            }
        }
        $totalAll = 0;
        $idHD = $HD->edit($data);

        Mail::raw("
        Họ tên: {$data['hotenkh']}
        Địa chỉ: {$data['diachi']}
        Số điện thoại: {$data['sdt']}
        Email: {$data['email']}", function ($message) {
            $message->to('vodoanthanh093@gmail.com')
                ->subject('CÓ KHÁCH ĐẶT HÀNG');
        });

        foreach ($listCart as $key => $value) {
            $total = $value['price'] * (100 - $value['sales']) / 100;
            $totalAll += $total;
            (new ChiTietHoaDon())->edit(array(
                'mahd' => $idHD,
                'masp' => $key,
                'gia' => $total,
                'soluong' => $value['times']
            ));
        }
        $totalAll = $totalAll * (100 - $phantram) / 100;
        $HD->edit(array('tongtien' => $totalAll));

        return $this->responseData('success for checkouting', true);
    }

    public function discount(Request $request, $page)
    {
        $view = 'user/layout';
        if (count($this->data) == 0) {
            $view = self::DISCOUNT_PAGE;
            $this->data['is_Ajax'] = true;
        }
        $SP = new SanPham();
        $this->data['discounts'] = $SP->getDiscount($page);

        foreach ($this->data['discounts']['data'] as &$value) {
            $value['pro_name'] = str_replace(' ', '-', $this->change_alias($value['tenloaisp'] . '-' . $value['tensp'])) . '-' . $value['id'];
        }

        $this->data['title_head'] = 'Khuyến mãi';
        return preg_replace("/\s+/", " ", view($view)->with(
            [
                'renderData' => $this->data,
                'container' => self::DISCOUNT_PAGE
            ]
        )->render());
    }

    public function search(Request $request)
    {
        $view = 'user/layout';
        if (count($this->data) == 0) {
            $view = self::SEARCH_PAGE;
            $this->data['is_Ajax'] = true;
        }
        $page = $request->get('trang', 1);
        $keyword = $request->get('kw', ' ');
        $keyword = str_replace('+', ' ', $keyword);
        $SP = new SanPham();
        $this->data['products'] = $SP->getByKeyWords($keyword, $page);
        foreach ($this->data['products']['data'] as &$value) {
            $value['pro_name'] = str_replace(' ', '-', $this->change_alias($value['tenloaisp'] . '-' . $value['tensp'])) . '-' . $value['id'];
        }
        $this->data['keywords'] = $keyword;

        $this->data['title_head'] = 'Tìm kiếm';
        return preg_replace("/\s+/", " ", view($view)->with(
            [
                'renderData' => $this->data,
                'container' => self::SEARCH_PAGE
            ]
        )->render());
    }

    public function practical(Request $request, $page)
    {
        $view = 'user/layout';
        if (count($this->data) == 0) {
            $view = self::PRACTICAL_PAGE;
            $this->data['is_Ajax'] = true;
        }
        $SP = new SanPham();
        $this->data['products'] = $SP->getByCatID(0, $page, 12);
        $H = new Hinh();
        foreach ($this->data['products']['data'] as &$value) {
            $value['title_link'] = str_replace(' ', '-', $this->change_alias($value['tensp'])) . '-' . $value['id'];
            $value['img'] = $H->getMainForPractial(['masp' => $value['id']]);
        }

        $this->data['title_head'] = 'Các sản phẩm thực tế';
        return preg_replace("/\s+/", " ", view($view)->with(
            [
                'renderData' => $this->data,
                'container' => self::PRACTICAL_PAGE
            ]
        )->render());
    }

    public function practicalDetail(Request $request, $title)
    {
        $view = 'user/layout';
        if (count($this->data) == 0) {
            $view = self::PRACTICAL_DETAIL_PAGE;
            $this->data['is_Ajax'] = true;
        }
        $masp = last(explode('-', $title));
        $SP = new SanPham();
        $spItem = $SP->find($masp);
        if (!$spItem || 0 != $spItem->maloaisp) {
            return response()->view('error', ['code' => 404, 'msg' => 'WAS NOT FOUND'], 404);
        }

        $H = new Hinh();
        $this->data['imgs'] = $H->where(['masp' => $masp])->orderBy('id', 'asc')->get()->toArray();

        $this->data['title_head'] = $spItem->tensp;
        $this->data['content'] = $spItem->dacta;

        return preg_replace("/\s+/", " ", view($view)->with(
            [
                'renderData' => $this->data,
                'container' => self::PRACTICAL_DETAIL_PAGE
            ]
        )->render());
    }

    public function blog(Request $request, $title)
    {
        $view = 'user/layout';
        if (count($this->data) == 0) {
            $view = self::BLOG_PAGE;
            $this->data['is_Ajax'] = true;
        }
        $id = last(explode('-', $title));

        $bv = new BaiViet();
        $item = $bv->find($id);
        if (!$item) {
            return response()->view('error', ['code' => 404, 'msg' => 'WAS NOT FOUND'], 404);
        }

        $this->data['chitiet'] = str_replace('<img src="', '<img class="product-thumb" style="max-width: 100%;max-height: 70%!important;cursor: pointer;" src="/', $item->chitiet);
        $this->data['chitiet'] = str_replace('<a ', '<a class="linkSocial" target="_blank"', $this->data['chitiet']);
        $this->data['tieude'] = $item->tieude;

        $this->data['title_head'] = 'Các sản phẩm thực tế';
        return preg_replace("/\s+/", " ", view($view)->with(
            [
                'renderData' => $this->data,
                'container' => self::BLOG_PAGE
            ]
        )->render());
    }
}
