<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller;

/**
 * Class BaseController
 * @package App\Http\Controllers
 */
class BaseController extends Controller
{
    /**
     * @var array
     */
    protected $data;

    /**
     * BaseController constructor.
     */
    public function __construct()
    {
        $this->data = [];
    }

    /**
     * @param string $msg
     * @param bool $status
     * @param int $code
     * @return \Illuminate\Http\JsonResponse
     */
    public function responseData($msg = '', $status = true, $code = 200)
    {
        return response()->json(
            [
                'status' => $status,
                'msg' => $msg,
                'data' => $this->data,
                'code' => $code
            ],
            $code
        );
    }
}
