<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class AdminController extends BaseController
{
    public function index(Request $request)
    {
        $USER_INFO = Session::get('USER_INFO', []);
        if (empty($USER_INFO)) {
            return redirect()->route('login_page')->send();
        }
        $renderData = array();
        $renderData['isAdmin'] = $USER_INFO['vaitro'];
        $renderData['tenhienthi'] = $USER_INFO['tenhienthi'];
        $path = public_path() . '/images/theme';
        $renderData['theme'] = count(scandir($path)) - 2;
        $path = public_path() . '/images/product';
        $renderData['product'] = count(scandir($path)) - 2;
        $path = public_path() . '/images/category';
        $renderData['category'] = count(scandir($path)) - 2;
        $path = public_path() . '/images/practical';
        $renderData['practical'] = count(scandir($path)) - 2;
        $path = public_path() . '/images/blog';
        $renderData['blog'] = count(scandir($path)) - 2;
        $path = public_path() . '/images/extend';
        $renderData['extend'] = count(scandir($path)) - 2;

        return preg_replace("/\s+/", " ",
            view('admin/index')->with(['renderData' => $renderData])
                ->render());
    }

    public function login(Request $request)
    {
        $USER_INFO = Session::get('USER_INFO', []);
        if (empty($USER_INFO)) {
            return preg_replace("/\s+/", " ", view('admin/login')->render());
        }
        return redirect()->route('admin_page')->send();
    }
}
