<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as Middleware;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;

class VerifyCsrfToken extends Middleware
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        //
    ];

    /**
     * @author ThanhVD
     * @param Request $request
     * @param \Closure $next
     * @return mixed
     * @throws \Illuminate\Session\TokenMismatchException
     * @desc check verify csrf
     */
    public function handle($request, \Closure $next)
    {
        $urlBase = URL::to('/');
        $hostRequest = $request->getHost();
        $checkVerify = strpos($urlBase, $hostRequest);
        if ($checkVerify) {
            // skip CSRF check
            return $next($request);
        }

        return parent::handle($request, $next);
    }
}
