<?php

namespace App\Service;

use Symfony\Component\HttpFoundation\File\UploadedFile;


/**
 * Class FileUploader
 * @package App\Service
 */
class FileUploader
{
    //jpg|jpeg|png|gif|bmp|pdf
    const ALLOW_UPLOAD_FILE_TYPE = [
        'image/jpeg' => 1,
        'image/png' => 1,
        'image/gif' => 1,
        'image/bmp' => 1,
        'application/pdf' => 1
    ];
    /**
     * @var string
     */
    private $targetDir;

    /**
     * FileUploader constructor.
     */
    public function __construct()
    {
        $this->targetDir = "";
    }

    /**
     * @return string
     */
    public function getTargetDir()
    {
        return $this->targetDir;
    }

    /**
     * @param string $targetDir
     */
    public function setTargetDir($targetDir)
    {
        $this->targetDir = $targetDir;
    }

    /**
     * @param $file
     * @return bool
     */
    public function checkAllowUploadFileType(string $file)
    {
        $extension = mime_content_type($file);
        return self::ALLOW_UPLOAD_FILE_TYPE[$extension] ? true : false;
    }

    /**
     * @param UploadedFile $file
     * @param bool $keepName
     * @return string
     */
    public function upload(UploadedFile $file, bool $keepName = true)
    {
        if (!empty($this->targetDir)) {
            if ($this->checkAllowUploadFileType($file)) {
                $fileName = $keepName ? $file->getClientOriginalName() : md5(uniqid()) . '.' . $file->guessExtension();
                if (file_exists($this->getTargetDir() . '/' . $fileName)) {
                    unlink($this->getTargetDir() . '/' . $fileName);
                }
                $file->move($this->getTargetDir(), $fileName);

                return $fileName;
            }
        }
        return '';
    }

    /**
     * @param $fileName
     * @return bool
     */
    public function removeFile($fileName)
    {
        if (!empty($this->targetDir)) {
            $file_path = $this->getTargetDir() . '/' . $fileName;
            if (file_exists($file_path)) {
                unlink($file_path);
                return true;
            }
        }
        return false;
    }
}
