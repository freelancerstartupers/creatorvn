<?php

namespace App\Models;

class Theme extends Base
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'theme';
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
}
