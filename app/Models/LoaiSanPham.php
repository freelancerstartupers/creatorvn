<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class LoaiSanPham extends Base
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'loaisanpham';
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    public function getName($id)
    {
         $item = $this->find($id);
        if($item)
        {
            return $item->tenloaisp;
        }
        return '';
    }
}
