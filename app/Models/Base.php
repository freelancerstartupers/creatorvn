<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Base
 * @package App\Models
 */
class Base extends Model
{
    public $timestamps = false;

    /**
     * @param array $where
     * @return mixed
     */
    public function get($where = [])
    {
        return $this->where($where)->orderBy('id','desc')->get()->toArray();
    }

    /**
     * @param $data
     * @return int
     */
    public function edit($data)
    {
        if (isset($data['id'])) {
            $item = $this->find($data['id']);
            unset($data['id']);
        } else {
            $item = $this;
        }
        foreach ($data as $key => $val) {
            $item->$key = $val;
        }
        $item->save();

        return $item->id;
    }


    /**
     * @param $id
     * @param $isDeleted
     * @return bool
     */
    public function remove($id, $isDeleted)
    {
        $item = $this->find($id);
        if ($item) {
            if ($isDeleted) {
                $item->delete();
            } else {
                $item->isdeleted = 1;
                $item->save();
            }

            return true;
        }
        return false;
    }

    /**
     * @return array|mixed
     */
    protected function getKeyForSaveQuery()
    {
        if (!is_array($this->primaryKey)) {
            $this->primaryKey = array_wrap($this->primaryKey);
        }
        $primaryKeyForSaveQuery = array(count($this->primaryKey));
        foreach ($this->primaryKey as $i => $pKey) {
            $primaryKeyForSaveQuery[$i] = isset($this->original[$this->getKeyName()[$i]])
                ? $this->original[$this->getKeyName()[$i]]
                : $this->getAttribute($this->getKeyName()[$i]);
        }

        return $primaryKeyForSaveQuery;
    }

    /**
     * Set the keys for a save update query.
     * @param  \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    protected function setKeysForSaveQuery(Builder $query)
    {
        $primaryKeys = array_wrap($this->getKeyName());
        foreach ($primaryKeys as $pKey) {
            $query->where($pKey, $this->getAttribute($pKey));
        }

        return $query;
    }

}
