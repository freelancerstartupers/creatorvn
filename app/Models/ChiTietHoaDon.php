<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;

class ChiTietHoaDon extends Base
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'chitiethoadon';
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    public function getListByMahd($mahd)
    {
        $results = DB::select("select chitiethoadon.`id`, sanpham.`tensp`, chitiethoadon.`gia`, `soluong` 
                    from `chitiethoadon`, `sanpham`
                    where `mahd` = $mahd and sanpham.`id` = chitiethoadon.`masp` and chitiethoadon.`isdeleted` = 0");

        return json_decode(json_encode($results), true);
    }
}
