<?php

namespace App\Models;

class Helps extends Base
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'helps';
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
}
