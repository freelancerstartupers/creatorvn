<?php

namespace App\Models;

class TaiKhoan extends Base
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'taikhoan';
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
}
