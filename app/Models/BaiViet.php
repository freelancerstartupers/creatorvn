<?php

namespace App\Models;

class BaiViet extends Base
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'baiviet';
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    public function getTopNew()
    {
        $results = $this->orderBy('id', 'desc')->limit(3)->get();

        return json_decode(json_encode($results), true);
    }
}
