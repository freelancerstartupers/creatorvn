<?php

namespace App\Models;

class Hinh extends Base
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'hinh';
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    public function upload($data)
    {
        $item = $this->where($data)->get()->toArray();
        if (empty($item)) {
            $result = $this->insert($data);
        } else {
            $item = $item[0];
            $result = $this->where(['id' => $item['id']])->update($data);
        }

        return $result;
    }

    public function getMainForPractial($data)
    {
        return $this->select('duongdan')->where($data)->orderBy('id', 'asc')->first()->toArray();
    }
}
