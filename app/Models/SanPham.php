<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;

class SanPham extends Base
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'sanpham';
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    public function getList()
    {
        $results = DB::select("select sanpham.`id`, `tensp`, `tenloaisp`, `maloaisp`, 
                `dacta`, `gia`, sanpham.`hinh`, 
                `soluongviews`, `soluongban`, `giamgia`, `ngaykt`, `created` 
                from `sanpham`, `loaisanpham`
                where sanpham.`isdeleted` = 0 and loaisanpham.`id` = `maloaisp` and `maloaisp` <> 0
                order by sanpham.id desc");

        return json_decode(json_encode($results), true);
    }

    public function getTop($orderBy)
    {
        return $this->select(['sanpham.id', 'tensp', 'gia', 'sanpham.hinh', 'giamgia', 'ngaykt', 'tenloaisp'])
            ->join(
                'loaisanpham',
                'loaisanpham.id', '=', 'sanpham.maloaisp'
            )
            ->where(['sanpham.isdeleted' => 0])
            ->where('sanpham.maloaisp', '<>', 0)
            ->orderBy('sanpham.' . $orderBy, 'desc')
            ->limit(8)
            ->get()
            ->toArray();
    }

    public function getByCatID($id, $page = 1, $limit = 12)
    {
        return $this->select(['sanpham.id', 'tensp', 'gia', 'sanpham.hinh', 'giamgia', 'ngaykt', 'dacta', 'tenloaisp'])
            ->join(
                'loaisanpham',
                'loaisanpham.id', '=', 'sanpham.maloaisp'
            )
            ->where(['sanpham.isdeleted' => 0, 'maloaisp' => $id])
            ->orderBy('sanpham.id', 'desc')
            ->paginate($limit, ['*'], 'page', $page)
            ->toArray();
    }

    public function getByKeyWords($keywords, $page = 1)
    {
        return $this->select(['sanpham.id', 'tensp', 'gia', 'sanpham.hinh', 'giamgia', 'ngaykt', 'tenloaisp'])
            ->join(
                'loaisanpham',
                'loaisanpham.id', '=', 'sanpham.maloaisp'
            )
            ->where(['sanpham.isdeleted' => 0])
            ->where('sanpham.maloaisp', '<>', 0)
            ->where(function ($q) use ($keywords) {
                $q->where('dacta', 'like', "%$keywords%")
                    ->orWhere('tensp', 'like', "%$keywords%")
                    ->orWhere('tenloaisp', 'like', "%$keywords%")
                    ->orWhere('tenloaisp0', 'like', "%$keywords%");
            })
            ->orderBy('sanpham.id', 'desc')
            ->paginate(12, ['*'], 'page', $page)
            ->toArray();
    }

    public function getDiscount($page = 1)
    {
        return $this->select(['sanpham.id', 'tensp', 'gia', 'sanpham.hinh', 'giamgia', 'ngaykt', 'tenloaisp', 'tenloaisp0'])
            ->join(
                'loaisanpham',
                'loaisanpham.id', '=', 'sanpham.maloaisp'
            )
            ->where(['sanpham.isdeleted' => 0])
            ->where('giamgia', '>', 0)
            ->where('ngaykt', '>=', date('Y/m/d H:i:s'))
            ->orderBy('sanpham.id', 'desc')
            ->paginate(12, ['*'], 'page', $page)
            ->toArray();
    }

    public function getByIDOrName($id, $proName)
    {
        $item = $this->select(['sanpham.id', 'tensp', 'gia', 'sanpham.hinh', 'giamgia', 'dacta', 'ngaykt', 'tenloaisp', 'maloaisp'])
            ->join(
                'loaisanpham',
                'loaisanpham.id', '=', 'sanpham.maloaisp'
            )
            ->where(['sanpham.isdeleted' => 0, 'sanpham.id' => $id])
            ->orWhere('sanpham.tensp', 'like', "%$proName%")
            ->first();
        if ($item) {
            return $item->toArray();
        }
        return [];
    }
}
