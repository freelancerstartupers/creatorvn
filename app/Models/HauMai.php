<?php

namespace App\Models;

class HauMai extends Base
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'haumai';
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * @param $mahm
     * @return array
     */
    public function checkVaild($mahm)
    {
        $item = $this->select(['id','phantram'])
            ->where(['mahm' => $mahm])
            ->where(['dasudung' => 0])
            ->where('ngaykt', '>=', date('Y/m/d H:i:s'))
            ->get()->toArray();
        if ($item) {
            return $item[0];
        }
        return [];
    }
}
