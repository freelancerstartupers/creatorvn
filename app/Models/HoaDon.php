<?php

namespace App\Models;

class HoaDon extends Base
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'hoadon';
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
}
