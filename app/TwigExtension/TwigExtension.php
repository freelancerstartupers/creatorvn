<?php

namespace App\TwigExtension;

use Illuminate\Support\Facades\URL;

class TwigExtension extends \Twig_Extension
{
    public function __construct()
    {

    }

    public function getName()
    {
        return 'TwigExtensions';
    }

    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction('test_twig_function', array($this, 'testTwigFunction')),
            new \Twig_SimpleFunction('base_url', array($this, 'baseUrl')),
            new \Twig_SimpleFunction('check_interger', array($this, 'checkInterger')),
        ];
    }

    public function testTwigFunction()
    {
        return "Tested";
    }

    public function baseUrl()
    {
        return URL::to('/');
    }

    public function checkInterger($var)
    {
        return is_numeric($var);
    }
}