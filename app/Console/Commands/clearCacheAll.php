<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Log;

class clearCacheAll extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'allcache:clear';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This is command clearing all cache in server.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $exitCode1 = Artisan::call('cache:clear');
        $exitCode2 = Artisan::call('view:clear');
        $exitCode3 = Artisan::call('config:clear');
        $exitCode4 = Artisan::call('twig:clean');
        $exitCode5 = Artisan::call('route:clear');
        $exitCode6 = Artisan::call('clear-compiled');

        Log::channel('cron')->info(sprintf("Clear: cache: $exitCode1, view: $exitCode2, config: $exitCode3, twig: $exitCode4, route: $exitCode5, compiled: $exitCode6 at %s", date('d.m.Y H:i:s')));
    }
}
