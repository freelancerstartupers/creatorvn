//Gọi API xử lý
$('#loaisanpham_tab').on('click', function (e) {
    e.preventDefault();
    setLoaiSanPham();
});

$('#sanpham_tab').on('click', function (e) {
    e.preventDefault();
    setSanPham();
});

$('#taikhoan_tab').on('click', function (e) {
    e.preventDefault();
    setTaiKhoan();
});

$('#haumai_tab').on('click', function (e) {
    e.preventDefault();
    setHauMai();
});

$('#baiviet_tab').on('click', function (e) {
    e.preventDefault();
    setBaiViet();
});

$('#hoadon_tab').on('click', function (e) {
    e.preventDefault();
    $('#hoadon-chitiethoadon').hide();
    setHoaDon();
});

$('#gioithieu_tab').on('click', function (e) {
    e.preventDefault();
    setGioiThieu();
});

$('#vechungtoi_tab').on('click', function (e) {
    e.preventDefault();
    setVeChungToi();
});

$('#chinhsach_tab').on('click', function (e) {
    e.preventDefault();
    setChinhSach();
});

$('#theme_tab').on('click', function (e) {
    e.preventDefault();
    setTheme();
});

$('#sanphamthucte_tab').on('click', function (e) {
    e.preventDefault();
    setSanPhamThucTe();
});

$('#dichvu_tab').on('click', function (e) {
    e.preventDefault();
    setDichVu();
});

$('#hotro_tab').on('click', function (e) {
    e.preventDefault();
    setHoTro();
});

Date.prototype.toDateTimeLocal =
    function toDateTimeLocal() {
        var date = this,
            ten = function (i) {
                return (i < 10 ? '0' : '') + i;
            }
        yyyy = date.getFullYear(),
            MM = ten(date.getMonth() + 1),
            dd = ten(date.getDate()),
            hh = ten(date.getHours()),
            mm = ten(date.getMinutes()),
            ss = ten(date.getSeconds());
        return yyyy + '-' + MM + '-' + dd + 'T' + hh + ':' + mm + ':' + ss;
    };

function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

$(document).ready(function () {
    setHoaDon(true);
    let options = {
        animationEnabled: true,
        title: {
            text: "DOANH THU TRONG TOP 10 NGÀY"
        },
        axisY: {
            title: "Triệu Đồng(VNĐ)",
            suffix: "",
            includeZero: false
        },
        axisX: {
            title: "NGÀY"
        },
        data: [{
            type: "column",
            yValueFormatString: "#,##0.0#" % "",
            dataPoints: [{
                label: "20/02",
                y: 100.09
            }, {
                label: "03/05",
                y: 90.40
            }, {
                label: "04/05",
                y: 80.50
            }, {
                label: "05/05",
                y: 7.96
            }, {
                label: "12/05",
                y: 200.80
            }, {
                label: "21/05",
                y: 70.56
            }, {
                label: "25/05",
                y: 10.20
            }, {
                label: "02/06",
                y: 7.1
            }, {
                label: "07/06",
                y: 92.12
            }, {
                label: "12/06",
                y: 15.12
            },]
        }]
    };
    $("#benefits").CanvasJSChart(options);

    tinymce.init({
        mode: "specific_textareas",
        editor_selector: /(fullWidth)/,
        width: 1200,
        height: 1200,
        menubar: true,
        theme: 'modern',
        fontsize_formats: "8pt 10pt 12pt 14pt 18pt 24pt 36pt",
        plugins: 'print preview fullpage powerpaste searchreplace autolink directionality advcode visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists textcolor wordcount tinymcespellchecker a11ychecker imagetools mediaembed  linkchecker contextmenu colorpicker textpattern help',
        toolbar1: 'formatselect | bold italic strikethrough forecolor backcolor | fontselect |  fontsizeselect | link | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent  | removeformat | image code ',
        image_advtab: true,
        content_css: [
            'fonts/familyLato300300i400400i.css',
            'css/codepen.min.css'
        ],
        images_upload_handler: function (blobInfo, success, failure) {
            var data = new FormData();
            data.append('FILE', blobInfo.blob());
            $.ajax({
                type: 'POST',
                url: 'uploadextend',
                processData: false,
                contentType: false,
                cache: false,
                dataType: 'json',
                data: data,
                success: function (result) {
                    success(result.data.duongdan);
                }
            });
        },
        images_upload_credentials: true
    });

    tinymce.init({
        selector: 'textarea',
        width: 1000,
        height: 250,
        menubar: true,
        theme: 'modern',
        fontsize_formats: "8pt 10pt 12pt 14pt 18pt 24pt 36pt",
        plugins: 'print preview fullpage powerpaste searchreplace autolink directionality advcode visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists textcolor wordcount tinymcespellchecker a11ychecker imagetools mediaembed  linkchecker contextmenu colorpicker textpattern help',
        toolbar1: 'formatselect | bold italic strikethrough forecolor backcolor | fontselect |  fontsizeselect | link | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent  | removeformat ',
        image_advtab: true,
        content_css: [
            'fonts/familyLato300300i400400i.css',
            'css/codepen.min.css'
        ]
    }).then(() => {
        $('.mce-notification').remove();
    });
});


// Loại sản phầm
$('#loaisanpham-them-Btn').click(function (e) {
    e.preventDefault();
    editLoaiSanPham('insert');
});

$('#loaisanpham-xoa-Btn').click(function (e) {
    e.preventDefault();
    editLoaiSanPham('delete');
});

$('#loaisanpham-sua-Btn').click(function (e) {
    e.preventDefault();
    editLoaiSanPham('update');
});
// Sản phẩm
$('#sanpham-them-Btn').click(function (e) {
    e.preventDefault();
    editSanPham('insert');
});

$('#sanpham-xoa-Btn').click(function (e) {
    e.preventDefault();
    editSanPham('delete');
});

$('#sanpham-sua-Btn').click(function (e) {
    e.preventDefault();
    editSanPham('update');
});
// Tài khoản
$('#taikhoan-them-Btn').click(function (e) {
    e.preventDefault();
    editTaiKhoan('insert');
});

$('#taikhoan-sua-Btn').click(function (e) {
    e.preventDefault();
    editTaiKhoan('update');
});

$('#taikhoan-xoa-Btn').click(function (e) {
    e.preventDefault();
    editTaiKhoan('delete');
});
//Hậu mãi
$('#haumai-them-Btn').click(function (e) {
    e.preventDefault();
    editHauMai('insert');
});

$('#haumai-sua-Btn').click(function (e) {
    e.preventDefault();
    editHauMai('update');
});

$('#haumai-xoa-Btn').click(function (e) {
    e.preventDefault();
    editHauMai('delete');
});
//Hậu mãi
$('#baiviet-them-Btn').click(function (e) {
    e.preventDefault();
    editBaiViet('insert');
});

$('#baiviet-sua-Btn').click(function (e) {
    e.preventDefault();
    editBaiViet('update');
});

$('#baiviet-xoa-Btn').click(function (e) {
    e.preventDefault();
    editBaiViet('delete');
});
//Hóa đơn
$('#hoadon-sua-Btn').click(function (e) {
    e.preventDefault();
    editHoaDon('update');
});

$('#hoadon-xoa-Btn').click(function (e) {
    e.preventDefault();
    editHoaDon('delete');
});

//Giới thiệu
$('#gioithieu-edit-Btn').click(function (e) {
    e.preventDefault();
    editGioiThieu();
});

//Về chúng tôi
$('#vechungtoi-edit-Btn').click(function (e) {
    e.preventDefault();
    editVeChungToi();
});

//Chính sách
$('#chinhsach-edit-Btn').click(function (e) {
    e.preventDefault();
    editChinhSach();
});

//Theme
$('#theme-them-Btn').click(function (e) {
    e.preventDefault();
    editTheme('insert');
});

$('#theme-sua-Btn').click(function (e) {
    e.preventDefault();
    editTheme('update');
});

$('#theme-xoa-Btn').click(function (e) {
    e.preventDefault();
    editTheme('delete');
});

//Sản phẩm thực tế
$('#sanphamthucte-them-Btn').click(function (e) {
    e.preventDefault();
    editSanPhamThucTe('insert');
});

$('#sanphamthucte-sua-Btn').click(function (e) {
    e.preventDefault();
    editSanPhamThucTe('update');
});

$('#sanphamthucte-xoa-Btn').click(function (e) {
    e.preventDefault();
    editSanPhamThucTe('delete');
});

//Dịch vụ
$('#dichvu-keyApi-Btn').click(function (e) {
    e.preventDefault();
    editDichVu();
});

//Hỗ trợ
$('#hotro-sua-Btn').click(function (e) {
    e.preventDefault();
    editHoTro();
});

//product
$('#optimze-product').click(function (e) {
    e.preventDefault();
    var count = parseFloat($(this).data('count'));
    for (var i = 0; i < count; i += 20) {
        $.ajax({
                type: "GET",
                url: "optimze-imgs-all?mode=product&offset=" + i,
                cache: true,
                success: function (result) {
                    if (i >= count) {
                        setDichVu();
                    }
                }
            }
        );
    }
});

//theme
$('#optimze-theme').click(function (e) {
    e.preventDefault();
    var count = parseFloat($(this).data('count'));
    for (var i = 0; i < count; i += 20) {
        $.ajax({
                type: "GET",
                url: "optimze-imgs-all?mode=theme&offset=" + i,
                cache: true,
                success: function (result) {
                    if (i >= count) {
                        setDichVu();
                    }
                }
            }
        );
    }
});

//category
$('#optimze-category').click(function (e) {
    e.preventDefault();
    var count = parseFloat($(this).data('count'));
    for (var i = 0; i < count; i += 20) {
        $.ajax({
                type: "GET",
                url: "optimze-imgs-all?mode=category&offset=" + i,
                cache: true,
                success: function (result) {
                    if (i >= count) {
                        setDichVu();
                    }
                }
            }
        );
    }
});

//practical
$('#optimze-practical').click(function (e) {
    e.preventDefault();
    var count = parseFloat($(this).data('count'));
    for (var i = 0; i < count; i += 20) {
        $.ajax({
                type: "GET",
                url: "optimze-imgs-all?mode=practical&offset=" + i,
                cache: true,
                success: function (result) {
                    if (i >= count) {
                        setDichVu();
                    }
                }
            }
        );
    }
});

//blog
$('#optimze-blog').click(function (e) {
    e.preventDefault();
    var count = parseFloat($(this).data('count'));
    for (var i = 0; i < count; i += 20) {
        $.ajax({
                type: "GET",
                url: "optimze-imgs-all?mode=blog&offset=" + i,
                cache: true,
                success: function (result) {
                    if (i >= count) {
                        setDichVu();
                    }
                }
            }
        );
    }
});

//blog
$('#optimze-extend').click(function (e) {
    e.preventDefault();
    var count = parseFloat($(this).data('count'));
    for (var i = 0; i < count; i += 20) {
        $.ajax({
                type: "GET",
                url: "optimze-imgs-all?mode=extend&offset=" + i,
                cache: true,
                success: function (result) {
                    if (i >= count) {
                        setDichVu();
                    }
                }
            }
        );
    }
});

//remove unused images
$('#remove-unused-images').click(function (e) {
    e.preventDefault();
    $.ajax({
            type: "GET",
            url: "remove-unused-imgs",
            cache: true,
            success: function () {
                window.location = "/admin";
            }
        }
    );
});

//optimze images
$('#optimze-images').click(function (e) {
    e.preventDefault();
    let data = new FormData();
    let FILES = $('#optimze-hinh-Files').get(0).files;
    if (FILES.length > 0) {
        for (var index = 0; index < FILES.length; index++) {
            const element = FILES[index];
            data.append(index, element);
        }
    } else {
        alert('Vui lòng thêm hình để tối ưu.')
        return false;
    }

    $.ajax({
            type: "POST",
            url: "optimize-imgs",
            processData: false,
            contentType: false,
            cache: false,
            dataType: 'json',
            data: data,
            success: function (result) {
                clearCache();
                setDichVu();
                $('#link-files-zip').attr('href', result.data.urlZip);
                $('#link-files-zip').text('Click to download ' + result.data.urlZip + ' - ' + result.data.fileSize + ' MB');
            }
        }
    );
});


$('#sanpham-hinh-File,#loaisanpham-hinh-File,#baiviet-hinh-File').change(function (event) {
    /* Act on the event */
    let id = event.target.id;
    if ($(this).get(0).files && $(this).get(0).files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            if (id == "sanpham-hinh-File") {
                $('#sanpham-hinh-Img').attr('src', e.target.result);
            } else if (id == "loaisanpham-hinh-File") {
                $('#loaisanpham-hinh-Img').attr('src', e.target.result);
            } else {
                $('#baiviet-hinh-Img').attr('src', e.target.result);
            }
        }
        reader.readAsDataURL($(this).get(0).files[0]);
    }
});

function clearCache() {
    $('#hinh-list').html('');
    $('#hinh-list-sanphamthucte').html('');
    $('.data-fix').attr('href', '');
    $('.data-fix').html('');
    $('.data-fix').val("");
    $('.data-fix').attr("src", "");
    tinymce.get('sanpham-dacta-TextArea').setContent('');
    tinymce.get('baiviet-noidung-TextArea').setContent('');
    tinymce.get('baiviet-chitiet-TextArea').setContent('');
    tinymce.get('theme-noidung-TextArea').setContent('');
    tinymce.get('sanphamthucte-dacta-TextArea').setContent('');
    tinymce.get('hotro-noidung-TextArea').setContent('');
};

$('table').click(function (e) {

    $('table tbody tr').each(function (a, b) {
        $(b).on('click', function () {
            $('table tbody tr').css('background', '');
            $(this).css('background', '#655a5adb');
        });
    });
    $('td').on("click", function (e) {
        let idTable = $(this).closest('table').attr('id');
        if (idTable == "hoadon-table") {
            setChiTietHoaDon($(this).parent().find(".hoadon-id").text());
        }
        switch (idTable) {
            case "hoadon-table":
                $(`#hoadon-tinhtrang-Select`).val($(this).parent().find(".hoadon-tinhtrang").data("tinhtrang"));
                $('#hoadon-ngaygiao-Date').val(new Date($(this).parent().find(".hoadon-ngaygiao").text()).toDateTimeLocal());

            case "sanpham-table":
                $(`#sanpham-tenloaisp-Select`).val($(this).parent().find(".sanpham-tenloaisp").data("maloaisp"));
                $('#sanpham-hinh-Link').attr("href", $.trim($(this).parent().find(".sanpham-hinh").data("hinh")));
                $('#sanpham-hinh-Link').html($.trim($(this).parent().find(".sanpham-hinh").text()));
                tinymce.get('sanpham-dacta-TextArea').setContent($.trim($(this).parent().find(".sanpham-dacta").html()));
                $('#sanpham-ngaykt-Date').val(new Date($(this).parent().find(".sanpham-ngaykt").text()).toDateTimeLocal());

            case "loaisanpham-table":
                $('#loaisanpham-hinh-Img').attr("src", $.trim($(this).parent().find(".loaisanpham-hinh").text()));
                $('#loaisanpham-hinh-Link').attr("href", $.trim($(this).parent().find(".loaisanpham-hinh").data("hinh")));
                $('#loaisanpham-hinh-Link').html($.trim($(this).parent().find(".loaisanpham-hinh").text()));

            case "taikhoan-table":
                $(`#taikhoan-vaitro-Select`).val($(this).parent().find(".taikhoan-vaitro").data("vaitro"));

            case "haumai-table":
                $(`#haumai-dasudung-Select`).val($(this).parent().find(".haumai-dasudung").data("dasudung"));
                $(`#haumai-ngaykt-Date`).val(new Date($(this).parent().find(".haumai-ngaykt").text()).toDateTimeLocal());

            case "baiviet-table":
                let media = $(this).parent().find(".baiviet-media").data("media");
                $(`#baiviet-media-Select`).val(media);
                if (media == '0') {
                    $('#baiviet-img').attr('hidden', false);
                    $('#baiviet-duongdan').attr('hidden', true);
                    $('#baiviet-hinh-Link').attr("href", $.trim($(this).parent().find(".baiviet-duongdan").text()));
                    $('#baiviet-hinh-Link').html($.trim($(this).parent().find(".baiviet-duongdan").text()).split('/').slice(-1).pop());
                    $('#baiviet-hinh-Img').attr("src", $.trim($(this).parent().find(".baiviet-duongdan").text()));
                } else {
                    $('#baiviet-img').attr('hidden', true);
                    $('#baiviet-duongdan').attr('hidden', false);
                }
                tinymce.get('baiviet-noidung-TextArea').setContent($.trim($(this).parent().find(".baiviet-noidung").html()));
                tinymce.get('baiviet-chitiet-TextArea').setContent($.trim($(this).parent().find(".baiviet-chitiet").html()));

            case "theme-table":
                $(`#theme-hienthi-Select`).val($(this).parent().find(".theme-hienthi").data("hienthi"));
                $('#theme-hinh-Link').attr("href", $.trim($(this).parent().find(".theme-hinh").data("hinh")));
                $('#theme-hinh-Link').html($.trim($(this).parent().find(".theme-hinh").text()));
                tinymce.get('theme-noidung-TextArea').setContent($.trim($(this).parent().find(".theme-noidung").html()));

            case "hotro-table":
                $('#hotro-hinh-Link').attr("href", $.trim($(this).parent().find(".hotro-hinh").data("hinh")));
                $('#hotro-hinh-Link').html($.trim($(this).parent().find(".hotro-hinh").text()));
                tinymce.get('hotro-noidung-TextArea').setContent($.trim($(this).parent().find(".hotro-noidung").html()));

            case "sanphamthucte-table":
                tinymce.get('sanphamthucte-dacta-TextArea').setContent($.trim($(this).parent().find(".sanphamthucte-dacta").html()));

            default:
                let a = $(this).parent().children();
                for (var i = 0; i < a.length; i++) {
                    let id = '#' + a[i].className.split(" ")[0] + '-Txt';
                    let classname = '.' + a[i].className.split(" ")[0];
                    $(id).val($.trim($(this).parent().find(classname).text()));
                }
                break;
        }
        if (idTable == "sanpham-table") {
            setHinh();
        }
        if (idTable == "sanphamthucte-table") {
            setHinh(true);
        }
    });
});

$('#logout-submit').click(function (e) {
    e.preventDefault();
    $.ajax({
        type: "DELETE",
        url: 'authen',
        cache: true,
        success: function (result) {
            window.location.href = 'login';
        }
    })
});

function setHoTro() {
    $('#hotro-table').DataTable().destroy();
    $.ajax({
        type: "GET",
        url: 'hotro',
        cache: true,
        success: function (result) {
            if (result.status) {
                let data = result.data;
                let htmlData = '';
                for (let i = 0; i < data.length; i++) {
                    htmlData += `
                        <tr>
                        <td class="hotro-id" >${data[i].id}</td>
                        <td class="hotro-tieude" >${data[i].tieude}</td>
                        <td class="hotro-noidung" >${data[i].noidung}</th>
                        <td class="hotro-hinh" data-hinh="${data[i].hinh}" >${data[i].hinh.split('/').slice(-1).pop()}</th>
                        </tr>
                        `;
                }
                clearCache();
                $('#hotro-table tbody').html(htmlData);
                $('#hotro-table').DataTable();
            }
        }
    });
}

function editHoTro() {
    let data = new FormData();
    tinyMCE.triggerSave();
    data.append('noidung', $.trim($('#hotro-noidung-TextArea').val()));
    data.append('tieude', $.trim($('#hotro-tieude-Txt').val()));
    let id = $.trim($('#hotro-id-Txt').val());
    if (!id) {
        alert("Vui lòng chọn 1 help!");
        return;
    }
    data.append('id', id);
    id = '?id=' + id;

    let FILE = $('#hotro-hinh-File').get(0).files;
    if (FILE.length > 0) {
        data.append('FILE', FILE[0]);
    }

    $.ajax({
        type: "POST",
        url: 'hotro' + id,
        processData: false,
        contentType: false,
        cache: false,
        dataType: 'json',
        data: data,
        success: function (result) {
            if (!result.status) {
                alert(result.msg);
            }
            setHoTro();
        }
    });
}

function setTheme() {
    $('#theme-table').DataTable().destroy();
    $.ajax({
        type: "GET",
        url: 'theme',
        cache: true,
        success: function (result) {
            if (result.status) {
                let data = result.data;
                let htmlData = '';
                for (let i = 0; i < data.length; i++) {
                    let hienthi = (data[i].hienthi == 1) ? 'Có' : 'Không';
                    htmlData += `
                        <tr>
                        <td class="theme-id" >${data[i].id}</td>
                        <td class="theme-hinh" data-hinh="${data[i].hinh}" >${data[i].hinh.split('/').slice(-1).pop()}</th>
                        <td class="theme-noidung" >${data[i].noidung}</th>
                        <td class="theme-hienthi" data-hienthi="${ data[i].hienthi }">${ hienthi }</th>
                        </tr>
                        `;
                }
                clearCache();
                $('#theme-table tbody').html(htmlData);
                $('#theme-table').DataTable();
            }
        }
    });
}

function editTheme(mode) {
    let data = new FormData();
    tinyMCE.triggerSave();
    data.append('noidung', $.trim($('#theme-noidung-TextArea').val()));
    data.append('hienthi', $.trim($('#theme-hienthi-Select').val()));
    let FILE = $('#theme-hinh-File').get(0).files;
    let typeMethod = "DELETE";
    let id = '';
    if (mode == 'update' || mode == 'delete') {
        id = $.trim($('#theme-id-Txt').val());
        if (!id) {
            alert("Vui lòng chọn 1 theme!");
            return;
        }
        data.append('id', id);
        id = '?id=' + id;
    }
    if (mode == 'insert' || mode == 'update') {
        if (FILE.length == 0 && mode == 'insert') {
            alert("Vui upload file ảnh cho theme!");
            return;
        }
        if (FILE.length > 0) {
            data.append('FILE', FILE[0]);
        }
        typeMethod = "POST";
    }
    $.ajax({
        type: typeMethod,
        url: 'theme' + id,
        processData: false,
        contentType: false,
        cache: false,
        dataType: 'json',
        data: data,
        success: function (result) {
            if (!result.status) {
                alert(result.msg);
            }
            setTheme();
        }
    });
}

function setGioiThieu() {
    $.ajax({
        type: "GET",
        url: 'gioithieu',
        cache: true,
        success: function (result) {
            if (result.status) {
                tinymce.get('gioithieu-noidung-TextArea').setContent(result.data.content);
            }
        }
    });
}

function editGioiThieu() {
    let data = new FormData();
    tinyMCE.triggerSave();
    data.append('content', $('#gioithieu-noidung-TextArea').val());
    $.ajax({
        type: "POST",
        url: 'gioithieu',
        processData: false,
        contentType: false,
        cache: false,
        dataType: 'json',
        data: data,
        success: function (result) {
            if (result.status) {
                alert('Success: Edit Intro');
            }
        }
    });
}

function setVeChungToi() {
    $.ajax({
        type: "GET",
        url: 'vechungtoi',
        cache: true,
        success: function (result) {
            if (result.status) {
                tinymce.get('vechungtoi-noidung-TextArea').setContent(result.data.content);
            }
        }
    });
}

function editVeChungToi() {
    let data = new FormData();
    tinyMCE.triggerSave();
    data.append('content', $('#vechungtoi-noidung-TextArea').val());
    $.ajax({
        type: "POST",
        url: 'vechungtoi',
        processData: false,
        contentType: false,
        cache: false,
        dataType: 'json',
        data: data,
        success: function (result) {
            if (result.status) {
                alert('Success: Edit About Us');
            }
        }
    });
}

function setChinhSach() {
    $.ajax({
        type: "GET",
        url: 'chinhsach',
        cache: true,
        success: function (result) {
            if (result.status) {
                tinymce.get('chinhsach-noidung-TextArea').setContent(result.data.content);
            }
        }
    });
}

function editChinhSach() {
    let data = new FormData();
    tinyMCE.triggerSave();
    data.append('content', $('#chinhsach-noidung-TextArea').val());
    $.ajax({
        type: "POST",
        url: 'chinhsach',
        processData: false,
        contentType: false,
        cache: false,
        dataType: 'json',
        data: data,
        success: function (result) {
            if (result.status) {
                alert('Success: Edit Policy');
            }
        }
    });
}

function setLoaiSanPham(isOptions = false) {
    if (!isOptions) {
        $('#loaisanpham-table').DataTable().destroy();
    }
    $.ajax({
        type: "GET",
        url: 'loaisanpham',
        cache: true,
        success: function (result) {
            if (result.status) {
                let data = result.data;
                let htmlData = '';
                let options = '';
                for (let i = 0; i < data.length; i++) {
                    if (!isOptions) {
                        htmlData += `
                        <tr>
                        <td class="loaisanpham-id" >${data[i].id}</td>
                        <td class="loaisanpham-tenloaisp0" >${data[i].tenloaisp0}</th>
                        <td class="loaisanpham-tenloaisp" >${data[i].tenloaisp}</th>
                        <td class="loaisanpham-hinh" data-hinh="${data[i].hinh}" >${data[i].hinh.split('/').slice(-1).pop()}</th>
                        </tr>
                        `;
                    } else {
                        options += `
                        <option value="${data[i].id}">${data[i].tenloaisp}</option>}
                        `;
                    }

                }
                clearCache();
                if (!isOptions) {
                    $('#loaisanpham-table tbody').html(htmlData);
                    $('#loaisanpham-table').DataTable({"order": [[0, "desc"]]});
                } else {
                    // Lưu biến option tại tap Sản Phẩm phần "Tên Loại sản phẩm"
                    $('#sanpham-tenloaisp-Select').html(options);
                }
            }
        }
    });
}

function editLoaiSanPham(mode) {
    let data = new FormData();
    let tenloaisp0 = $.trim($('#loaisanpham-tenloaisp0-Txt').val());
    let tenloaisp = $.trim($('#loaisanpham-tenloaisp-Txt').val());
    let FILE = $('#loaisanpham-hinh-File').get(0).files;
    let typeMethod = "DELETE";
    let id = '';
    if (mode == 'update' || mode == 'delete') {
        id = $.trim($('#loaisanpham-id-Txt').val());
        if (!id) {
            alert("Vui lòng chọn 1 loại sản phẩm!");
            return;
        }
        data.append('id', id);
        id = '?id=' + id;
    }
    if (mode == 'insert' || mode == 'update') {
        if (!tenloaisp) {
            alert("Vui lòng nhập tên loại sản phẩm!");
            return;
        }
        data.append('tenloaisp', tenloaisp);
        if (!tenloaisp0) {
            alert("Vui lòng nhập tên loại sản phẩm 0!");
            return;
        }
        data.append('tenloaisp0', tenloaisp0);
        if (FILE.length == 0 && mode == 'insert') {
            alert("Vui upload file ảnh cho loại sản phẩm!");
            return;
        }
        if (FILE.length > 0) {
            data.append('FILE', FILE[0]);
        }
        typeMethod = "POST";
    }
    $.ajax({
        type: typeMethod,
        url: 'loaisanpham' + id,
        processData: false,
        contentType: false,
        cache: false,
        dataType: 'json',
        data: data,
        success: function (result) {
            if (!result.status) {
                alert(result.msg);
            }
            setLoaiSanPham();
        }
    });
}

function setSanPham() {
    setLoaiSanPham(true);
    $('#sanpham-table').DataTable().destroy();
    $.ajax({
        type: "GET",
        url: 'sanpham',
        cache: true,
        success: function (result) {
            if (result.status) {
                let data = result.data;
                let htmlData = '';
                for (let i = 0; i < data.length; i++) {
                    htmlData = htmlData + `
                        <tr>
                        <td class="sanpham-created" >${data[i].created}</th>
                        <td class="sanpham-id" >${data[i].id}</td>
                        <td class="sanpham-tensp" >${data[i].tensp}</th>
                        <td class="sanpham-tenloaisp" data-maloaisp="${data[i].maloaisp}" >${data[i].tenloaisp}</th>
                        <td class="sanpham-hinh" data-hinh="${data[i].hinh}" >${data[i].hinh.split('/').slice(-1).pop()}</th>
                        <td class="sanpham-dacta" >${data[i].dacta}</th>
                        <td class="sanpham-gia" >${data[i].gia}</th>
                        <td class="sanpham-soluongviews" >${data[i].soluongviews}</th>
                        <td class="sanpham-soluongban" >${data[i].soluongban}</th>
                        <td class="sanpham-giamgia" >${data[i].giamgia}</th>
                        <td class="sanpham-ngaykt" >${data[i].ngaykt}</th>
                        </tr>
                        `;
                }
                clearCache();
                $('#sanpham-table tbody').html(htmlData);
                $('#sanpham-table').DataTable({
                    "order": [[0, "desc"]]
                });
            }
        }
    });
}

function editSanPham(mode) {
    let data = new FormData();

    data.append('maloaisp', $.trim($('#sanpham-tenloaisp-Select').val()));
    tinyMCE.triggerSave();
    data.append('dacta', $.trim($('#sanpham-dacta-TextArea').val()));

    let gia = parseFloat($.trim($('#sanpham-gia-Txt').val()));
    if (isNaN(gia) || gia <= 0) {
        gia = 0;
    }
    data.append('gia', gia);

    let soluongviews = parseFloat($.trim($('#sanpham-soluongviews-Txt').val()));
    if (isNaN(soluongviews) || soluongviews <= 0) {
        data.append('soluongviews', 0);
    } else {
        data.append('soluongviews', soluongviews);
    }


    let soluongban = parseFloat($.trim($('#sanpham-soluongban-Txt').val()));
    if (isNaN(soluongban) || soluongban <= 0) {
        data.append('soluongban', 0);
    } else {
        data.append('soluongban', soluongban);
    }

    let giamgia = parseFloat($.trim($('#sanpham-giamgia-Txt').val()));
    if (isNaN(giamgia) || (giamgia < 0 || giamgia > 100)) {
        data.append('giamgia', 0);
    } else {
        data.append('giamgia', giamgia);
    }

    let ngaykt = $.trim($('#sanpham-ngaykt-Date').val());
    if (!ngaykt) {
        data.append('ngaykt', new Date("2001-01-01").toDateTimeLocal());
    } else {
        data.append('ngaykt', new Date(ngaykt).toDateTimeLocal());
    }

    let tensp = $.trim($('#sanpham-tensp-Txt').val());
    let FILE = $('#sanpham-hinh-File').get(0).files;
    let typeMethod = "DELETE";
    let id = '';
    if (mode == 'update' || mode == 'delete') {
        id = $.trim($('#sanpham-id-Txt').val());
        if (!id) {
            alert("Vui lòng chọn 1 sản phẩm!");
            return;
        }
        data.append('id', id);
        id = '?id=' + id;
    }
    if (mode == 'insert' || mode == 'update') {
        if (!tensp) {
            alert("Vui lòng nhập tên sản phẩm!");
            return;
        }
        data.append('tensp', tensp);
        if (mode == 'insert') {
            data.append('created', new Date().toDateTimeLocal())
        }
        if (FILE.length == 0 && mode == 'insert') {
            alert("Vui upload file ảnh cho sản phẩm!");
            return;
        }
        if (FILE.length > 0) {
            data.append('FILE', FILE[0]);
        }
        typeMethod = "POST";
    }

    $.ajax({
        type: typeMethod,
        url: 'sanpham' + id,
        processData: false,
        contentType: false,
        cache: false,
        dataType: 'json',
        data: data,
        success: function (result) {
            if (!result.status) {
                alert(result.msg);
            } else {
                if (typeMethod == "POST") {
                    editHinh('upload', result.data.id);
                }
                setSanPham();
            }
        }
    });
}

function setHinh(sanphamthucte = false) {
    let masp = $.trim($('#sanpham-id-Txt').val());
    if (sanphamthucte) {
        masp = $.trim($('#sanphamthucte-id-Txt').val());
    }
    $.ajax({
        type: "GET",
        url: 'hinh?masp=' + masp,
        cache: true,
        success: function (result) {
            if (result.status) {
                let data = result.data;
                let htmlData = '';
                for (let i = 0; i < data.length; i++) {
                    htmlData += `
                    <tr id="hinh-${data[i].id}">
                    <td style="padding: 10px;">
                    <a target="_blank" rel="noopener noreferrer" href="${data[i].duongdan}">${data[i].tenhinh}</a>
                    </td> 
                    <td style="padding: 10px;">
                    <button style=" " type="button" class="btn btn-danger" onclick="editHinh('delete',${data[i].id});">Xóa</button>
                    </td>
                    </tr>
                        `;
                }
                if (sanphamthucte) {
                    $('#hinh-list-sanphamthucte').html(htmlData);
                } else {
                    $('#hinh-list').html(htmlData);
                }
            }
        }
    });
}

function editHinh(mode, id, sanphamthucte = false) {
    let data = new FormData();
    let typeMethod = "DELETE";
    let tmp = id;
    if (mode == 'upload') {
        data.append('masp', id);
        id = '';
        typeMethod = "POST";
        let FILES = $('#sanpham-hinh-Files').get(0).files;
        if (sanphamthucte) {
            FILES = $('#sanphamthucte-hinh-Files').get(0).files;
        }
        data.append('FILES', FILES[0]);
        if (FILES.length > 0) {
            for (var index = 0; index < FILES.length; index++) {
                const element = FILES[index];
                data.append(index, element);
            }
        } else {
            return;
        }
    } else {
        id = '?id=' + id;
    }
    if (sanphamthucte === true) {
        if (id != '') {
            id = id + '&practical=1'
        } else {
            id = '?practical=1'
        }
    }
    $.ajax({
        type: typeMethod,
        url: 'hinh' + id,
        processData: false,
        contentType: false,
        cache: false,
        dataType: 'json',
        data: data,
        success: function (result) {
            if (!result.status) {
                alert(result.msg);
            } else {
                if (result.data.delete) {
                    $('#hinh-' + tmp).remove();
                }
            }
        }
    });
}

function setTaiKhoan() {
    $('#taikhoan-table').DataTable().destroy();
    $.ajax({
        type: "GET",
        url: 'taikhoan',
        cache: true,
        success: function (result) {
            if (result.status) {
                let data = result.data;
                let htmlData = '';
                for (let i = 0; i < data.length; i++) {
                    let vaitro = data[i].vaitro == 1 ? "ADMIN" : "USER";
                    htmlData += `
                        <tr>
                        <td class="taikhoan-id" >${data[i].id}</td>
                        <td class="taikhoan-tenhienthi">${data[i].tenhienthi}</th>
                        <td class="taikhoan-matkhau">${data[i].matkhau}</th>
                        <td class="taikhoan-vaitro" data-vaitro="${data[i].vaitro}">${vaitro}</th>
                        </tr>
                        `;
                }
                clearCache();
                $('#taikhoan-table tbody').html(htmlData);
                $('#taikhoan-table').DataTable();
            }
        }
    });
}

function editTaiKhoan(mode) {
    let data = new FormData();
    let typeMethod = "DELETE";
    let id = '';
    if (mode == 'update' || mode == 'delete') {
        id = $.trim($('#taikhoan-id-Txt').val());
        if (!id) {
            alert("Vui lòng chọn 1 tài khoản!");
            return;
        }
        data.append('id', id);
        id = '?id=' + id;
    }
    if (mode == 'insert' || mode == 'update') {
        let tenhienthi = $.trim($('#taikhoan-tenhienthi-Txt').val());
        if (!tenhienthi) {
            alert("Vui lòng nhập tên hiển thị của tài khoản!");
            return;
        }
        data.append('tenhienthi', tenhienthi);

        let matkhau = $.trim($('#taikhoan-matkhau-Txt').val());
        if (!matkhau) {
            alert("Vui lòng nhập mật khẩu của tài khoản!");
            return;
        }
        data.append('matkhau', matkhau);

        let vaitro = $.trim($('#taikhoan-vaitro-Select').val());
        data.append('vaitro', vaitro);
        typeMethod = "POST";
    }

    $.ajax({
        type: typeMethod,
        url: 'taikhoan' + id,
        processData: false,
        contentType: false,
        cache: false,
        dataType: 'json',
        data: data,
        success: function (result) {
            if (!result.status) {
                alert(result.msg);
            }
            setTaiKhoan();
        }
    });
}

function setHauMai() {
    $('#haumai-table').DataTable().destroy();
    $.ajax({
        type: "GET",
        url: 'haumai',
        cache: true,
        success: function (result) {
            if (result.status) {
                let data = result.data;
                let htmlData = '';
                for (let i = 0; i < data.length; i++) {
                    let tinhtrang = data[i].dasudung == 1 ? "Đã sử dụng" : "Chưa sử dụng";
                    htmlData += `
                        <tr>
                        <td class="haumai-id" >${data[i].id}</td>
                        <td class="haumai-mahm">${data[i].mahm}</th>
                        <td class="haumai-phantram">${data[i].phantram}</th>
                        <td class="haumai-dasudung" data-dasudung="${data[i].dasudung}">${tinhtrang}</th>
                        <td class="haumai-ngaykt">${data[i].ngaykt}</th>
                        </tr>
                        `;
                }
                clearCache();
                $('#haumai-table tbody').html(htmlData);
                $('#haumai-table').DataTable();
            }
        }
    });
}

function editHauMai(mode) {
    let data = new FormData();

    let typeMethod = "DELETE";
    let id = '';
    if (mode == 'update' || mode == 'delete') {
        id = $.trim($('#haumai-id-Txt').val());
        if (!id) {
            alert("Vui lòng chọn 1 hậu mãi!");
            return;
        }
        data.append('id', id);
        id = '?id=' + id;
    }
    if (mode == 'insert' || mode == 'update') {
        data.append('dasudung', $.trim($('#haumai-dasudung-Select').val()));

        let phantram = parseFloat($.trim($('#haumai-phantram-Txt').val()));
        if (isNaN(phantram) || (phantram <= 0 || phantram > 100)) {
            alert('Vui lòng nhập giảm giá > 0 tới 100');
            return;
        }
        data.append('phantram', phantram);

        let ngaykt = $.trim($('#haumai-ngaykt-Date').val());

        if (!ngaykt) {
            alert('Vui lòng nhập ngày kết thúc theo định dạnh dd/mm/yy hh:mm s');
            return;
        }
        data.append('ngaykt', new Date(ngaykt).toDateTimeLocal());

        typeMethod = "POST";
    }

    $.ajax({
        type: typeMethod,
        url: 'haumai' + id,
        processData: false,
        contentType: false,
        cache: false,
        dataType: 'json',
        data: data,
        success: function (result) {
            if (!result.status) {
                alert(result.msg);
            }
            setHauMai();
        }
    });
}

$('#baiviet-media-Select').on('change', function () {
    let media = $('#baiviet-img');
    let duongdan = $('#baiviet-duongdan');
    if (this.value == '0') {
        media.show();
        duongdan.hide();
    } else {
        media.hide();
        duongdan.show();
    }
    ;
})

function setBaiViet() {
    $('#baiviet-table').DataTable().destroy();
    $.ajax({
        type: "GET",
        url: 'baiviet',
        cache: true,
        success: function (result) {
            if (result.status) {
                let data = result.data;
                let htmlData = '';
                for (let i = 0; i < data.length; i++) {
                    let media = data[i].media == 1 ? "Video" : "Ảnh";
                    htmlData += `
                        <tr>
                        <td class="baiviet-id" >${data[i].id}</td>
                        <td class="baiviet-tieude">${data[i].tieude}</th>
                        <td class="baiviet-noidung">${data[i].noidung}</th>
                        <td class="baiviet-chitiet">${data[i].chitiet}</th>
                        <td class="baiviet-media" data-media="${data[i].media}">${media}</th>
                        <td class="baiviet-duongdan">${data[i].duongdan}</th>
                        <td class="baiviet-link">${data[i].link}</th>
                        </tr>
                        `;
                }
                clearCache();
                $('#baiviet-table tbody').html(htmlData);
                $('#baiviet-table').DataTable();
            }
        }
    });
}

function editBaiViet(mode) {
    let data = new FormData();

    let typeMethod = "DELETE";
    let id = '';
    if (mode == 'update' || mode == 'delete') {
        id = $.trim($('#baiviet-id-Txt').val());
        if (!id) {
            alert("Vui lòng chọn 1 bài viết!");
            return;
        }
        data.append('id', id);
        id = '?id=' + id;
    }
    if (mode == 'insert' || mode == 'update') {
        let tieude = $.trim($('#baiviet-tieude-Txt').val());
        if (!tieude) {
            alert('Vui lòng nhập tiêu đề cho bài viết');
            return;
        }
        data.append('tieude', tieude);

        tinyMCE.triggerSave();
        let noidung = $.trim($('#baiviet-noidung-TextArea').val());
        data.append('noidung', noidung);
        let chitiet = $.trim($('#baiviet-chitiet-TextArea').val());
        data.append('chitiet', chitiet);

        let media = $.trim($('#baiviet-media-Select').val())
        data.append('media', media);

        if (media == '0') // ảnh
        {
            let FILE = $('#baiviet-hinh-File').get(0).files;
            if (FILE.length == 0 && mode == 'insert') {
                alert("Vui upload file ảnh cho sản phẩm!");
                return;
            }
            if (FILE.length > 0) {
                data.append('FILE', FILE[0]);
            }
        } else {
            let duongdan = $.trim($('#baiviet-duongdan-Txt').val());
            if (!duongdan) {
                alert('Vui lòng nhập đường dẫn video cho bài viết');
                return;
            }
            data.append('duongdan', duongdan);
        }
        data.append('link', $.trim($('#baiviet-link-Txt').val()));
        typeMethod = "POST";
    }

    $.ajax({
        type: typeMethod,
        url: 'baiviet' + id,
        processData: false,
        contentType: false,
        cache: false,
        dataType: 'json',
        data: data,
        success: function (result) {
            if (!result.status) {
                alert(result.msg);
            }
            setBaiViet();
        }
    });
}

function setChiTietHoaDon(mahd, tinhtrang) {
    $('#hoadon-chitiethoadon').show();
    $('#chitiethoadon-table').DataTable().destroy();
    $.ajax({
        type: "GET",
        url: 'chitiethoadon?mahd=' + mahd,
        cache: true,
        success: function (result) {
            if (result.status) {
                let data = result.data;
                let htmlData = '';
                for (let i = 0; i < data.length; i++) {
                    htmlData += `
                        <tr>
                        <td class="chitiethoadon-id" >${data[i].id}</td>
                        <td class="chitiethoadon-tensp">${data[i].tensp}</th>
                        <td class="chitiethoadon-gia">${data[i].gia}</th>
                        <td class="chitiethoadon-soluong">${data[i].soluong}</th>
                        </tr>
                        `;
                }
                $('#chitiethoadon-table tbody').html(htmlData);
                $('#chitiethoadon-table').DataTable();
            }
        }
    });
    $(`#hoadon-tinhtrang-Select`).val();
    $(`#hoadon-id-Txt`).val(mahd);

}

function setHoaDon(getUnSeen = false) {
    if (!getUnSeen) {
        $('#hoadon-table').DataTable().destroy();
        $('#hoadon-id-Txt').val('');
        $('#hoadon-ngaygiao-Date').val('');
        $('#hoadon-chitiethoadon').hide();
    }
    $.ajax({
        type: "GET",
        url: 'hoadon',
        cache: true,
        success: function (result) {
            if (result.status) {
                let data = result.data;
                let htmlData = '';
                let count = 0;
                for (let i = 0; i < data.length; i++) {
                    data[i].tinhtrang == '0' ? count++ : null;
                    if (!getUnSeen) {
                        let tinhtrang = data[i].tinhtrang == 0 ? "Chưa giao" : (data[i].tinhtrang == 1 ? "Đã giao" : "Hủy(bởi shop)");
                        htmlData += `
                        <tr>
                        <td class="hoadon-id" >${data[i].id}</td>
                        <td class="hoadon-mahm">${data[i].mahm}</th>
                        <td class="hoadon-hotenkh">${data[i].hotenkh}</th>
                        <td class="hoadon-email">${data[i].email}</th>
                        <td class="hoadon-sdt">${data[i].sdt}</th>
                        <td class="hoadon-diachi">${data[i].diachi}</th>
                        <td class="hoadon-tongtien">${data[i].tongtien}</th>
                        <td class="hoadon-tinhtrang" data-tinhtrang="${data[i].tinhtrang}">${tinhtrang}</th>
                        <td class="hoadon-ngaygiao">${data[i].ngaygiao}</th>
                        <td class="hoadon-created">${data[i].created}</th>
                        </tr>
                        `;
                    }
                }
                $('#hoadon-unseen').html(count);
                if (!getUnSeen) {
                    clearCache();
                    $('#hoadon-table tbody').html(htmlData);
                    $('#hoadon-table').DataTable({
                        "order": [[7, "asc"]]
                    });
                }
            }
        }
    });
}

function editHoaDon(mode) {
    let data = new FormData();
    let typeMethod = "DELETE";
    let id = '';
    if (mode == 'update' || mode == 'delete') {
        id = $.trim($('#hoadon-id-Txt').val());
        if (!id) {
            alert("Vui lòng chọn 1 hóa đơn!");
            return;
        }
        data.append('id', id);
        id = '?id=' + id;
    }
    if (mode == 'update') {
        let tinhtrang = $.trim($('#hoadon-tinhtrang-Select').val())
        data.append('tinhtrang', tinhtrang);
        var ngaygiao = $.trim($('#hoadon-ngaygiao-Date').val());
        if (!ngaygiao) {
            alert('Vui lòng nhập ngày giao theo định dạnh dd/mm/yy hh:mm s');
            return;
        }
        data.append('ngaygiao', ngaygiao);
        typeMethod = "POST";
    }
    $('#hoadon-chitiethoadon').show();
    $.ajax({
        type: typeMethod,
        url: 'hoadon' + id,
        processData: false,
        contentType: false,
        cache: false,
        dataType: 'json',
        data: data,
        success: function (result) {
            if (!result.status) {
                alert(result.msg);
            }
            setHoaDon();
        }
    });
}

function setSanPhamThucTe() {
    $('#sanphamthucte-table').DataTable().destroy();
    $.ajax({
        type: "GET",
        url: 'sanphamthucte',
        cache: true,
        success: function (result) {
            if (result.status) {
                let data = result.data;
                let htmlData = '';
                for (let i = 0; i < data.length; i++) {
                    htmlData = htmlData + `
                        <tr>
                        <td class="sanphamthucte-created" >${data[i].created}</th>
                        <td class="sanphamthucte-id" >${data[i].id}</td>
                        <td class="sanphamthucte-tensp" >${data[i].tensp}</th>
                        <td class="sanphamthucte-dacta" >${data[i].dacta}</th>
                        </tr>
                        `;
                }
                clearCache();
                $('#sanphamthucte-table tbody').html(htmlData);
                $('#sanphamthucte-table').DataTable({
                    "order": [[0, "desc"]]
                });
            }
        }
    });
}

function editSanPhamThucTe(mode) {
    let data = new FormData();

    data.append('maloaisp', 0);
    tinyMCE.triggerSave();
    data.append('dacta', $.trim($('#sanphamthucte-dacta-TextArea').val()));

    let tensp = $.trim($('#sanphamthucte-tensp-Txt').val());
    let typeMethod = "DELETE";
    let id = '';
    if (mode == 'update' || mode == 'delete') {
        id = $.trim($('#sanphamthucte-id-Txt').val());
        if (!id) {
            alert("Vui lòng chọn 1 sản phẩm!");
            return;
        }
        data.append('id', id);
        id = '?id=' + id;
    }
    if (mode == 'insert' || mode == 'update') {
        data.append('tensp', tensp);
        if (mode == 'insert') {
            data.append('created', new Date().toDateTimeLocal())
        }
        typeMethod = "POST";
    }

    $.ajax({
        type: typeMethod,
        url: 'sanphamthucte' + id,
        processData: false,
        contentType: false,
        cache: false,
        dataType: 'json',
        data: data,
        success: function (result) {
            if (!result.status) {
                alert(result.msg);
            } else {
                if (typeMethod == "POST") {
                    editHinh('upload', result.data.id, true);
                }
                setSanPhamThucTe();
            }
        }
    });
}

function setDichVu() {
    $.ajax({
        type: "GET",
        url: 'dichvu',
        cache: true,
        success: function (result) {
            $('#dichvu-keyApi-Txt').val(result.data.keyApi);
            $('#dichvu-keyApiTimes-Txt').val(result.data.compressionsThisMonth);
        }
    });
}

function editDichVu() {
    let data = new FormData();
    data.append('keyApi', $.trim($('#dichvu-keyApi-Txt').val()));

    $.ajax({
        type: "POST",
        url: 'dichvu',
        processData: false,
        contentType: false,
        cache: false,
        dataType: 'json',
        data: data,
        success: function (result) {
            if (!result.status) {
                alert(result.msg);
            }
            setDichVu();
        }
    });
}

