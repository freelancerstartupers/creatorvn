!function (a) {
    "use strict";
    a(window).on("load", function () {
        a(".preloader").fadeOut(), a("#preloader").delay(550).fadeOut("slow"), a("body").delay(450).css({overflow: "visible"})
    }), a(window).on("scroll", function () {
        50 < a(window).scrollTop() ? a(".main-header").addClass("fixed-menu") : a(".main-header").removeClass("fixed-menu")
    }), a("#slides-shop").superslides({
        inherit_width_from: ".cover-slides",
        inherit_height_from: ".cover-slides",
        play: 5e3,
        animation: "fade"
    }), a(".cover-slides ul li").append("<div class='overlay-background'></div>"), a(document).ready(function () {
        a(window).on("scroll", function () {
            100 < a(this).scrollTop() ? a("#back-to-top").fadeIn() : a("#back-to-top").fadeOut()
        }), a("#back-to-top").click(function () {
            return a("html, body").animate({scrollTop: 0}, 1500, 'linear'), !1
        })
    }), a(".container").imagesLoaded(function () {
        a(".special-menu").on("click", "button", function () {
            a(this).addClass("active").siblings().removeClass("active");
            var e = a(this).attr("data-filter");
            o.isotope({filter: e})
        });
        var o = a(".special-list").isotope({itemSelector: ".special-grid"})
    }), baguetteBox.run(".tz-gallery", {
        animation: "fadeIn",
        noScrollbars: !0
    }), a(".offer-box").inewsticker({
        speed: 3e3,
        effect: "fade",
        dir: "ltr",
        font_size: 13,
        color: "#ffffff",
        font_family: "Montserrat, sans-serif",
        delay_after: 1e3
    }), a(document).ready(function () {
        a('[data-toggle="tooltip"]').tooltip()
    }), a(".main-instagram").owlCarousel({
        loop: !0,
        margin: 0,
        dots: !1,
        autoplay: !0,
        autoplayTimeout: 3e3,
        autoplayHoverPause: !0,
        navText: ["<i class='fas fa-arrow-left'></i>", "<i class='fas fa-arrow-right'></i>"],
        responsive: {0: {items: 2, nav: !0}, 600: {items: 4, nav: !0}, 1e3: {items: 8, nav: !0, loop: !0}}
    }), a(".featured-products-box").owlCarousel({
        loop: !0,
        margin: 0,
        dots: !1,
        autoplay: !0,
        autoplayTimeout: 3e3,
        autoplayHoverPause: !0,
        navText: ["<i class='fas fa-arrow-left'></i>", "<i class='fas fa-arrow-right'></i>"],
        responsive: {0: {items: 1, nav: !0}, 600: {items: 3, nav: !0}, 1e3: {items: 4, nav: !0, loop: !0}}
    }), a(document).ready(function () {
        a(window).on("scroll", function () {
            100 < a(this).scrollTop() ? a("#back-to-top").fadeIn() : a("#back-to-top").fadeOut()
        }), a("#back-to-top").click(function () {
            return a("html, body").animate({scrollTop: 0}, 900), !1
        })
    }), a(function () {
        a("#slider-range").slider({
            range: !0, min: 0, max: 4e3, values: [1e3, 3e3], slide: function (e, o) {
                a("#amount").val("$" + o.values[0] + " - $" + o.values[1])
            }
        }), a("#amount").val("$" + a("#slider-range").slider("values", 0) + " - $" + a("#slider-range").slider("values", 1))
    }), a(".brand-box").niceScroll({cursorcolor: "#9b9b9c"})
}(jQuery);

function Edit(id, times = 0) {
    var list = sessionStorage.getItem('list');
    if (!list) {
        list = {};
    } else {
        list = JSON.parse(list);
    }
    if (list[id]) {
        if (times === 0) {
            delete list[id];
        } else {
            times = parseInt($(times).val());
            list[id].times = times;
        }
    }
    sessionStorage.setItem('list', JSON.stringify(list));
    LoadCart();
    setCart(1, 1, 1, 1, 1, 1, 1, 1, true);
}

function LoadCartInCheckout() {
    var htmlData = '';
    var list = sessionStorage.getItem('list');
    if (!list) {
        list = {};
    } else {
        list = JSON.parse(list);
    }
    list = Object.values(list);
    let total = 0;
    let discout = 0;
    for (var i = 0; i < list.length; i++) {
        let price = parseInt(list[i].price);
        let sum = price * parseInt(list[i].times);
        discout += (price * parseInt(list[i].times) * parseInt(list[i].sales)) / 100;
        total += sum;
        htmlData += ` <div class="media mb-2 border-bottom"> <div class="media-body"><span><a> ${list[i].name} </a></span> <div class="small text-muted">Giá: ${ numberWithCommas(price) } VND <span class="mx-2">|</span> SL: ${list[i].times} <span class="mx-2">|</span> Tổng: ${ numberWithCommas(sum) } VND</div> </div> </div> `;
    }
    $('#cart-checkout').html(htmlData);
    $('#sub-total').text(numberWithCommas(total) + ' VND');
    $('#sales-checkout').text(numberWithCommas(discout) + ' VND');
    $('#gr-total').text(numberWithCommas(total - discout) + ' VND');
    $('#gr-total').data('total', (total - discout));
}

function LoadCart() {
    var htmlData = '';
    var list = sessionStorage.getItem('list');
    if (!list) {
        list = {};
    } else {
        list = JSON.parse(list);
    }
    list = Object.values(list);
    if (list.length < 1) {
        $('#cart-content').attr('hidden', true);
        $('#cart-null').attr('hidden', false);
        return;
    }
    $('#cart-null').attr('hidden', true);
    $('#cart-content').attr('hidden', false);
    let total = 0;
    let discout = 0;
    for (var i = 0; i < list.length; i++) {
        let price = parseInt(list[i].price);
        let sum = price * parseInt(list[i].times);
        discout += (price * parseInt(list[i].times) * parseInt(list[i].sales)) / 100;
        total += sum;
        htmlData += ` 
             <tr> 
                 <td class="thumbnail-img"> <a> <img class="img-fluid" src="${list[i].img} " alt="" /> </a> </td>
                 <td class="name-pr"> <a> ${list[i].name} </a> </td> 
                 <td class="price-pr"> <p>${ numberWithCommas(price) } VND</p> </td> 
                 <td class="quantity-box"><input id="times-${ list[i].id }" type="number" onkeypress="return isNumber(event);" size="4" value="${ list[i].times }" min="1" step="1" class="amount-times c-input-text qty text"></td>
                 <td class="total-pr"> <p>${ numberWithCommas(sum) } VND</p> </td> 
                 <td class="remove-pr"> <button onclick="Edit(${list[i].id} , '#times-${ list[i].id }'); "><i class="fa fa-edit"></i></button> </td>
                 <td class="remove-pr"> <button onclick="Edit(${list[i].id} ); "><i class="fas fa-times"></i></button> </td> 
             </tr>`;
    }
    $('tbody').html(htmlData);
    $('#sub-total').text(numberWithCommas(total) + ' VND');
    $('#sales-cart').text(numberWithCommas(discout) + ' VND');
    $('#gr-total').text(numberWithCommas(total - discout) + ' VND');
    ValidNumber();
}

function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}

function executeCheckout() {
    var data = new FormData();
    var list = sessionStorage.getItem('list');
    if (isEmpty(list)) {
        swal("WARNING", "Vui đặt mua sản phẩm trước khi thanh toán");
        callAjax('');
        return;
    }
    data.append('list-cart', list);
    var fullname = $('#fullname').val();
    if (!fullname) {
        swal("WARNING", "Vui lòng nhập họ tên");
        return;
    }
    data.append('hotenkh', fullname);
    var address = $('#address').val();
    if (!address) {
        swal("WARNING", "Vui lòng nhập địa chỉ");
        return;
    }
    data.append('diachi', address);
    var phone = $('#phone').val();
    if (!phone) {
        swal("WARNING", "Vui lòng nhập số điện thoại");
        return;
    }
    data.append('sdt', phone);
    var email = $('#email').val();
    if (!email) {
        swal("WARNING", "Vui lòng nhập email");
        return;
    }
    data.append('email', email);
    var mahm = $.trim($('#coupon').val());
    if (!mahm) {
        swal({
            title: "Bạn có chắc?",
            text: "Bạn có muốn nhập mã khuyến mãi để được giảm giá không?",
            icon: "warning",
            dangerMode: true,
            buttons: true,
        }).then((willDelete) => {
            if (!willDelete) {
                executeCheckoutAjax(data);
            }
        });
    } else {
        data.append('mahm', mahm);
        executeCheckoutAjax(data);
    }
}

function executeCheckoutAjax(data) {
    $.ajax({
        type: "POST",
        url: 'execute-checkout',
        processData: false,
        contentType: false,
        cache: false,
        dataType: 'json',
        data: data,
        success: function (result) {
            if (result.status) {
                callAjax('');
                swal("Đặt hàng thành công", "Cảm ơn bạn, sẽ có người liên hệ với bạn ngay khi xác nhận đơn hàng.");
                sessionStorage.clear();
                setCart(1, 1, 1, 1, 1, 1, 1, 1, true);
            }
        }
    });
}

function setCart(id, pro_name, name, img, price, sales, times, date, isFirst = false) {
    let list = sessionStorage.getItem('list');
    if (!list) {
        list = {};
    } else {
        list = JSON.parse(list);
    }
    if (isFirst === false) {
        if (times !== 1) {
            times = parseInt($(times).val());
        }
        let item = {id, pro_name, name, img, price, sales, times, date};
        if (list[id]) {
            list[id].times += item.times;
        } else {
            list[id] = item;
        }
        sessionStorage.setItem('list', JSON.stringify(list));
    }
    let htmlData = '';
    let total = 0;
    let numberProduct = 0;
    list = Object.values(list);
    for (let i = 0; i < list.length; i++) {
        numberProduct += list[i].times;
        let sum = parseInt(list[i].price) * parseInt(list[i].times);
        total += sum;
        htmlData += ` <li> <a class="photo"><img src="${ list[i].img } " class="cart-thumb" alt=""/></a> <h6><a>${ list[i].name } </a></h6> <p>${ list[i].times } x - <span class="price">${ numberWithCommas(sum) } VND</span></p> </li> `;
    }
    htmlData += ` <li><span class="float-right" style="font-size: 14pt; " ><strong>Tổng</strong>: ${ numberWithCommas(total) } VND</span><br/></li> <li class="total text-center"> <a onclick="callAjax('gio-hang'); $('nav.navbar.bootsnav > .side').removeClass('on'); $('body').removeClass('on-side'); " class="btn btn-default hvr-hover btn-cart btn-xs">Giỏ Hàng</a> </li> `;
    $('#cart-list').html(htmlData);
    $('#number-product').text(numberProduct);
}

var localCache = {
    data: {},
    remove: function (url) {
        delete localCache.data[url];
    },
    exist: function (url) {
        return localCache.data.hasOwnProperty(url) && localCache.data[url] !== null;
    },
    get: function (url) {
        console.log('Getting in cache for url ' + url);
        return localCache.data[url];
    },
    set: function (url, cachedData, callback) {
        localCache.remove(url);
        localCache.data[url] = cachedData;
        if ($.isFunction(callback)) callback(cachedData);
    }
};

function callAjax(link, changeLink = true) {
    link = link.replace('#', '');
    link = '/' + link;
    var url = link;
    $.ajax({
            type: "GET", url: link, cache: true,
            beforeSend: function () {
                if (changeLink === true) {
                    window.history.pushState({path: ""}, "", link);
                }
                if (localCache.exist(url)) {
                    doSomething(localCache.get(url), url);
                    return false;
                }
                return true;
            },
            success: function (view) {
                if (link === '/gio-hang') {
                    $('#container-content').html(view);
                    LoadCart();
                } else if (link === '/dat-hang') {
                    $('#container-content').html(view);
                    LoadCartInCheckout();
                } else {
                    localCache.set(url, view, doSomething);
                }
            }
        }
    );
}

function doSomething(view, link) {
    document.body.scrollTop = document.documentElement.scrollTop = 50;
    $('#container-content').html(view);
    dbClickThumb();
    $('body>.tooltip').remove();
    if (link === '/') {
        document.getElementById('slides-shop').style.backgroundColor = '#ffffff';
        document.getElementById('slides-shop').style.backgroundImage = 'none';
    }
    $('#navbar-menu').removeClass('show');
    $('#search-input').val('');
    $(".top-search").slideUp();
    $('.owl-nav').remove();
}



