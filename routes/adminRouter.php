<?php

//Admin page
Route::get('admin', 'AdminController@index')->name('admin_page');
Route::get('login', 'AdminController@login')->name('login_page');
//Home controller
Route::match(['get', 'post', 'delete', 'put'], 'loaisanpham', 'HomeController@loaiSanPham');
Route::match(['post', 'delete'], 'authen', 'HomeController@authen');
Route::match(['get', 'post', 'delete'], 'taikhoan', 'HomeController@taiKhoan');
Route::match(['get', 'post', 'delete'], 'haumai', 'HomeController@hauMai');
Route::match(['get', 'post', 'delete'], 'baiviet', 'HomeController@baiViet');
Route::post('uploadextend', 'HomeController@uploadFileExtend');
Route::match(['get', 'post', 'delete'], 'chitiethoadon', 'HomeController@chiTietHoaDon');
Route::match(['get', 'post', 'delete'], 'hoadon', 'HomeController@hoaDon');
Route::match(['get', 'post', 'delete'], 'sanpham', 'HomeController@sanPham');
Route::match(['get', 'post', 'delete'], 'sanphamthucte', 'HomeController@sanPhamThucTe');
Route::match(['get', 'post', 'delete'], 'hinh', 'HomeController@hinh');
Route::match(['get', 'post'], 'gioithieu', 'HomeController@gioiThieu');
Route::match(['get', 'post'], 'vechungtoi', 'HomeController@veChungToi');
Route::match(['get', 'post'], 'chinhsach', 'HomeController@chinhSach');
Route::match(['get', 'post', 'delete'], 'theme', 'HomeController@theme');
Route::match(['get', 'post'], 'dichvu', 'HomeController@dichVu');
Route::get('/optimze-imgs-all', 'HomeController@optimizeAll');
Route::get('/remove-unused-imgs', 'HomeController@removeUnusedImgs');
Route::post('/optimize-imgs', 'HomeController@optimizeImgs');
Route::match(['get', 'post'], 'hotro', 'HomeController@helps');