<?php

//User page
Route::get('/', 'UserController@home')->name("index_page");
Route::get('danh-muc/{cat_name}/trang/{page}', 'UserController@category');
Route::get('san-pham/{pro_name}', 'UserController@product');
Route::get('khuyen-mai/trang/{page}', 'UserController@discount');
Route::get('gio-hang', 'UserController@cart');
Route::get('tim-kiem', 'UserController@search');
Route::get('chinh-sach-va-quy-dinh', 'UserController@policy');
Route::get('bai-viet/{title}', 'UserController@blog');
Route::get('ve-chung-toi', 'UserController@aboutus');
Route::get('gioi-thieu', 'UserController@intro');
Route::get('dat-hang', 'UserController@checkout');
Route::get('cac-san-pham-thuc-te/trang/{page}', 'UserController@practical');
Route::get('cac-san-pham-thuc-te/chi-tiet/{title}', 'UserController@practicalDetail');
Route::post('execute-checkout', 'UserController@execute_checkout');
Route::post('check-coupon', 'UserController@checkCoupon');