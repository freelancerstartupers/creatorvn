<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('hello');
//});
//Clear Cache facade value:

Route::get('/getpath',function (){
   var_dump(base_path());die;
});

Route::get('/clear', function () {
    $exitCode1 = Artisan::call('cache:clear');
    $exitCode2 = Artisan::call('view:clear');
    $exitCode3 = Artisan::call('config:clear');
    $exitCode4 = Artisan::call('twig:clean');
    $exitCode5 = Artisan::call('route:clear');
    $exitCode6 = Artisan::call('clear-compiled');
    return "<h1>Clear: cache: $exitCode1, view: $exitCode2, config: $exitCode3, twig: $exitCode4, route: $exitCode5, compiled: $exitCode6</h1>";
})->name("clear_cache_all");


//Reoptimized class loader:
Route::get('/optimize', function () {
    $exitCode = Artisan::call('optimize');
    return '<h1>Reoptimized class loader</h1>';
});

//Route cache:
Route::get('/route-cache', function () {
    $exitCode = Artisan::call('route:cache');
    return '<h1>Routes cached</h1>';
});

//Clear Route cache:
Route::get('/router-clear', function () {
    $exitCode = Artisan::call('router:clear');
    return '<h1>Route cache cleared</h1>';
});

//test
Route::get('/test', function () {
    var_dump(ini_get('max_execution_time'));
    return 'Route Test';
});




require_once 'adminRouter.php';
require_once 'userRouter.php';