-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Máy chủ: localhost:3306
-- Thời gian đã tạo: Th9 29, 2018 lúc 11:17 AM
-- Phiên bản máy phục vụ: 10.1.31-MariaDB-cll-lve
-- Phiên bản PHP: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `creatorv_rp520`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `baiviet`
--

CREATE TABLE `baiviet` (
  `id` int(11) NOT NULL,
  `noidung` text,
  `chitiet` text NOT NULL,
  `tieude` text,
  `media` int(11) DEFAULT NULL,
  `duongdan` text,
  `link` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `baiviet`
--

INSERT INTO `baiviet` (`id`, `noidung`, `chitiet`, `tieude`, `media`, `duongdan`, `link`) VALUES
(2, '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p style=\"text-align: justify;\"><span style=\"color: #606060; font-family: Arial, Helvetica, sans-serif; font-size: 16px; background-color: #ffffff;\">Với c&aacute;c c&ocirc;ng ty, tập&nbsp;đo&agrave;n hoạt&nbsp;động trong c&aacute;c lĩnh vực kinh tế, t&agrave;i ch&iacute;nh hay kinh doanh c&aacute;c sản phẩm, dịch vụ cao cấp, thiết kế sảnh lễ t&acirc;n cần thiết tạo&nbsp;được&nbsp;ấn tượng mạnh. Thiết kế kh&ocirc;ng chỉ ch&uacute; trọng v&agrave;o đường n&eacute;t, m&agrave;u sắc m&agrave; c&ograve;n đặc biệt kỹ lưỡng trong việc sử dụng vật liệu. Kh&ocirc;ng phải ngẫu nhi&ecirc;n khi vật liệu ch&iacute;nh sử dụng&nbsp;ở khu vực n&agrave;y thường l&agrave; gỗ hay&nbsp;đ&aacute; tự nhi&ecirc;n.</span></p>\r\n</body>\r\n</html>', '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p style=\"margin-top: 0px; margin-bottom: 20px; color: #606060; font-family: Arial, Helvetica, sans-serif; font-size: 16px; background-color: #ffffff;\"><span style=\"font-weight: bold;\">Sảnh lễ t&acirc;n, Reception, Lobbie, Entryway</span>&nbsp;l&agrave; những c&aacute;i t&ecirc;n&nbsp;được quy&nbsp;định cho khu vực kh&aacute;ch v&agrave;o&nbsp;ở văn ph&ograve;ng v&agrave;&nbsp;những nơi giao dịch kh&aacute;c.&nbsp;Đ&acirc;y ch&iacute;nh l&agrave; nơi sẽ tạo&nbsp;ấn tượng&nbsp;đầu ti&ecirc;n v&agrave; c&oacute; thể n&oacute;i l&agrave; quan trọng nhất, gi&uacute;p cho c&ocirc;ng ty, doanh nghiệp tạo thiện cảm với kh&aacute;ch h&agrave;ng,&nbsp;đối t&aacute;c (v&agrave; cả nh&acirc;n vi&ecirc;n của m&igrave;nh). Ch&iacute;nh v&igrave; vậy m&agrave; khi&nbsp;<a style=\"background-repeat: initial; color: #000000; text-decoration-line: none; outline: 0px; transition: all 0.3s ease 0s;\" href=\"http://afa.com.vn/dich-vu/thiet-ke-van-phong\">thiết kế văn ph&ograve;ng</a>&nbsp;đ&ograve;i hỏi người thiết kế cần nghi&ecirc;n cứu kỹ lưỡng về c&aacute;c lĩnh vực kinh doanh của c&ocirc;ng ty, t&igrave;m hiểu về văn h&oacute;a doanh nghiệp từ&nbsp;đ&oacute; lựa chọn, s&aacute;ng tạo n&ecirc;n những phương&nbsp;&aacute;n thiết kế sảnh lễ t&acirc;n tối&nbsp;ưu. C&ugrave;ng&nbsp;AFA Design&nbsp;điểm qua những mẫu sảnh lễ t&acirc;n với những phong c&aacute;ch&nbsp;đa dạng, ph&ugrave; hợp với nhiều m&ocirc; h&igrave;nh v&agrave; văn h&oacute;a c&ocirc;ng ty.</p>\r\n<p>&nbsp;</p>\r\n<h2 style=\"margin-top: 20px; margin-bottom: 30px; background-color: #ffffff; color: #000000; font-weight: bold; font-size: 26px; font-family: Arial, Helvetica, sans-serif; line-height: 26px;\">Sảnh lễ t&acirc;n th&acirc;n thiện với m&agrave;u sắc c&acirc;y xanh</h2>\r\n<p style=\"margin-top: 0px; margin-bottom: 0px; background-color: #ffffff; color: #606060; font-family: Arial, Helvetica, sans-serif; font-size: 16px;\">C&ugrave;ng với phong c&aacute;ch thiết kế văn ph&ograve;ng xanh, khu vực lễ t&acirc;n với những mảng xanh g&oacute;p phần tạo cảm gi&aacute;c thoải m&aacute;i, dễ chịu cho những người gh&eacute; thăm. Bạn ho&agrave;n to&agrave;n c&oacute; thể kết hợp c&acirc;y xanh với k&iacute;nh, gỗ hay&nbsp;đ&aacute;, từ phong c&aacute;ch hiện&nbsp;đại tối giản tới phong c&aacute;ch industry hay cổ&nbsp;điển.</p>\r\n<p style=\"margin-top: 0px; margin-bottom: 0px; background-color: #ffffff; color: #606060; font-family: Arial, Helvetica, sans-serif; font-size: 16px;\">&nbsp;</p>\r\n<p style=\"margin-top: 0px; margin-bottom: 0px; background-color: #ffffff; color: #606060; font-family: Arial, Helvetica, sans-serif; font-size: 16px;\"><img src=\"images/extend/le-tan-vp-1.jpg\" alt=\"\" width=\"660\" height=\"440\" /></p>\r\n<p style=\"margin-top: 0px; margin-bottom: 0px; background-color: #ffffff; color: #606060; font-family: Arial, Helvetica, sans-serif; font-size: 16px;\">&nbsp;</p>\r\n<p style=\"margin-top: 0px; margin-bottom: 0px; background-color: #ffffff; color: #606060; font-family: Arial, Helvetica, sans-serif; font-size: 16px;\"><span style=\"color: #606060; background-color: #ffffff;\">Việc mang c&aacute;c yếu tố thi&ecirc;n nhi&ecirc;n v&agrave;o trong văn ph&ograve;ng gi&uacute;p x&oacute;a nh&ograve;a c&aacute;c kh&ocirc;ng gian trong v&agrave; ngo&agrave;i. M&agrave;u xanh vừa thể hện sự tươi mới, t&iacute;ch cực vừa tạo cảm gi&aacute;c gần gũi.</span></p>\r\n<p style=\"margin-top: 0px; margin-bottom: 0px; background-color: #ffffff; color: #606060; font-family: Arial, Helvetica, sans-serif; font-size: 16px;\">&nbsp;</p>\r\n<p style=\"margin-top: 0px; margin-bottom: 0px; background-color: #ffffff; color: #606060; font-family: Arial, Helvetica, sans-serif; font-size: 16px;\"><span style=\"color: #606060; background-color: #ffffff;\"><img src=\"images/extend/20141231073135025.jpg\" alt=\"\" width=\"680\" height=\"453\" /></span></p>\r\n<h2 style=\"font-family: Arial, Helvetica, sans-serif; font-weight: bold; line-height: 26px; color: #000000; margin-top: 20px; margin-bottom: 30px; font-size: 26px; background-color: #ffffff;\">Sảnh lễ t&acirc;n sang trọng</h2>\r\n<p style=\"margin-top: 0px; margin-bottom: 0px; color: #606060; font-family: Arial, Helvetica, sans-serif; font-size: 16px; background-color: #ffffff;\">Với c&aacute;c c&ocirc;ng ty, tập&nbsp;đo&agrave;n hoạt&nbsp;động trong c&aacute;c lĩnh vực kinh tế, t&agrave;i ch&iacute;nh hay kinh doanh c&aacute;c sản phẩm, dịch vụ cao cấp, thiết kế sảnh lễ t&acirc;n cần thiết tạo&nbsp;được&nbsp;ấn tượng mạnh. Thiết kế kh&ocirc;ng chỉ ch&uacute; trọng v&agrave;o đường n&eacute;t, m&agrave;u sắc m&agrave; c&ograve;n đặc biệt kỹ lưỡng trong việc sử dụng vật liệu. Kh&ocirc;ng phải ngẫu nhi&ecirc;n khi vật liệu ch&iacute;nh sử dụng&nbsp;ở khu vực n&agrave;y thường l&agrave; gỗ hay&nbsp;đ&aacute; tự nhi&ecirc;n.</p>\r\n<p style=\"margin-top: 0px; margin-bottom: 0px; color: #606060; font-family: Arial, Helvetica, sans-serif; font-size: 16px; background-color: #ffffff;\">&nbsp;</p>\r\n<p style=\"margin-top: 0px; margin-bottom: 0px; color: #606060; font-family: Arial, Helvetica, sans-serif; font-size: 16px; background-color: #ffffff;\">&nbsp;</p>\r\n<p style=\"margin-top: 0px; margin-bottom: 0px; color: #606060; font-family: Arial, Helvetica, sans-serif; font-size: 16px; background-color: #ffffff;\"><img src=\"images/extend/cach-trang-tri-quay-le-tan-khach-san-hop-phong-thuy-hut-tai-loc1.jpg\" alt=\"\" width=\"600\" height=\"451\" /></p>\r\n<p style=\"margin-top: 0px; margin-bottom: 0px; color: #606060; font-family: Arial, Helvetica, sans-serif; font-size: 16px; background-color: #ffffff;\">&nbsp;</p>\r\n<p style=\"margin-top: 0px; margin-bottom: 0px; color: #606060; font-family: Arial, Helvetica, sans-serif; font-size: 16px; background-color: #ffffff;\"><span style=\"color: #606060; background-color: #ffffff;\">Kh&ocirc;ng phải ngẫu nhi&ecirc;n khi vật liệu ch&iacute;nh sử dụng&nbsp;ở khu vực n&agrave;y thường l&agrave; gỗ hay&nbsp;đ&aacute; tự nhi&ecirc;n.</span></p>\r\n<p style=\"margin-top: 0px; margin-bottom: 0px; color: #606060; font-family: Arial, Helvetica, sans-serif; font-size: 16px; background-color: #ffffff;\">&nbsp;</p>\r\n<p style=\"margin-top: 0px; margin-bottom: 0px; color: #606060; font-family: Arial, Helvetica, sans-serif; font-size: 16px; background-color: #ffffff;\"><span style=\"color: #606060; background-color: #ffffff;\"><img src=\"images/extend/Design-Deneys-Reitz-Office-Interior-Design-by-Collaboration-Architecture-Photos.jpg\" alt=\"\" width=\"940\" height=\"626\" /></span></p>\r\n<p style=\"margin-top: 0px; margin-bottom: 0px; color: #606060; font-family: Arial, Helvetica, sans-serif; font-size: 16px; background-color: #ffffff;\">&nbsp;</p>\r\n<h2 style=\"font-family: Arial, Helvetica, sans-serif; font-weight: bold; line-height: 26px; color: #000000; margin-top: 20px; margin-bottom: 30px; font-size: 26px; background-color: #ffffff;\">Sảnh lễ t&acirc;n hiện đại</h2>\r\n<p style=\"margin-top: 0px; margin-bottom: 0px; color: #606060; font-family: Arial, Helvetica, sans-serif; font-size: 16px; background-color: #ffffff;\">Kh&ocirc;ng c&oacute; một giới hạn n&agrave;o cho khu vực lễ t&acirc;n theo phong c&aacute;ch hiện&nbsp;đại.&nbsp;Đ&oacute; c&oacute; thể l&agrave; phong c&aacute;ch nội thất tối giản hay theo trường ph&aacute;i&nbsp;đa phong c&aacute;ch. Bạn cũng ho&agrave;n to&agrave;n c&oacute; thể thử nghiệm những vật liệu mới v&agrave;&nbsp;đột ph&aacute; trong c&aacute;ch sắp xếp.</p>\r\n<p style=\"margin-top: 0px; margin-bottom: 0px; color: #606060; font-family: Arial, Helvetica, sans-serif; font-size: 16px; background-color: #ffffff;\">&nbsp;</p>\r\n<p style=\"margin-top: 0px; margin-bottom: 0px; color: #606060; font-family: Arial, Helvetica, sans-serif; font-size: 16px; background-color: #ffffff;\"><img src=\"images/extend/ban-len-tan-2.png\" alt=\"\" width=\"814\" height=\"591\" /></p>\r\n<p style=\"margin-top: 0px; margin-bottom: 0px; color: #606060; font-family: Arial, Helvetica, sans-serif; font-size: 16px; background-color: #ffffff;\">&nbsp;</p>\r\n<p style=\"margin-top: 0px; margin-bottom: 0px; color: #606060; font-family: Arial, Helvetica, sans-serif; font-size: 16px; background-color: #ffffff;\"><span style=\"color: #606060; background-color: #ffffff;\">Phong c&aacute;ch nội thất&nbsp;hiện&nbsp;đại,&nbsp;đơn giản với c&aacute;c m&agrave;u sắc trung t&iacute;nh c&agrave;ng ng&agrave;y c&agrave;ng&nbsp;được&nbsp;ưa chuộng.</span></p>\r\n<p style=\"margin-top: 0px; margin-bottom: 0px; color: #606060; font-family: Arial, Helvetica, sans-serif; font-size: 16px; background-color: #ffffff;\">&nbsp;</p>\r\n<p style=\"margin-top: 0px; margin-bottom: 0px; color: #606060; font-family: Arial, Helvetica, sans-serif; font-size: 16px; background-color: #ffffff;\"><span style=\"color: #606060; background-color: #ffffff;\"><img src=\"images/extend/Creator.jpg\" alt=\"\" width=\"720\" height=\"350\" /></span></p>\r\n<p style=\"margin-top: 0px; margin-bottom: 0px; color: #606060; font-family: Arial, Helvetica, sans-serif; font-size: 16px; background-color: #ffffff;\">&nbsp;</p>\r\n<p style=\"margin-top: 0px; margin-bottom: 0px; color: #606060; font-family: Arial, Helvetica, sans-serif; font-size: 16px; background-color: #ffffff;\"><span style=\"color: #606060; background-color: #ffffff;\"><img src=\"images/extend/fitness_gym_reception_by_archjun-d5llgfp.jpg\" alt=\"\" width=\"900\" height=\"535\" /></span></p>\r\n<p style=\"margin-top: 0px; margin-bottom: 0px; color: #606060; font-family: Arial, Helvetica, sans-serif; font-size: 16px; background-color: #ffffff;\">&nbsp;</p>\r\n<p style=\"margin-top: 0px; margin-bottom: 0px; color: #606060; font-family: Arial, Helvetica, sans-serif; font-size: 16px; background-color: #ffffff;\"><span style=\"color: #606060; background-color: #ffffff;\">Một thiết kế sảnh lễ t&acirc;n&nbsp;đẹp, thể hiện sự chuy&ecirc;n nghệp sẽ g&oacute;p phần kh&ocirc;ng nhỏ v&agrave;o việc tạo dựng niềm tin vững chắc cho kh&aacute;ch h&agrave;ng v&agrave;&nbsp;đối t&aacute;c. B&ecirc;n cạnh&nbsp;đ&oacute;, kh&ocirc;ng thể kh&ocirc;ng kể&nbsp;đến những lợi&nbsp;&iacute;ch kh&aacute;c về kinh tế v&agrave; truyền th&ocirc;ng m&agrave; n&oacute; mang lại. Mỗi c&ocirc;ng ty, doanh nghiệp sẽ c&oacute; những&nbsp;định hướng v&agrave; y&ecirc;u cầu ri&ecirc;ng&nbsp;để ph&ugrave; hợp với&nbsp;đường lối ph&aacute;t triển, văn h&oacute;a doanh nghiệp của ri&ecirc;ng m&igrave;nh. Nếu bạn cần sự tư vấn cụ thể từ chuy&ecirc;n gia,&nbsp;đừng qu&ecirc;n li&ecirc;n lạc với ch&uacute;ng t&ocirc;i&nbsp;để c&oacute; c&aacute;i nh&igrave;n tổng quan hơn về việc&nbsp;</span><a style=\"background-repeat: initial; background-color: #ffffff; color: #000000; text-decoration-line: none; outline: 0px; transition: all 0.3s ease 0s;\" href=\"http://afa.com.vn/dich-vu/thiet-ke-van-phong\">thiết kế văn ph&ograve;ng</a><span style=\"color: #606060; background-color: #ffffff;\">.</span></p>\r\n<p style=\"margin-top: 0px; margin-bottom: 0px; color: #606060; font-family: Arial, Helvetica, sans-serif; font-size: 16px; background-color: #ffffff;\">&nbsp;</p>\r\n<p style=\"margin-top: 0px; margin-bottom: 0px; color: #606060; font-family: Arial, Helvetica, sans-serif; font-size: 16px; background-color: #ffffff; text-align: right;\"><span style=\"color: #606060; background-color: #ffffff;\">-Sưu tầm-</span></p>\r\n</body>\r\n</html>', 'Bí quyết để có một sảnh lễ tân đẹp và ấn tượng', 0, 'http://creatorvn.com/images/blog/fitness_gym_reception_by_archjun-d5llgfp.jpg', 'null'),
(3, '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p><span style=\"color: #222222; font-family: arial; background-color: #ffffff;\">Năm 2018, xu hướng nội thất đơn giản ho&aacute; sẽ ng&agrave;y c&agrave;ng phổ biến. C&aacute;c loại nội thất mang phong c&aacute;ch cổ điển với đường n&eacute;t cầu kỳ chỉ ph&ugrave; hợp với đối tượng trung ni&ecirc;n.</span></p>\r\n<p>&nbsp;</p>\r\n</body>\r\n</html>', '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p><span style=\"color: #222222; font-family: arial; font-weight: bold; background-color: #ffffff;\">Dưới&nbsp;đ&acirc;y l&agrave; 10 xu hướng nội thất&nbsp;\"hot\" nhất năm 2018.</span></p>\r\n<p><span style=\"color: #222222; font-family: arial; background-color: #ffffff;\"><img src=\"images/extend/10-xu-huong-noi-that-se-sot.jpg\" alt=\"\" width=\"640\" height=\"318\" /></span></p>\r\n<p><span style=\"color: #222222; font-family: arial; background-color: #ffffff;\">Năm 2018, xu hướng nội thất đơn giản ho&aacute; sẽ ng&agrave;y c&agrave;ng phổ biến. C&aacute;c loại nội thất mang phong c&aacute;ch cổ điển với đường n&eacute;t cầu kỳ chỉ ph&ugrave; hợp với đối tượng trung ni&ecirc;n.</span></p>\r\n<p><span style=\"color: #222222; font-family: arial; background-color: #ffffff;\"><img src=\"images/extend/10-xu-huong-noi-that-se-sot-1.jpg\" alt=\"\" width=\"500\" height=\"300\" /></span></p>\r\n<p><span style=\"color: #222222; font-family: arial; background-color: #ffffff;\">Trong khi đ&oacute;, giới trẻ sẽ thi&ecirc;n về nội thất đơn giản, chiếm &iacute;t kh&ocirc;ng gian v&agrave; dễ vận chuyển. Xu hướng nội thất đơn giản ph&ugrave; hợp cho cuộc sống hiện đại, năng động, đặc biệt l&agrave; căn hộ chung cư.&nbsp;</span></p>\r\n<p><span style=\"color: #222222; font-family: arial; background-color: #ffffff;\"><img src=\"images/extend/10-xu-huong-noi-that-se-sot-2.jpg\" alt=\"\" width=\"667\" height=\"500\" /></span></p>\r\n<p><span style=\"color: #222222; font-family: arial; background-color: #ffffff;\">Sự trỗi dậy của phong c&aacute;ch nội thất m&agrave;u gỗ tự nhi&ecirc;n sẽ mang lại vẻ đẹp tươi s&aacute;ng v&agrave; kh&ocirc;ng lo lỗi thời.</span></p>\r\n<p><span style=\"color: #222222; font-family: arial; background-color: #ffffff;\"><img src=\"images/extend/10-xu-huong-noi-that-se-sot-3.jpg\" alt=\"\" width=\"800\" height=\"400\" /></span></p>\r\n<p><span style=\"color: #222222; font-family: arial; background-color: #ffffff;\">Cũng như thời trang, xu hướng nội thất họa tiết sẽ l&ecirc;n ng&ocirc;i trong những năm tới. Giường họa tiết hoa, ghế sofa họa tiết hoa, tường họa tiết hoa... mang đến l&agrave;n gi&oacute; mới cho căn ph&ograve;ng.&nbsp;</span></p>\r\n<p><span style=\"color: #222222; font-family: arial; background-color: #ffffff;\"><img src=\"images/extend/10-xu-huong-noi-that-se-sot-4.jpg\" alt=\"\" width=\"665\" height=\"500\" /></span></p>\r\n<p><span style=\"color: #222222; font-family: arial; background-color: #ffffff;\">Họa tiết h&igrave;nh học đ&atilde; xuất hiện một thời gian v&agrave; dự b&aacute;o sẽ dẫn đầu tr&agrave;o lưu trang tr&iacute; nội thất v&agrave;o năm sau. Xu hướng n&agrave;y ảnh hưởng trực tiếp l&ecirc;n việc lựa chọn ga, thảm, giấy d&aacute;n tường, r&egrave;m cửa&hellip; </span></p>\r\n<p><span style=\"color: #222222; font-family: arial; background-color: #ffffff;\"><img src=\"images/extend/10-xu-huong-noi-that-se-sot-5.jpg\" alt=\"\" width=\"600\" height=\"400\" /></span></p>\r\n<p><span style=\"color: #222222; font-family: arial; background-color: #ffffff;\">Sự kết hợp s&aacute;ng tạo của hai gam m&agrave;u tương phản đen v&agrave; trắng mang đến gi&aacute; trị l&acirc;u bền.&nbsp;</span></p>\r\n<p><span style=\"color: #222222; font-family: arial; background-color: #ffffff;\"><img src=\"images/extend/10-xu-huong-noi-that-se-sot-6.jpg\" alt=\"\" width=\"898\" height=\"500\" /></span></p>\r\n<p><span style=\"color: #222222; font-family: arial; background-color: #ffffff;\">Thiết kế tường 3D sẽ trở th&agrave;nh xu hướng &ldquo;g&acirc;y b&atilde;o&rdquo; trong thời gian tới, đặc biệt l&agrave; khoảng đầu năm 2018. C&aacute;c mảng tường 3D sẽ l&agrave; điểm nhấn kh&aacute;c biệt cho kh&ocirc;ng gian.&nbsp;</span></p>\r\n<p><span style=\"color: #222222; font-family: arial; background-color: #ffffff;\"><img src=\"images/extend/10-xu-huong-noi-that-se-sot-7.jpg\" alt=\"\" width=\"580\" height=\"500\" /><br /></span></p>\r\n<p><span style=\"color: #222222; font-family: arial; background-color: #ffffff;\">Thiết kế đơn giản v&agrave; nhấn nh&aacute; bằng kim loại với những đường vuốt cong kh&eacute;o l&eacute;o sẽ l&agrave;m n&ecirc;n gi&aacute; trị thẩm mỹ cho c&aacute;c kh&ocirc;ng gian hiện đại của năm 2018.&nbsp;</span></p>\r\n<p><span style=\"color: #222222; font-family: arial; background-color: #ffffff;\"><img src=\"images/extend/10-xu-huong-noi-that-se-sot-8.jpg\" alt=\"\" width=\"667\" height=\"500\" /></span></p>\r\n<p><span style=\"color: #222222; font-family: arial; background-color: #ffffff;\">Về m&agrave;u sắc, 2018 sẽ l&agrave; những kh&ocirc;ng gian của sự ấm c&uacute;ng v&agrave; gần gũi như kết hợp n&acirc;u với xanh lam, đen, v&agrave;ng...</span></p>\r\n<p><span style=\"color: #222222; font-family: arial; background-color: #ffffff;\"><img src=\"images/extend/10-xu-huong-noi-that-se-sot-9.jpg\" alt=\"\" width=\"740\" height=\"493\" /></span></p>\r\n<p><span style=\"color: #222222; font-family: arial; background-color: #ffffff;\">Xi măng trần c&ugrave;ng gạch Retro xuất hiện trong nội thất phong c&aacute;ch c&ocirc;ng nghiệp v&agrave; mang lại hiệu quả thẩm mỹ cao. </span><span style=\"color: #222222; font-family: arial; background-color: #ffffff;\"><img src=\"images/extend/10-xu-huong-noi-that-se-sot-10.jpg\" alt=\"\" width=\"434\" height=\"500\" /></span></p>\r\n<p><span style=\"color: #222222; font-family: arial; background-color: #ffffff;\">Năm 2018 cũng sẽ l&agrave; sự hồi sinh của c&aacute;c loại giấy d&aacute;n tường. Ng&agrave;y nay, giấy d&aacute;n tường được n&acirc;ng cấp với chất liệu bền hơn, hoa văn tinh tế hơn, đặc biệt l&agrave; c&oacute; lớp bảo vệ chống thấm, chống ẩm mốc, ph&ugrave; hợp nhiều vị tr&iacute;.</span></p>\r\n<p style=\"text-align: right;\"><span style=\"color: #222222; font-family: arial; background-color: #ffffff;\">- Sưu tầm -</span></p>\r\n</body>\r\n</html>', 'Xu hướng nội thất hot xình xịch năm 2018', 0, 'https://creatorvn.com/images/blog/10-xu-huong-noi-that-se-sot.jpg', NULL);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `chitiethoadon`
--

CREATE TABLE `chitiethoadon` (
  `id` int(11) NOT NULL,
  `mahd` int(11) DEFAULT NULL,
  `masp` int(11) DEFAULT NULL,
  `gia` int(11) DEFAULT NULL,
  `soluong` int(11) DEFAULT NULL,
  `isdeleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `chitiethoadon`
--

INSERT INTO `chitiethoadon` (`id`, `mahd`, `masp`, `gia`, `soluong`, `isdeleted`) VALUES
(14, 14, 129, 8500000, NULL, 0),
(15, 15, 34, 13000000, 1, 0),
(16, 16, 2, 1800000, 1, 0),
(17, 17, 17, 4000000, NULL, 0);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `haumai`
--

CREATE TABLE `haumai` (
  `id` int(11) NOT NULL,
  `mahm` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `phantram` int(11) DEFAULT '0',
  `dasudung` int(11) DEFAULT '0',
  `ngaykt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `hinh`
--

CREATE TABLE `hinh` (
  `id` int(11) NOT NULL,
  `masp` int(11) DEFAULT NULL,
  `tenhinh` varchar(255) DEFAULT NULL,
  `duongdan` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `hinh`
--

INSERT INTO `hinh` (`id`, `masp`, `tenhinh`, `duongdan`) VALUES
(4, 16, 'BS025-1.png', 'https://creatorvn.com/images/product/BS025-1.png'),
(8, 33, 'GB002-1.jpg', 'https://creatorvn.com/images/product/GB002-1.jpg'),
(9, 43, 'GB012-1.jpg', 'https://creatorvn.com/images/product/GB012-1.jpg'),
(14, 45, 'GB013-1.jpg', 'https://creatorvn.com/images/product/GB013-1.jpg'),
(15, 45, 'GB013-2.jpg', 'https://creatorvn.com/images/product/GB013-2.jpg'),
(16, 45, 'GB013-3.jpg', 'https://creatorvn.com/images/product/GB013-3.jpg'),
(17, 45, 'GB013-4.jpg', 'https://creatorvn.com/images/product/GB013-4.jpg'),
(18, 46, 'GB013-1.jpg', 'https://creatorvn.com/images/product/GB013-1.jpg'),
(19, 46, 'GB013-2.jpg', 'https://creatorvn.com/images/product/GB013-2.jpg'),
(20, 46, 'GB013-3.jpg', 'https://creatorvn.com/images/product/GB013-3.jpg'),
(21, 46, 'GB013-4.jpg', 'https://creatorvn.com/images/product/GB013-4.jpg'),
(22, 47, 'GB016-1.jpg', 'https://creatorvn.com/images/product/GB016-1.jpg'),
(23, 48, 'GB016-1.jpg', 'https://creatorvn.com/images/product/GB016-1.jpg'),
(24, 49, 'GB018-1.jpg', 'https://creatorvn.com/images/product/GB018-1.jpg'),
(25, 49, 'GB018-2.jpg', 'https://creatorvn.com/images/product/GB018-2.jpg'),
(26, 50, 'GB019-1.jpg', 'https://creatorvn.com/images/product/GB019-1.jpg'),
(27, 54, 'GB23-1.jpg', 'https://creatorvn.com/images/product/GB23-1.jpg'),
(75, 73, 'Creator 2.png', 'http://creatorvn.com/images/practical/Creator 2.png'),
(77, 73, 'Creator 5.png', 'http://creatorvn.com/images/practical/Creator 5.png'),
(78, 73, 'Creator 9.png', 'http://creatorvn.com/images/practical/Creator 9.png'),
(79, 73, 'Creator 10.png', 'http://creatorvn.com/images/practical/Creator 10.png'),
(80, 73, 'Creator 11.png', 'http://creatorvn.com/images/practical/Creator 11.png'),
(81, 73, 'Creator 12.png', 'http://creatorvn.com/images/practical/Creator 12.png'),
(82, 73, 'Creator 14.png', 'http://creatorvn.com/images/practical/Creator 14.png'),
(83, 73, 'Creator 15.png', 'http://creatorvn.com/images/practical/Creator 15.png'),
(84, 73, 'Creator 16.jpg', 'http://creatorvn.com/images/practical/Creator 16.jpg'),
(85, 73, 'Creator 17.png', 'http://creatorvn.com/images/practical/Creator 17.png'),
(86, 73, 'Creator 18.png', 'http://creatorvn.com/images/practical/Creator 18.png'),
(87, 74, 'CreatorVN\'s 4.png', 'http://creatorvn.com/images/practical/CreatorVN\'s 4.png'),
(88, 74, 'CreatorVN\'s 6.png', 'http://creatorvn.com/images/practical/CreatorVN\'s 6.png'),
(89, 74, 'CreatorVN\'s 7.png', 'http://creatorvn.com/images/practical/CreatorVN\'s 7.png'),
(90, 74, 'CreatorVN\'s 10.png', 'http://creatorvn.com/images/practical/CreatorVN\'s 10.png'),
(132, 72, '2.jpg', 'http://creatorvn.com/images/product/2.jpg'),
(133, 72, '3.jpg', 'http://creatorvn.com/images/product/3.jpg'),
(134, 72, '4.jpg', 'http://creatorvn.com/images/product/4.jpg'),
(135, 72, '5.jpg', 'http://creatorvn.com/images/product/5.jpg'),
(138, 76, 'home.jpg', 'https://creatorvn.com/images/practical/home.jpg'),
(139, 77, '1.png', 'http://creatorvn.com/images/practical/1.png'),
(140, 77, '2.png', 'http://creatorvn.com/images/practical/2.png'),
(141, 77, '3.png', 'http://creatorvn.com/images/practical/3.png'),
(142, 78, '1.jpg', 'http://creatorvn.com/images/practical/1.jpg'),
(143, 78, '2.jpg', 'http://creatorvn.com/images/practical/2.jpg'),
(144, 78, '3.jpg', 'http://creatorvn.com/images/practical/3.jpg'),
(145, 78, '4.jpg', 'http://creatorvn.com/images/practical/4.jpg'),
(146, 79, '5.jpg', 'http://creatorvn.com/images/practical/5.jpg'),
(150, 79, '9.jpg', 'http://creatorvn.com/images/practical/9.jpg'),
(151, 79, '10.jpg', 'http://creatorvn.com/images/practical/10.jpg'),
(152, 79, '11.jpg', 'http://creatorvn.com/images/practical/11.jpg'),
(153, 79, '12.jpg', 'http://creatorvn.com/images/practical/12.jpg'),
(154, 79, '13.jpg', 'http://creatorvn.com/images/practical/13.jpg'),
(155, 79, '6.jpg', 'http://creatorvn.com/images/practical/6.jpg'),
(156, 75, '4.png', 'http://creatorvn.com/images/practical/4.png'),
(157, 75, '5.png', 'http://creatorvn.com/images/practical/5.png'),
(158, 75, '6.png', 'http://creatorvn.com/images/practical/6.png'),
(159, 75, '7.png', 'http://creatorvn.com/images/practical/7.png'),
(160, 75, '8.png', 'http://creatorvn.com/images/practical/8.png'),
(161, 75, '9.png', 'http://creatorvn.com/images/practical/9.png'),
(162, 75, '10.png', 'http://creatorvn.com/images/practical/10.png'),
(163, 75, '11.png', 'http://creatorvn.com/images/practical/11.png'),
(164, 75, '12.png', 'http://creatorvn.com/images/practical/12.png'),
(165, 75, '13.png', 'http://creatorvn.com/images/practical/13.png'),
(166, 75, '14.png', 'http://creatorvn.com/images/practical/14.png'),
(167, 75, '15.png', 'http://creatorvn.com/images/practical/15.png'),
(168, 75, '16.png', 'http://creatorvn.com/images/practical/16.png'),
(169, 75, '21.png', 'http://creatorvn.com/images/practical/21.png'),
(170, 75, '22.png', 'http://creatorvn.com/images/practical/22.png'),
(171, 75, '23.png', 'http://creatorvn.com/images/practical/23.png'),
(172, 75, '24.png', 'http://creatorvn.com/images/practical/24.png'),
(173, 75, '27.png', 'http://creatorvn.com/images/practical/27.png'),
(174, 56, 'GB025.png', 'http://creatorvn.com/images/product/GB025.png'),
(175, 56, 'GB025-1.png', 'http://creatorvn.com/images/product/GB025-1.png'),
(176, 44, 'GB013-1.jpg', 'http://creatorvn.com/images/product/GB013-1.jpg'),
(177, 44, 'GB013-2.jpg', 'http://creatorvn.com/images/product/GB013-2.jpg'),
(178, 44, 'GB013-3.jpg', 'http://creatorvn.com/images/product/GB013-3.jpg'),
(179, 44, 'GB013-4.jpg', 'http://creatorvn.com/images/product/GB013-4.jpg'),
(216, 82, '111.png', 'http://creatorvn.com/images/practical/111.png'),
(217, 82, '112.png', 'http://creatorvn.com/images/practical/112.png'),
(218, 82, '113.png', 'http://creatorvn.com/images/practical/113.png'),
(219, 82, '114.png', 'http://creatorvn.com/images/practical/114.png'),
(220, 82, '115.png', 'http://creatorvn.com/images/practical/115.png'),
(221, 82, '116.png', 'http://creatorvn.com/images/practical/116.png'),
(222, 82, '117.png', 'http://creatorvn.com/images/practical/117.png'),
(223, 82, '118.png', 'http://creatorvn.com/images/practical/118.png'),
(225, 83, 'Creator 111.png', 'http://creatorvn.com/images/practical/Creator 111.png'),
(226, 83, 'Creator 222.png', 'http://creatorvn.com/images/practical/Creator 222.png'),
(227, 83, 'Creator 444.png', 'http://creatorvn.com/images/practical/Creator 444.png'),
(228, 83, 'CreatorVN\'s 1.png', 'http://creatorvn.com/images/practical/CreatorVN\'s 1.png'),
(229, 83, 'i1.jpg', 'http://creatorvn.com/images/practical/i1.jpg'),
(230, 83, 'i1.png', 'http://creatorvn.com/images/practical/i1.png'),
(231, 83, 'i3.png', 'http://creatorvn.com/images/practical/i3.png'),
(232, 83, 'i4.png', 'http://creatorvn.com/images/practical/i4.png'),
(233, 83, 'i5.png', 'http://creatorvn.com/images/practical/i5.png'),
(234, 83, 'i6.png', 'http://creatorvn.com/images/practical/i6.png'),
(242, 84, 'L04.png', 'http://creatorvn.com/images/practical/L04.png'),
(243, 84, 'L05.png', 'http://creatorvn.com/images/practical/L05.png'),
(244, 84, 'L01.png', 'http://creatorvn.com/images/practical/L01.png'),
(245, 84, 'L02.png', 'http://creatorvn.com/images/practical/L02.png'),
(246, 84, 'L03.png', 'http://creatorvn.com/images/practical/L03.png'),
(247, 84, 'L06.jpg', 'http://creatorvn.com/images/practical/L06.jpg'),
(248, 84, 'L07.png', 'http://creatorvn.com/images/practical/L07.png'),
(249, 59, 'GL007.png', 'http://creatorvn.com/images/product/GL007.png'),
(250, 85, 'CC1 (88).png', 'http://creatorvn.com/images/practical/CC1 (88).png'),
(251, 85, 'CC1 (89).png', 'http://creatorvn.com/images/practical/CC1 (89).png'),
(252, 85, 'CC1 (87).png', 'http://creatorvn.com/images/practical/CC1 (87).png'),
(253, 85, 'CC1 (86).png', 'http://creatorvn.com/images/practical/CC1 (86).png'),
(254, 85, 'CC1 (85).png', 'http://creatorvn.com/images/practical/CC1 (85).png'),
(255, 85, 'CC1 (84).png', 'http://creatorvn.com/images/practical/CC1 (84).png'),
(256, 85, 'CC1 (82).png', 'http://creatorvn.com/images/practical/CC1 (82).png'),
(257, 85, 'CC2 (83).png', 'http://creatorvn.com/images/practical/CC2 (83).png'),
(259, 89, 'D002-1.png', 'http://creatorvn.com/images/product/D002-1.png'),
(260, 89, 'D002-2.png', 'http://creatorvn.com/images/product/D002-2.png'),
(261, 89, 'D002-3.png', 'http://creatorvn.com/images/product/D002-3.png'),
(262, 90, 'D003.png', 'http://creatorvn.com/images/product/D003.png'),
(263, 90, 'D003-1.png', 'http://creatorvn.com/images/product/D003-1.png'),
(264, 90, 'D003-2.png', 'http://creatorvn.com/images/product/D003-2.png'),
(265, 91, 'D004.png', 'http://creatorvn.com/images/product/D004.png'),
(266, 91, 'D004-2.png', 'http://creatorvn.com/images/product/D004-2.png'),
(267, 91, 'D004-3.png', 'http://creatorvn.com/images/product/D004-3.png'),
(268, 93, 'D006.png', 'http://creatorvn.com/images/product/D006.png'),
(269, 95, 'D008-1.png', 'http://creatorvn.com/images/product/D008-1.png'),
(270, 97, 'D010-1.png', 'http://creatorvn.com/images/product/D010-1.png'),
(271, 98, 'D011.png', 'http://creatorvn.com/images/product/D011.png'),
(272, 98, 'D011-2.png', 'http://creatorvn.com/images/product/D011-2.png'),
(273, 88, 'D001.png', 'http://creatorvn.com/images/product/D001.png'),
(274, 88, 'D001-2.png', 'http://creatorvn.com/images/product/D001-2.png'),
(275, 99, 'GB027-1.png', 'http://creatorvn.com/images/product/GB027-1.png'),
(276, 102, 'GB030-1.png', 'http://creatorvn.com/images/product/GB030-1.png'),
(277, 102, 'GB030-2.png', 'http://creatorvn.com/images/product/GB030-2.png'),
(278, 102, 'GB030-3.png', 'http://creatorvn.com/images/product/GB030-3.png'),
(279, 104, 'GB032-1.png', 'http://creatorvn.com/images/product/GB032-1.png'),
(280, 106, 'GB034-1.png', 'http://creatorvn.com/images/product/GB034-1.png'),
(281, 109, 'GB037-1.png', 'http://creatorvn.com/images/product/GB037-1.png'),
(282, 110, 'GB038-1.png', 'http://creatorvn.com/images/product/GB038-1.png'),
(283, 111, 'GB039-1.png', 'http://creatorvn.com/images/product/GB039-1.png'),
(284, 112, 'GB040-1.png', 'http://creatorvn.com/images/product/GB040-1.png'),
(285, 113, 'GB041-1.png', 'http://creatorvn.com/images/product/GB041-1.png'),
(286, 113, 'GB041-2.png', 'http://creatorvn.com/images/product/GB041-2.png'),
(287, 114, 'GL011-2.png', 'http://creatorvn.com/images/product/GL011-2.png'),
(288, 118, 'GL015-1.png', 'http://creatorvn.com/images/product/GL015-1.png'),
(289, 122, 'GL019.png', 'http://creatorvn.com/images/product/GL019.png'),
(290, 125, 'GL022-1.png', 'http://creatorvn.com/images/product/GL022-1.png'),
(291, 125, 'GL022-2.png', 'http://creatorvn.com/images/product/GL022-2.png'),
(292, 125, 'GL022-3.png', 'http://creatorvn.com/images/product/GL022-3.png'),
(293, 125, 'GL022-4.png', 'http://creatorvn.com/images/product/GL022-4.png'),
(294, 126, 'GL023-1.png', 'http://creatorvn.com/images/product/GL023-1.png'),
(295, 126, 'GL023-2.png', 'http://creatorvn.com/images/product/GL023-2.png'),
(296, 127, 'AC001.png', 'http://creatorvn.com/images/product/AC001.png'),
(297, 127, 'AC001-2.png', 'http://creatorvn.com/images/product/AC001-2.png'),
(298, 128, 'AC003-1.png', 'http://creatorvn.com/images/product/AC003-1.png'),
(299, 128, 'AC003-2.png', 'http://creatorvn.com/images/product/AC003-2.png'),
(300, 128, 'AC003-3.png', 'http://creatorvn.com/images/product/AC003-3.png'),
(301, 129, 'AC004-1.png', 'http://creatorvn.com/images/product/AC004-1.png');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `hoadon`
--

CREATE TABLE `hoadon` (
  `id` int(11) NOT NULL,
  `mahm` varchar(255) DEFAULT NULL,
  `hotenkh` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `sdt` varchar(255) DEFAULT NULL,
  `diachi` varchar(255) DEFAULT NULL,
  `tongtien` int(11) DEFAULT NULL,
  `tinhtrang` int(11) DEFAULT '0',
  `ngaygiao` timestamp NULL DEFAULT NULL,
  `created` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `isdeleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `hoadon`
--

INSERT INTO `hoadon` (`id`, `mahm`, `hotenkh`, `email`, `sdt`, `diachi`, `tongtien`, `tinhtrang`, `ngaygiao`, `created`, `isdeleted`) VALUES
(14, NULL, 'vv', 'v', 'v', 'v', 8500000, 0, NULL, '2018-09-25 02:15:30', 0),
(15, NULL, 'nam van bui', 'levvanh@gmail.com', '085612562351230', '21321 35dsfsd', 13000000, 0, NULL, '2018-09-25 12:24:54', 0),
(16, NULL, 'Lê Thị Thu Huyền', 'thuhuyen2fly@gmail.com', '969824508', '11 Lê Thước , Thảo Điền,Quận 2 , TP Hồ Chí Minh', 1800000, 0, NULL, '2018-09-26 04:59:32', 0),
(17, NULL, 'ád', 'sdasdasd', 'ádádá', 'ádas', 4000000, 0, NULL, '2018-09-27 12:15:47', 0);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `loaisanpham`
--

CREATE TABLE `loaisanpham` (
  `id` int(11) NOT NULL,
  `tenloaisp0` varchar(255) DEFAULT NULL,
  `tenloaisp` varchar(255) DEFAULT NULL,
  `hinh` varchar(255) DEFAULT NULL,
  `isdeleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `loaisanpham`
--

INSERT INTO `loaisanpham` (`id`, `tenloaisp0`, `tenloaisp`, `hinh`, `isdeleted`) VALUES
(0, NULL, 'Sản phẩm thực tế', NULL, 0),
(1, 'Bàn', 'Bàn trà - Bàn sofa', 'https://creatorvn.com/images/category/CreatorVN-coffe-table.jpg', 0),
(2, 'Ghế', 'Ghế sofa băng', 'http://creatorvn.com/images/category/261cd704d6dcdbd0f657655d6665fcf1.jpg', 0),
(3, 'Bàn', 'Bàn làm việc', 'http://creatorvn.com/images/category/BLV.jpg', 0),
(4, 'Ghế', 'Ghế sofa góc', 'http://creatorvn.com/images/category/sofa góc.jpg', 0),
(8, 'Bàn', 'Bàn ăn', 'http://creatorvn.com/images/category/15.jpg', 0),
(9, 'new', 'new 1', 'https://creatorvn.com/images/category/test.jpg', 1),
(10, 'Ghế', 'Đôn', 'http://creatorvn.com/images/category/D004-3.png', 0),
(11, 'Ghế', 'Arm Chair', 'http://creatorvn.com/images/category/AC002.png', 0);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `sanpham`
--

CREATE TABLE `sanpham` (
  `id` int(11) NOT NULL,
  `tensp` varchar(255) DEFAULT NULL,
  `maloaisp` int(11) DEFAULT NULL,
  `dacta` text,
  `gia` int(11) DEFAULT '0',
  `hinh` varchar(255) DEFAULT NULL,
  `soluongviews` int(11) DEFAULT '0',
  `soluongban` int(11) DEFAULT '0',
  `giamgia` int(11) DEFAULT '0',
  `ngaykt` timestamp NULL DEFAULT NULL,
  `isdeleted` int(11) DEFAULT '0',
  `created` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `sanpham`
--

INSERT INTO `sanpham` (`id`, `tensp`, `maloaisp`, `dacta`, `gia`, `hinh`, `soluongviews`, `soluongban`, `giamgia`, `ngaykt`, `isdeleted`, `created`) VALUES
(1, 'BS001 (Mặt gỗ)', 1, '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<ul>\r\n<li>K&iacute;ch thước: D100xR50xC45 cm</li>\r\n<li>Khung sắt sơn đen tĩnh điện</li>\r\n<li>Mặt b&agrave;n gỗ phủ verneer v&acirc;n sồi</li>\r\n</ul>\r\n</body>\r\n</html>', 2000000, 'http://creatorvn.com/images/product/BS001 (Mặt gỗ).jpg', 250, 80, 0, '2018-08-02 08:38:04', 0, '2018-08-02 08:38:04'),
(2, 'BS001 (Mặt đá)', 1, '<ul>\r\n<li>K&iacute;ch thước: D100xR50xC45 cm</li>\r\n<li>Khung sắt sơn đen tĩnh điện</li>\r\n<li>Mặt b&agrave;n đ&aacute; Moca trắng v&acirc;n m&acirc;y</li>\r\n</ul>', 1800000, 'https://creatorvn.com/images/product/BS001 (Mặt đá).png', 360, 0, 0, '2018-08-24 15:46:00', 0, '2018-08-24 15:46:00'),
(3, 'BS002 (Mặt đá)', 1, '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<ul>\r\n<li>K&iacute;ch thước: D30xR50xC67 cm</li>\r\n<li>Khung sắt sơn đen tĩnh điện</li>\r\n<li>Mặt b&agrave;n đ&aacute; Moca trắng v&acirc;n m&acirc;y</li>\r\n</ul>\r\n</body>\r\n</html>', 1000000, 'http://creatorvn.com/images/product/BS002 (Mặt đá).jpg', 150, 0, 0, '2018-08-11 07:13:55', 0, '2018-08-11 07:13:55'),
(4, 'BS002 (Mặt gỗ)', 1, '<ul>\r\n<li>K&iacute;ch thước: D30xR50xC67 cm</li>\r\n<li>Khung sắt sơn đen tĩnh điện</li>\r\n<li>Mặt b&agrave;n gỗ phủ verneer&nbsp; v&acirc;n sồi</li>\r\n</ul>', 900000, 'https://creatorvn.com/images/product/BS002 (Mặt gỗ).jpg', 220, 0, 10, '2018-08-24 15:46:00', 0, '2018-08-24 15:47:10'),
(5, 'BS004', 1, '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<ul>\r\n<li>K&iacute;ch thước: D70 xC45 cm</li>\r\n<li>Khung sắt sơn đen tĩnh điện</li>\r\n<li>Mặt b&agrave;n đ&aacute; Moca trắng v&acirc;n m&acirc;y</li>\r\n</ul>\r\n</body>\r\n</html>', 1900000, 'http://creatorvn.com/images/product/BS004.jpg', 120, 0, 0, '2018-08-11 07:13:55', 0, '2018-08-24 15:46:55'),
(6, 'BS005', 1, '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<ul>\r\n<li>K&iacute;ch thước: D80xC45 cm &amp; D60xC40 cm</li>\r\n<li>Khung sắt sơn đen tĩnh điện</li>\r\n<li>Mặt b&agrave;n đ&aacute; Moca trắng v&acirc;n m&acirc;y</li>\r\n</ul>\r\n</body>\r\n</html>', 3900000, 'http://creatorvn.com/images/product/BS005.jpg', 138, 0, 0, '2018-08-11 07:13:55', 0, '2018-08-11 07:13:55'),
(7, 'BS006', 1, '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<ul>\r\n<li>K&iacute;ch thước: D70xC45 cm</li>\r\n<li>Ch&acirc;n inox</li>\r\n<li>Mặt b&agrave;n đ&aacute; Moca trắng v&acirc;n m&acirc;y</li>\r\n</ul>\r\n</body>\r\n</html>', 2100000, 'http://creatorvn.com/images/product/BS006.jpg', 600, 540, 0, '2001-01-01 00:00:00', 0, '2018-08-24 15:46:00'),
(8, 'BS008', 1, '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<ul>\r\n<li>K&iacute;ch thước: D60xR60xC45 cm</li>\r\n<li>Khung sắt sơn đen tĩnh điện</li>\r\n<li>Mặt b&agrave;n đ&aacute; Moca trắng v&acirc;n m&acirc;y</li>\r\n</ul>\r\n</body>\r\n</html>', 1800000, 'http://creatorvn.com/images/product/BS008.jpg', 241, 0, 0, '2018-08-11 07:13:55', 0, '2018-08-11 07:13:55'),
(9, 'BS009', 1, '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<ul>\r\n<li>K&iacute;ch thước: D60xR60xC45 cm</li>\r\n<li>Khung sắt sơn đen tĩnh điện</li>\r\n<li>Mặt b&agrave;n đ&aacute; Moca trắng v&acirc;n m&acirc;y</li>\r\n</ul>\r\n</body>\r\n</html>', 1850000, 'http://creatorvn.com/images/product/BS009.jpg', 121, 0, 0, '2018-08-11 07:13:55', 0, '2018-08-11 07:13:55'),
(10, 'BS010', 1, '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<ul>\r\n<li>K&iacute;ch thước: D70xC45 cm</li>\r\n<li>Khung sắt sơn đen tĩnh điện</li>\r\n<li>Mặt b&agrave;n đ&aacute; Moca trắng v&acirc;n m&acirc;y</li>\r\n</ul>\r\n</body>\r\n</html>', 1700000, 'http://creatorvn.com/images/product/BS010.jpg', 250, 0, 0, '2018-08-11 07:13:55', 0, '2018-08-11 07:13:55'),
(11, 'BS016', 1, '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<ul>\r\n<li>K&iacute;ch thước: D100xR50xC45 cm</li>\r\n<li>Khung sắt sơn đen tĩnh điện</li>\r\n<li>Mặt b&agrave;n đ&aacute; Moca trắng v&acirc;n m&acirc;y</li>\r\n</ul>\r\n</body>\r\n</html>', 1800000, 'http://creatorvn.com/images/product/BS016.jpg', 223, 0, 0, '2018-08-11 07:13:55', 0, '2018-08-11 07:13:55'),
(12, 'BS017', 1, '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<ul>\r\n<li>K&iacute;ch thước: D100xR50xC45 cm</li>\r\n<li>Khung sắt sơn đen tĩnh điện</li>\r\n<li>Mặt b&agrave;n đ&aacute; Moca trắng v&acirc;n m&acirc;y</li>\r\n</ul>\r\n</body>\r\n</html>', 2000000, 'http://creatorvn.com/images/product/BS017.jpg', 457, 440, 0, '2018-08-11 07:13:55', 0, '2018-08-11 07:13:55'),
(13, 'BS019', 1, '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<ul>\r\n<li>K&iacute;ch thước: D70xC45 cm</li>\r\n<li>Khung sắt sơn đen tĩnh điện</li>\r\n<li>Mặt b&agrave;n đ&aacute; Moca trắng v&acirc;n m&acirc;y</li>\r\n</ul>\r\n</body>\r\n</html>', 2000000, 'http://creatorvn.com/images/product/BS019.jpg', 559, 450, 0, '2018-08-11 07:13:55', 0, '2018-08-11 07:13:55'),
(14, 'BS020', 1, '<ul>\r\n<li>K&iacute;ch thước: D70xC45 cm</li>\r\n<li>Khung inox</li>\r\n<li>Mặt b&agrave;n gỗ phủ verneer v&acirc;n sồi</li>\r\n</ul>', 2500000, 'https://creatorvn.com/images/product/BS020.png', 235, 0, 0, '2018-08-11 07:13:55', 0, '2018-08-11 07:13:55'),
(15, 'BS027', 1, '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<ul>\r\n<li>K&iacute;ch thước: D70xC45 cm</li>\r\n<li>Khung sắt sơn đen tĩnh điện</li>\r\n<li>Mặt b&agrave;n gỗ phủ verneer v&acirc;n sồi</li>\r\n</ul>\r\n</body>\r\n</html>', 2500000, 'http://creatorvn.com/images/product/BS027.png', 125, 0, 0, '2018-08-11 07:13:55', 0, '2018-08-11 07:13:55'),
(16, 'BS025', 1, '<ul>\r\n<li>K&iacute;ch thước: D100xR50xC45 cm</li>\r\n<li>Khung sắt sơn đen tĩnh điện</li>\r\n<li>Mặt b&agrave;n gỗ sơn đen</li>\r\n</ul>', 2000000, 'https://creatorvn.com/images/product/BS025.png', 455, 0, 0, '2018-08-11 07:13:55', 0, '2018-08-11 07:13:55'),
(17, 'BS026', 1, '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<ul>\r\n<li>K&iacute;ch thước: D100xR50xC45 cm</li>\r\n<li>Ch&acirc;n gỗ</li>\r\n<li>Mặt b&agrave;n đ&aacute; Moca trắng v&acirc;n m&acirc;y</li>\r\n</ul>\r\n</body>\r\n</html>', 4000000, 'http://creatorvn.com/images/product/BS026.png', 458, 0, 0, '2018-08-11 07:13:55', 0, '2018-08-11 07:13:55'),
(18, 'BLV001', 3, '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<ul>\r\n<li>K&iacute;ch thước: D120xR50xC75</li>\r\n<li>Ch&acirc;n sắt sơn tĩnh điện</li>\r\n<li>Mặt b&agrave;n gỗ</li>\r\n</ul>\r\n</body>\r\n</html>', 2500000, 'https://creatorvn.com/images/product/BLV001.jpg', 875, 0, 0, '2018-08-24 15:46:00', 1, '2018-08-28 03:24:12'),
(19, 'BLV001', 3, '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<ul>\r\n<li>K&iacute;ch thước: D120xR50xC75</li>\r\n<li>Ch&acirc;n sắt sơn tĩnh điện</li>\r\n<li>Mặt b&agrave;n gỗ</li>\r\n</ul>\r\n</body>\r\n</html>', 2500000, 'http://creatorvn.com/images/product/001.jpg', 875, 450, 0, '2018-08-11 07:13:55', 0, '2018-08-11 07:13:55'),
(20, 'BLV002', 3, '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<ul>\r\n<li>K&iacute;ch thước: D120xR50xC75</li>\r\n<li>Ch&acirc;n sắt sơn tĩnh điện</li>\r\n<li>Mặt b&agrave;n gỗ</li>\r\n</ul>\r\n</body>\r\n</html>', 2200000, 'http://creatorvn.com/images/product/002.jpg', 875, 420, 0, '2018-08-11 07:13:55', 0, '2018-08-11 07:13:55'),
(21, 'BLV003', 3, '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<ul>\r\n<li>K&iacute;ch thước: D120xR50xC75</li>\r\n<li>Ch&acirc;n sắt sơn tĩnh điện</li>\r\n<li>Mặt b&agrave;n gỗ</li>\r\n</ul>\r\n</body>\r\n</html>', 3000000, 'https://creatorvn.com/images/product/BLV003.jpg', 650, 0, 0, '2018-08-24 15:46:00', 1, '2018-08-28 03:24:31'),
(22, 'BLV003', 3, '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<ul>\r\n<li>K&iacute;ch thước: D120xR50xC75</li>\r\n<li>Ch&acirc;n sắt sơn tĩnh điện</li>\r\n<li>Mặt b&agrave;n gỗ</li>\r\n</ul>\r\n</body>\r\n</html>', 3000000, 'http://creatorvn.com/images/product/003.jpg', 650, 320, 0, '2018-08-11 07:13:55', 0, '2018-08-11 07:13:55'),
(23, 'BLV005', 3, '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<ul>\r\n<li>K&iacute;ch thước: D120xR50xC75</li>\r\n<li>Ch&acirc;n sắt sơn tĩnh điện</li>\r\n<li>Mặt b&agrave;n gỗ</li>\r\n</ul>\r\n</body>\r\n</html>', 2850000, 'http://creatorvn.com/images/product/005.jpg', 534, 210, 0, '2018-08-11 07:13:55', 0, '2018-08-11 07:13:55'),
(24, 'BLV006', 3, '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<ul>\r\n<li>K&iacute;ch thước: D120xR50xC75</li>\r\n<li>Ch&acirc;n sắt sơn tĩnh điện</li>\r\n<li>Mặt b&agrave;n gỗ</li>\r\n</ul>\r\n</body>\r\n</html>', 2500000, 'http://creatorvn.com/images/product/006.jpg', 521, 220, 0, '2018-08-11 07:13:55', 0, '2018-08-11 07:13:55'),
(25, 'BLV007', 3, '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<ul>\r\n<li>K&iacute;ch thước: D120xR50xC75</li>\r\n<li>Ch&acirc;n sắt sơn tĩnh điện</li>\r\n<li>Mặt b&agrave;n gỗ</li>\r\n</ul>\r\n</body>\r\n</html>', 2800000, 'http://creatorvn.com/images/product/007.jpg', 521, 120, 0, '2018-08-11 07:13:55', 0, '2018-08-11 07:13:55'),
(26, 'BLV008', 3, '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<ul>\r\n<li>K&iacute;ch thước: D120xR50xC75</li>\r\n<li>Ch&acirc;n sắt sơn tĩnh điện</li>\r\n<li>Mặt b&agrave;n gỗ</li>\r\n</ul>\r\n</body>\r\n</html>', 2400000, 'http://creatorvn.com/images/product/008.jpg', 539, 130, 0, '2018-08-11 07:13:55', 0, '2018-08-11 07:13:55'),
(27, 'BLV009', 3, '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<ul>\r\n<li>K&iacute;ch thước: D120xR50xC75</li>\r\n<li>Ch&acirc;n sắt sơn tĩnh điện</li>\r\n<li>Mặt b&agrave;n gỗ</li>\r\n</ul>\r\n</body>\r\n</html>', 2300000, 'http://creatorvn.com/images/product/009.jpg', 582, 0, 0, '2018-08-11 07:13:55', 0, '2018-08-11 07:13:55'),
(28, 'BLV010', 3, '<ul>\r\n<li>K&iacute;ch thước: D120xR50xC75</li>\r\n<li>Ch&acirc;n sắt sơn tĩnh điện</li>\r\n<li>Mặt b&agrave;n gỗ</li>\r\n</ul>', 3000000, 'https://creatorvn.com/images/product/BLV011.jpg', 335, 0, 0, '2018-08-11 07:13:55', 1, '2018-08-11 07:13:55'),
(29, 'BLV011', 3, '<ul>\r\n<li>K&iacute;ch thước: D120xR50xC75</li>\r\n<li>Ch&acirc;n sắt sơn tĩnh điện</li>\r\n<li>Mặt b&agrave;n gỗ</li>\r\n</ul>', 2400000, 'https://creatorvn.com/images/product/BLV011.jpg', 375, 0, 0, '2018-08-11 07:13:55', 1, '2018-08-11 07:13:55'),
(30, 'BLV012', 3, '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<ul>\r\n<li>K&iacute;ch thước: D120xR50xC75</li>\r\n<li>Ch&acirc;n sắt sơn tĩnh điện</li>\r\n<li>Mặt b&agrave;n gỗ</li>\r\n</ul>\r\n</body>\r\n</html>', 2650000, 'http://creatorvn.com/images/product/012.jpg', 675, 110, 0, '2018-08-11 07:13:55', 0, '2018-08-11 07:13:55'),
(31, 'BLV013', 3, '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<ul>\r\n<li>K&iacute;ch thước: D120xR50xC75</li>\r\n<li>Ch&acirc;n sắt sơn tĩnh điện</li>\r\n<li>Mặt b&agrave;n gỗ</li>\r\n</ul>\r\n</body>\r\n</html>', 2750000, 'http://creatorvn.com/images/product/013.jpg', 751, 250, 0, '2018-08-11 07:13:55', 0, '2018-08-11 07:13:55'),
(32, 'GB001', 2, '<ul>\r\n<li>K&iacute;ch thước: D200xC75xS80</li>\r\n<li>Khung gỗ dầu 100%</li>\r\n<li>Ch&acirc;n sắt</li>\r\n</ul>', 13000000, 'https://creatorvn.com/images/product/GB001.png', 771, 0, 0, '2018-08-11 07:13:55', 0, '2018-08-11 07:13:55'),
(33, 'GB002', 2, '<ul>\r\n<li>K&iacute;ch thước: D200xC75xS80</li>\r\n<li>Khung gỗ dầu 100%</li>\r\n<li>Ch&acirc;n gỗ tự nhi&ecirc;n</li>\r\n</ul>', 13000000, 'https://creatorvn.com/images/product/GB002.jpg', 651, 0, 0, '2018-08-11 07:13:55', 0, '2018-08-11 07:13:55'),
(34, 'GB003', 2, '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<ul>\r\n<li>K&iacute;ch thước: D220xC75xS80</li>\r\n<li>Khung gỗ dầu 100%</li>\r\n<li>Ch&acirc;n gỗ tự nhi&ecirc;n</li>\r\n</ul>\r\n</body>\r\n</html>', 13000000, 'http://creatorvn.com/images/product/GB003.jpg', 651, 0, 0, '2018-08-11 07:13:55', 0, '2018-08-11 07:13:55'),
(35, 'GB004', 2, '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<ul>\r\n<li>K&iacute;ch thước: D220xC75xS80</li>\r\n<li>Khung gỗ dầu 100%</li>\r\n<li>Ch&acirc;n gỗ tự nhi&ecirc;n</li>\r\n</ul>\r\n</body>\r\n</html>', 10000000, 'http://creatorvn.com/images/product/GB004.png', 835, 0, 0, '2018-08-11 07:13:55', 0, '2018-08-11 07:13:55'),
(36, 'GB005', 2, '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<ul>\r\n<li>K&iacute;ch thước: D200xC80xS85</li>\r\n<li>Khung gỗ dầu 100%</li>\r\n<li>Ch&acirc;n gỗ&nbsp;</li>\r\n</ul>\r\n</body>\r\n</html>', 13000000, 'http://creatorvn.com/images/product/GB005.png', 865, 0, 0, '2018-08-11 07:13:55', 0, '2018-08-11 07:13:55'),
(37, 'GB006', 2, '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<ul>\r\n<li>K&iacute;ch thước: D200xC75xS85</li>\r\n<li>Khung gỗ dầu 100%</li>\r\n<li>Ch&acirc;n sắt</li>\r\n</ul>\r\n</body>\r\n</html>', 10000000, 'http://creatorvn.com/images/product/GB006.png', 865, 620, 0, '2018-08-11 07:13:55', 0, '2018-08-11 07:13:55'),
(38, 'GB009', 2, '<ul>\r\n<li>K&iacute;ch thước: D200xC75xS80</li>\r\n<li>Khung gỗ dầu 100%</li>\r\n<li>Ch&acirc;n sắt</li>\r\n</ul>', 12000000, 'https://creatorvn.com/images/product/GB009.png', 541, 0, 0, '2018-08-11 07:13:55', 0, '2018-08-11 07:13:55'),
(39, 'GB007', 2, '<ul>\r\n<li>K&iacute;ch thước: D200xC75xS80</li>\r\n<li>Khung gỗ dầu 100%</li>\r\n<li>Ch&acirc;n sắt</li>\r\n</ul>', 10000000, 'https://creatorvn.com/images/product/GB007.png', 653, 0, 0, '2018-08-11 07:13:55', 1, '2018-08-11 07:13:55'),
(40, 'GB008', 2, '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<ul>\r\n<li>K&iacute;ch thước: D200xC75xS80</li>\r\n<li>Khung gỗ dầu 100%</li>\r\n<li>Ch&acirc;n sắt</li>\r\n</ul>\r\n</body>\r\n</html>', 11000000, 'http://creatorvn.com/images/product/GB008.png', 784, 0, 0, '2018-08-11 07:13:55', 0, '2018-08-11 07:13:55'),
(41, 'GB010', 2, '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<ul>\r\n<li>K&iacute;ch thước: D200xC80xS90</li>\r\n<li>Khung gỗ dầu 100%</li>\r\n<li>Ch&acirc;n sắt</li>\r\n</ul>\r\n</body>\r\n</html>', 12000000, 'http://creatorvn.com/images/product/GB010.png', 814, 620, 0, '2018-08-11 07:13:55', 0, '2018-08-11 07:13:55'),
(42, 'GB011', 2, '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<ul>\r\n<li>K&iacute;ch thước: D200xC80xS80</li>\r\n<li>Khung gỗ dầu 100%</li>\r\n<li>Ch&acirc;n sắt</li>\r\n</ul>\r\n</body>\r\n</html>', 13000000, 'http://creatorvn.com/images/product/GB011.jpg', 958, 0, 0, '2018-08-11 07:13:55', 0, '2018-08-11 07:13:55'),
(43, 'GB012', 2, '<ul>\r\n<li>K&iacute;ch thước: D200xC75xS80</li>\r\n<li>Khung gỗ dầu 100%</li>\r\n<li>Ch&acirc;n sắt</li>\r\n</ul>', 10000000, 'https://creatorvn.com/images/product/GB012.jpg', 857, 0, 0, '2018-08-11 07:13:55', 0, '2018-08-11 07:13:55'),
(44, 'GB013', 2, '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<ul>\r\n<li>K&iacute;ch thước: D200xC80xS80</li>\r\n<li>Khung gỗ dầu 100%</li>\r\n<li>Ch&acirc;n sắt</li>\r\n</ul>\r\n</body>\r\n</html>', 10000000, 'http://creatorvn.com/images/product/GB013.jpg', 980, 0, 0, '2018-08-11 07:13:55', 0, '2018-08-11 07:13:55'),
(45, 'GB014', 2, '<ul>\r\n<li>K&iacute;ch thước: D200xC80xS80</li>\r\n<li>Khung gỗ dầu 100%</li>\r\n<li>Ch&acirc;n gỗ tự nhi&ecirc;n</li>\r\n</ul>', 12000000, 'https://creatorvn.com/images/product/GB014.png', 659, 0, 0, '2018-08-11 07:13:55', 0, '2018-08-11 07:13:55'),
(46, 'GB015', 2, '<ul>\r\n<li>K&iacute;ch thước: D200xC80xS80</li>\r\n<li>Khung gỗ dầu 100%</li>\r\n<li>Ch&acirc;n gỗ tự nhi&ecirc;n</li>\r\n</ul>', 14000000, 'https://creatorvn.com/images/product/GB015.png', 894, 0, 0, '2018-08-11 07:13:55', 0, '2018-08-11 07:13:55'),
(47, 'GB016', 2, '<ul>\r\n<li>K&iacute;ch thước: D200xC75xS80</li>\r\n<li>Khung gỗ dầu 100%</li>\r\n<li>Ch&acirc;n gỗ tự nhi&ecirc;n</li>\r\n</ul>', 12000000, 'https://creatorvn.com/images/product/GB016.jpg', 673, 0, 0, '2018-08-11 07:13:55', 0, '2018-08-11 07:13:55'),
(48, 'GB017', 2, '<ul>\r\n<li>K&iacute;ch thước: D200xC85xS80</li>\r\n<li>Khung gỗ dầu 100%</li>\r\n<li>Ch&acirc;n gỗ tự nhi&ecirc;n</li>\r\n</ul>', 13000000, 'https://creatorvn.com/images/product/GB017.png', 579, 0, 0, '2018-08-11 07:13:55', 0, '2018-08-11 07:13:55'),
(49, 'GB018', 2, '<ul>\r\n<li>K&iacute;ch thước: D200xC80xS80</li>\r\n<li>Khung gỗ dầu 100%</li>\r\n<li>Ch&acirc;n sắt</li>\r\n</ul>', 15000000, 'https://creatorvn.com/images/product/GB018.jpg', 989, 0, 0, '2018-08-11 07:13:55', 0, '2018-08-11 07:13:55'),
(50, 'GB019', 2, '<ul>\r\n<li>K&iacute;ch thước: D200xC70xS80</li>\r\n<li>Khung gỗ dầu 100%</li>\r\n<li>Ch&acirc;n sắt</li>\r\n</ul>', 13000000, 'https://creatorvn.com/images/product/GB019.jpg', 874, 0, 0, '2018-08-11 07:13:55', 0, '2018-08-11 07:13:55'),
(51, 'GB020', 2, '<ul>\r\n<li>K&iacute;ch thước: D200xC70xS80</li>\r\n<li>Khung gỗ dầu 100%</li>\r\n<li>Ch&acirc;n sắt</li>\r\n</ul>', 12000000, 'https://creatorvn.com/images/product/GB020.png', 855, 0, 0, '2018-08-11 07:13:55', 0, '2018-08-11 07:13:55'),
(52, 'GB021', 2, '<ul>\r\n<li>K&iacute;ch thước: D200xC75xS80</li>\r\n<li>Khung gỗ dầu 100%</li>\r\n<li>Ch&acirc;n inox</li>\r\n</ul>', 11000000, 'https://creatorvn.com/images/product/GB021.png', 870, 0, 0, '2018-08-11 07:13:55', 0, '2018-08-11 07:13:55'),
(53, 'GB022', 2, '<ul>\r\n<li>K&iacute;ch thước: D220xC75xS80</li>\r\n<li>Khung gỗ dầu 100%</li>\r\n<li>Ch&acirc;n sắt</li>\r\n</ul>', 13000000, 'https://creatorvn.com/images/product/GB022.png', 800, 0, 0, '2018-08-11 07:13:55', 0, '2018-08-11 07:13:55'),
(54, 'GB023', 2, '<ul>\r\n<li>K&iacute;ch thước: D220xC95xS80</li>\r\n<li>Khung gỗ dầu 100%</li>\r\n<li>Ch&acirc;n sắt</li>\r\n</ul>', 13000000, 'https://creatorvn.com/images/product/GB023.jpg', 547, 0, 0, '2018-08-11 07:13:55', 0, '2018-08-11 07:13:55'),
(55, 'GB024', 2, '<ul>\r\n<li>K&iacute;ch thước: D220xC85xS85</li>\r\n<li>Khung gỗ dầu 100%</li>\r\n<li>Ch&acirc;n sắt</li>\r\n</ul>', 13000000, 'https://creatorvn.com/images/product/GB024.png', 450, 0, 0, '2018-08-11 07:13:55', 1, '2018-08-11 07:13:55'),
(56, 'GB025', 2, '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<ul>\r\n<li>K&iacute;ch thước: D200xC75xS80</li>\r\n<li>Khung gỗ dầu 100%</li>\r\n<li>Ch&acirc;n gỗ sồi</li>\r\n</ul>\r\n</body>\r\n</html>', 14000000, 'http://creatorvn.com/images/product/GB025-2.png', 965, 0, 0, '2018-08-11 07:13:55', 0, '2018-08-11 07:13:55'),
(57, 'GL001', 4, '<ul>\r\n<li>K&iacute;ch thước: (D280 x 170)xC70xS85&nbsp;</li>\r\n<li>Khung gỗ dầu 100%</li>\r\n<li>Ch&acirc;n inox</li>\r\n</ul>', 19000000, 'https://creatorvn.com/images/product/GL001.png', 847, 0, 0, '2018-08-11 07:13:55', 0, '2018-08-11 07:13:55'),
(58, 'GL002', 4, '<ul>\r\n<li>K&iacute;ch thước: (D280 x 170)xC75xS85&nbsp;</li>\r\n<li>Khung gỗ dầu 100%</li>\r\n<li>Ch&acirc;n gỗ tự nhi&ecirc;n</li>\r\n</ul>', 16000000, 'https://creatorvn.com/images/product/GL002.png', 751, 0, 0, '2018-08-11 07:13:55', 0, '2018-08-11 07:13:55'),
(59, 'GL003', 4, '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<ul>\r\n<li>K&iacute;ch thước: (D280 x 170)xC75xS85&nbsp;</li>\r\n<li>Khung gỗ dầu 100%</li>\r\n<li>Ch&acirc;n inox</li>\r\n</ul>\r\n</body>\r\n</html>', 17000000, 'http://creatorvn.com/images/product/GL003.png', 788, 512, 0, '2018-08-11 07:13:55', 0, '2018-08-11 07:13:55'),
(60, 'GL006', 4, '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<ul>\r\n<li>K&iacute;ch thước: (D200 x 200)xC75xS80</li>\r\n<li>Khung gỗ dầu 100%</li>\r\n<li>Ch&acirc;n gỗ tự nhi&ecirc;n</li>\r\n</ul>\r\n</body>\r\n</html>', 14000000, 'http://creatorvn.com/images/product/GL006.jpg', 759, 0, 0, '2018-08-11 07:13:55', 0, '2018-08-11 07:13:55'),
(61, 'GL005', 4, '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<ul>\r\n<li>K&iacute;ch thước: (D250 x 200)xC75xS85&nbsp;</li>\r\n<li>Khung gỗ dầu 100%</li>\r\n<li>Ch&acirc;n gỗ tự nhi&ecirc;n</li>\r\n</ul>\r\n</body>\r\n</html>', 16000000, 'http://creatorvn.com/images/product/GL005.png', 657, 0, 0, '2018-08-11 07:13:55', 0, '2018-08-28 03:24:45'),
(62, 'GL007', 4, '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<ul>\r\n<li>K&iacute;ch thước: (D280 x 170)xC75xS80</li>\r\n<li>Khung gỗ dầu 100%</li>\r\n<li>Ch&acirc;n inox</li>\r\n</ul>\r\n</body>\r\n</html>', 19000000, 'http://creatorvn.com/images/product/GL007.png', 899, 0, 0, '2018-08-11 07:13:55', 1, '2018-08-11 07:13:55'),
(63, 'GL004', 4, '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<ul>\r\n<li>K&iacute;ch thước: (D200 x 200)xC75xS85</li>\r\n<li>Khung gỗ dầu 100%</li>\r\n<li>Ch&acirc;n sắt</li>\r\n</ul>\r\n</body>\r\n</html>', 15000000, 'http://creatorvn.com/images/product/GL004.png', 857, 0, 0, '2018-08-11 07:13:55', 0, '2018-08-11 07:13:55'),
(64, 'GL008', 4, '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<ul>\r\n<li>K&iacute;ch thước: (D280 x 170)xC75xS80</li>\r\n<li>Khung gỗ dầu 100%</li>\r\n<li>Ch&acirc;n inox</li>\r\n</ul>\r\n</body>\r\n</html>', 19000000, 'http://creatorvn.com/images/product/GL008.png', 894, 0, 0, '2018-08-11 07:13:55', 0, '2018-08-11 07:13:55'),
(65, 'GL009', 4, '<ul>\r\n<li>K&iacute;ch thước: (D280 x 170)xC80xS80</li>\r\n<li>Khung gỗ dầu 100%</li>\r\n<li>Ch&acirc;n gỗ tự nhi&ecirc;n</li>\r\n</ul>', 18000000, 'https://creatorvn.com/images/product/GL009.png', 877, 0, 0, '2018-08-11 07:13:55', 0, '2018-08-11 07:13:55'),
(66, 'GL010', 4, '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<ul>\r\n<li>K&iacute;ch thước: (D280 x 170)xC70xS85</li>\r\n<li>Khung gỗ dầu 100%</li>\r\n<li>Ch&acirc;n gỗ tự nhi&ecirc;n</li>\r\n</ul>\r\n</body>\r\n</html>', 17000000, 'http://creatorvn.com/images/product/GL010.png', 963, 0, 0, '2018-08-11 07:13:55', 0, '2018-08-11 07:13:55'),
(67, 'test insert', 1, '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p><strong>this is test</strong></p>\r\n</body>\r\n</html>', 100000, 'https://creatorvn.com/images/product/about-img.jpg', 0, 0, 0, '2001-01-01 00:00:00', 1, '2018-09-02 13:54:32'),
(68, 'test insert', 8, '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p><strong>this is test ban awn</strong></p>\r\n</body>\r\n</html>', 100000, 'https://creatorvn.com/images/product/img-pro-01.jpg', 0, 0, 0, '2001-01-01 00:00:00', 1, '2018-09-02 13:58:02'),
(70, 'New', 8, '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p>test 10MB</p>\r\n<p>&nbsp;</p>\r\n</body>\r\n</html>', 0, 'https://creatorvn.com/images/product/Pizigani_1367_Chart_10MB.jpg', 0, 0, 0, '2001-01-01 00:00:00', 1, '2018-09-02 23:34:35'),
(71, 'BA010', 8, '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<ul>\r\n<li>K&iacute;ch thuớc: (D140xR70); (D160xR80); (D180xR90); (D200xR100) x C75 cm</li>\r\n<li>Ch&acirc;n sắt sơn đen tĩnh điện</li>\r\n<li>Mặt b&agrave;n đ&aacute; (t&ugrave;y chọn)</li>\r\n</ul>\r\n</body>\r\n</html>', 0, 'http://creatorvn.com/images/product/35922828_190020581701980_78711945783410688_n.jpg', 840, 720, 0, '2001-01-01 00:00:00', 0, '2018-09-03 03:21:42'),
(72, 'BA001', 8, '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<ul>\r\n<li>K&iacute;ch thước: (D140xR70); (D160xR80); (D180xR90); (D200xR100) x C75 cm</li>\r\n<li>Ch&acirc;n sắt sơn đen tĩnh điện</li>\r\n<li>Mặt b&agrave;n đ&aacute; (tự chọn)</li>\r\n</ul>\r\n</body>\r\n</html>', 0, 'http://creatorvn.com/images/product/1.jpg', 870, 750, 0, '2001-01-01 00:00:00', 0, '2018-09-03 05:47:08'),
(73, 'Công trình Tôn Thất Đạm, Quận 1, TPHCM', 0, '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p>B&agrave;n giao sản phẩm nội thất cho một c&ocirc;ng ty nằm tr&ecirc;n đường T&ocirc;n Thất Đạm, phường Nguyễn Th&aacute;i B&igrave;nh, quận 1, TPHCM. Đ&acirc;y l&agrave; đơn h&agrave;ng gấp, do đ&oacute;, đội ngũ nh&acirc;n vi&ecirc;n Creator phải chạy đua với thời gian, để trao tận tay kh&aacute;ch h&agrave;ng những sản phẩm chất lượng. Lời khen của kh&aacute;ch l&agrave; động lực gi&uacute;p Creator l&agrave;m việc kh&ocirc;ng biết mệt mỏi. Xin cảm ơn anh B&igrave;nh đ&atilde; tin tưởng Creator&nbsp;</p>\r\n</body>\r\n</html>', 0, NULL, 0, 0, 0, NULL, 0, '2018-09-03 06:39:21'),
(74, 'Biệt thự quận 9, TPHCM', 0, '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p>Vừa qua, Xưởng Creator đ&atilde; b&agrave;n giao 2 ghế Arm chair cho kh&aacute;ch tại quận 9, TPHCM. Cảm ơn qu&yacute; kh&aacute;ch h&agrave;ng đ&atilde; tin tưởng, v&agrave; mong kh&aacute;ch sẽ tiếp tục ủng hộ xưởng Creator trong tương lai</p>\r\n</body>\r\n</html>', 0, NULL, 0, 0, 0, NULL, 0, '2018-09-03 06:52:12'),
(75, 'Căn hộ tại City Garden, quận Bình Thạnh, TPHCM', 0, '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p><span style=\"background-color: #ffffff; color: #626262;\">Với mục đ&iacute;ch cho thu&ecirc; căn hộ, nhưng chị chủ nh&agrave; đặt mục ti&ecirc;u Đẹp-nhưng-Chất lượng đ&atilde; t&igrave;m đến tận Creator trong một chiều th&aacute;ng 7/2018 để kiểm tra chất lượng c&aacute;c sản phẩm do xưởng l&agrave;m.&nbsp;</span>Sau đ&oacute;, bộ sản phẩm bao gồm ghế băng d&agrave;i v&agrave; 2 ghế Arm chair v&agrave; b&agrave;n sofa&nbsp; đ&atilde; ra đời, v&agrave; được giao tận tay đến kh&aacute;ch h&agrave;ng, kh&aacute;ch đ&atilde; feedback rất tốt về sản phẩm. Mong sẽ được tiếp tục hợp t&aacute;c với chị trong tương lai. Cảm ơn chị !!</p>\r\n</body>\r\n</html>', 0, NULL, 0, 0, 0, NULL, 0, '2018-09-03 07:00:43'),
(77, 'Căn hộ tại Phú Mỹ Hưng, quận7, TPHCM', 0, '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p>B&agrave;n giao b&agrave;n sofa&nbsp; ch&acirc;n gỗ sồi v&agrave; sofa cho hai anh chị người nước ngo&agrave;i dễ thương. Khi giao h&agrave;ng, shipper b&ecirc;n Creator c&oacute; hỏi kh&aacute;ch một v&agrave;i c&acirc;u hỏi:</p>\r\n<ul>\r\n<li>Anh chị ngồi c&oacute; thấy &ecirc;m kh&ocirc;ng? - Good good</li>\r\n<li>Anh chị thấy c&oacute; đẹp kh&ocirc;ng? - Good good</li>\r\n<li>Cho em xin v&agrave;i tấm h&igrave;nh nh&eacute; !! - Ok</li>\r\n</ul>\r\n<p><span style=\"color: #1d2129; font-family: Helvetica, Arial, sans-serif; background-color: #ffffff;\">V&agrave; shipper gửi về cho Xưởng 3 tấm h&igrave;nh kỉ niệm với kh&aacute;ch&nbsp;</span></p>\r\n</body>\r\n</html>', 0, NULL, 0, 0, 0, NULL, 0, '2018-09-04 15:22:40'),
(78, 'Căn hộ cho thuê tại Quận 4, TPHCM', 0, '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p>Creator b&agrave;n giao 1 d&agrave;n b&agrave;n ghế cho kh&aacute;ch, do căn hộ của kh&aacute;ch c&oacute; diện t&iacute;ch hơi khi&ecirc;m tốn n&ecirc;n xưởng cũng phải thu nhỏ k&iacute;ch thước sản phẩm lại. Tuy nhỏ nhưng chất lượng vẫn chất như nước cất lu&ocirc;n kh&aacute;ch nh&eacute;</p>\r\n</body>\r\n</html>', 0, NULL, 0, 0, 0, NULL, 0, '2018-09-04 15:29:15'),
(79, 'Ghế Custom', 0, '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p><span style=\"color: #1d2129; font-family: Helvetica, Arial, sans-serif; background-color: #ffffff;\">Kh&aacute;ch của Creator c&oacute; mua 1 bộ ghế ở tận nước ngo&agrave;i xa x&ocirc;i, nhưng v&igrave; nhiều l&yacute; do, n&ecirc;n kh&aacute;ch kh&ocirc;ng kiếm ra th&ecirc;m bộ thứ 2 để tậu về nh&agrave;.</span><span style=\"background-color: #ffffff; color: #1d2129; font-family: Helvetica, Arial, sans-serif;\">&nbsp;Kh&aacute;ch c&oacute; đọc tr&ecirc;n Fanpage v&agrave; t&igrave;m đến Creator trong một buổi trưa m&ugrave;a h&egrave;, Creator nhận l&agrave;m bộ n&agrave;y.&nbsp;</span><span class=\"text_exposed_show\" style=\"background-color: #ffffff; font-family: inherit; color: #1d2129; display: inline;\">Tới ng&agrave;y giao h&agrave;ng, kh&aacute;ch tới Xưởng để xem th&agrave;nh phẩm, v&agrave; thốt l&ecirc;n đầy ngạc nhi&ecirc;n: \"Oh My God\" v&agrave; biểu cảm đầy kinh ngạc.&nbsp;Kh&aacute;ch c&ograve;n hứa hẹn sẽ đặt th&ecirc;m nhiều mẫu kh&oacute; nữa tại Creator. Vậy n&ecirc;n nếu c&oacute; mẫu kh&oacute;, kh&aacute;ch cứ li&ecirc;n hệ với Creator nh&eacute;</span></p>\r\n</body>\r\n</html>', 0, NULL, 0, 0, 0, NULL, 0, '2018-09-04 15:35:17'),
(80, 'GB007', 2, '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<ul>\r\n<li style=\"width: 595px; padding-right: 10px; padding-left: 10px; color: #666666; font-family: -apple-system, BlinkMacSystemFont, \'Segoe UI\', Roboto, \'Helvetica Neue\', Arial, sans-serif, \'Apple Color Emoji\', \'Segoe UI Emoji\', \'Segoe UI Symbol\'; font-size: 15px; background-color: #ffffff;\">K&iacute;ch thước: D200xC75xS85</li>\r\n<li style=\"width: 595px; padding-right: 10px; padding-left: 10px; color: #666666; font-family: -apple-system, BlinkMacSystemFont, \'Segoe UI\', Roboto, \'Helvetica Neue\', Arial, sans-serif, \'Apple Color Emoji\', \'Segoe UI Emoji\', \'Segoe UI Symbol\'; font-size: 15px; background-color: #ffffff;\">Khung gỗ dầu 100%</li>\r\n<li style=\"width: 595px; padding-right: 10px; padding-left: 10px; color: #666666; font-family: -apple-system, BlinkMacSystemFont, \'Segoe UI\', Roboto, \'Helvetica Neue\', Arial, sans-serif, \'Apple Color Emoji\', \'Segoe UI Emoji\', \'Segoe UI Symbol\'; font-size: 15px; background-color: #ffffff;\">Ch&acirc;n sắt</li>\r\n</ul>\r\n</body>\r\n</html>', 10000000, 'http://creatorvn.com/images/product/GB007.png', 730, 534, 0, '2001-01-01 00:00:00', 0, '2018-09-04 15:54:11'),
(81, 'BLV010', 3, '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<ul>\r\n<li style=\"width: 595px; padding-right: 10px; padding-left: 10px; color: #666666; font-family: -apple-system, BlinkMacSystemFont, \'Segoe UI\', Roboto, \'Helvetica Neue\', Arial, sans-serif, \'Apple Color Emoji\', \'Segoe UI Emoji\', \'Segoe UI Symbol\'; font-size: 15px; background-color: #ffffff;\">K&iacute;ch thước: D120xR50xC75</li>\r\n<li style=\"width: 595px; padding-right: 10px; padding-left: 10px; color: #666666; font-family: -apple-system, BlinkMacSystemFont, \'Segoe UI\', Roboto, \'Helvetica Neue\', Arial, sans-serif, \'Apple Color Emoji\', \'Segoe UI Emoji\', \'Segoe UI Symbol\'; font-size: 15px; background-color: #ffffff;\">Ch&acirc;n sắt sơn tĩnh điện</li>\r\n<li style=\"width: 595px; padding-right: 10px; padding-left: 10px; color: #666666; font-family: -apple-system, BlinkMacSystemFont, \'Segoe UI\', Roboto, \'Helvetica Neue\', Arial, sans-serif, \'Apple Color Emoji\', \'Segoe UI Emoji\', \'Segoe UI Symbol\'; font-size: 15px; background-color: #ffffff;\">Mặt b&agrave;n gỗ</li>\r\n</ul>\r\n</body>\r\n</html>', 3000000, 'http://creatorvn.com/images/product/010.jpg', 220, 100, 0, '2001-01-01 00:00:00', 0, '2018-09-04 16:11:41'),
(82, 'Anh Thanh, quận Thủ Đức, TPHCM', 0, '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p>Gửi kh&aacute;ch h&igrave;nh thực tế bộ sofa v&agrave; b&agrave;n ăn&nbsp; mặt đ&aacute; thi&ecirc;n nhi&ecirc;n cao cấp m&agrave; Creator đ&atilde; l&agrave;m v&agrave; giao cho kh&aacute;ch h&agrave;ng. Cảm ơn anh đ&atilde; ủng hộ Creator</p>\r\n</body>\r\n</html>', 0, NULL, 0, 0, 0, NULL, 0, '2018-09-04 16:19:05'),
(83, 'Căn hộ tại quận 2, TPHCM', 0, '<!DOCTYPE html>\n<html>\n<head>\n</head>\n<body>\n<p>C&aacute;c sản phẩm Creator đ&atilde; l&agrave;m theo đơn đặt h&agrave;ng của một vị kh&aacute;ch nước ngo&agrave;i, đang sinh sống v&agrave; l&agrave;m việc tại quận 2, TPHCM</p>\n</body>\n</html>', 0, NULL, 0, 0, 0, NULL, 0, '2018-09-04 16:21:25'),
(84, 'Anh Long, quận 8, TPHCM', 0, '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p>Mẫu sofa v&agrave; b&agrave;n m&agrave; Creator đ&atilde; l&agrave;m v&agrave; giao cho anh Long v&agrave;o th&aacute;ng 7/2018. Chụp r&otilde; từng đường kim mũi chỉ để kh&aacute;ch c&oacute; thể so s&aacute;nh sự kh&aacute;c biết giữa&nbsp; sofa của Creator với c&aacute;c sofa tại nơi kh&aacute;c</p>\r\n</body>\r\n</html>', 0, NULL, 0, 0, 0, NULL, 0, '2018-09-04 16:34:58'),
(85, 'Chị Trâm (Buôn Ma Thuột)', 0, '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p><span style=\"color: #1d2129; font-family: Helvetica, Arial, sans-serif; background-color: #ffffff;\">Đ&oacute;ng g&oacute;i v&agrave; ship h&agrave;ng cho chị kh&aacute;ch ở Bu&ocirc;n Ma Thuột. Cảm ơn chị đ&atilde; tin tưởng v&agrave; đặt h&agrave;ng tại Xưởng nội thất Creator&nbsp;</span><span class=\"_47e3 _5mfr\" style=\"line-height: 0; vertical-align: middle; margin-right: 1px; margin-left: 1px; font-family: Helvetica, Arial, sans-serif; color: #1d2129; background-color: #ffffff;\" title=\"Biểu tượng cảm x&uacute;c heart\"><span class=\"_7oe\" style=\"display: inline; font-size: 0px; width: 0px; font-family: inherit;\" aria-hidden=\"true\">&lt;</span></span></p>\r\n</body>\r\n</html>', 0, NULL, 0, 0, 0, NULL, 0, '2018-09-13 14:27:57'),
(86, 'san pham 11111', 9, '<!DOCTYPE html>\n<html>\n<head>\n</head>\n<body>\n\n</body>\n</html>', 0, 'https://creatorvn.com/images/product/test.jpg', 0, 0, 0, '2001-01-01 00:00:00', 1, '2018-09-22 00:00:21'),
(87, 'san pham 11111', 9, '<!DOCTYPE html>\n<html>\n<head>\n</head>\n<body>\n\n</body>\n</html>', 10000, 'https://creatorvn.com/images/product/fitness_gym_reception_by_archjun-d5llgfp.jpg', 0, 0, 0, '2001-01-01 00:00:00', 1, '2018-09-22 00:02:44'),
(88, 'D001', 10, '<!DOCTYPE html>\n<html>\n<head>\n</head>\n<body>\n<ul>\n<li>K&iacute;ch thước: D45 x C45 (cm)</li>\n</ul>\n</body>\n</html>', 1200000, 'http://creatorvn.com/images/product/D001-3.png', 420, 330, 0, '2001-01-01 00:00:00', 0, '2018-09-22 06:47:33'),
(89, 'D002', 10, '<!DOCTYPE html>\n<html>\n<head>\n</head>\n<body>\n<ul>\n<li>K&iacute;ch thước: D60 x C45 (cm)</li>\n</ul>\n</body>\n</html>', 1200000, 'http://creatorvn.com/images/product/D002.png', 505, 301, 0, '2001-01-01 00:00:00', 0, '2018-09-22 06:49:30'),
(90, 'D003', 10, '<!DOCTYPE html>\n<html>\n<head>\n</head>\n<body>\n<ul>\n<li>K&iacute;ch thước: D60&nbsp;x R45 x C45 (cm)</li>\n</ul>\n</body>\n</html>', 1800000, 'http://creatorvn.com/images/product/D003-3.png', 550, 120, 0, '2001-01-01 00:00:00', 0, '2018-09-22 06:52:31'),
(91, 'D004', 10, '<!DOCTYPE html>\n<html>\n<head>\n</head>\n<body>\n<ul>\n<li>K&iacute;ch thước: D90 x R45 x C45 (cm)</li>\n</ul>\n</body>\n</html>', 2500000, 'http://creatorvn.com/images/product/D004-1.png', 450, 330, 0, '2001-01-01 00:00:00', 0, '2018-09-22 06:54:46'),
(92, 'D005', 10, '<!DOCTYPE html>\n<html>\n<head>\n</head>\n<body>\n<ul>\n<li>K&iacute;ch thước: D60 x C50 (cm)</li>\n</ul>\n</body>\n</html>', 1500000, 'http://creatorvn.com/images/product/D005.png', 410, 120, 0, '2001-01-01 00:00:00', 0, '2018-09-22 06:57:35'),
(93, 'D006', 10, '<!DOCTYPE html>\n<html>\n<head>\n</head>\n<body>\n<ul>\n<li>K&iacute;ch thước: D60 x C40 (cm) hoặc D45 x C45 (cm)</li>\n</ul>\n</body>\n</html>', 2800000, 'http://creatorvn.com/images/product/D006-1.png', 650, 520, 0, '2001-01-01 00:00:00', 0, '2018-09-22 06:59:22'),
(94, 'D007', 10, '<!DOCTYPE html>\n<html>\n<head>\n</head>\n<body>\n<ul>\n<li>K&iacute;ch thước: D60&nbsp; x C50 (cm)</li>\n</ul>\n</body>\n</html>', 1500000, 'http://creatorvn.com/images/product/D007.png', 230, 210, 0, '2001-01-01 00:00:00', 0, '2018-09-22 07:00:40'),
(95, 'D008', 10, '<!DOCTYPE html>\n<html>\n<head>\n</head>\n<body>\n<ul>\n<li>K&iacute;ch thước: D45 x R45 x C55 (cm)</li>\n</ul>\n</body>\n</html>', 1000000, 'http://creatorvn.com/images/product/D008.png', 340, 250, 0, '2001-01-01 00:00:00', 0, '2018-09-22 07:01:54'),
(96, 'D009', 10, '<!DOCTYPE html>\n<html>\n<head>\n</head>\n<body>\n<ul>\n<li>K&iacute;ch thước: D60 x C45 (cm) hoặc D50 x C45 (cm) hoặc D40 x C45 (cm)</li>\n</ul>\n</body>\n</html>', 1500000, 'http://creatorvn.com/images/product/D009.png', 430, 300, 0, '2001-01-01 00:00:00', 0, '2018-09-22 07:03:32'),
(97, 'D010', 10, '<!DOCTYPE html>\n<html>\n<head>\n</head>\n<body>\n<ul>\n<li>K&iacute;ch thước: D90 x R60 x C42 (cm)</li>\n</ul>\n</body>\n</html>', 2000000, 'http://creatorvn.com/images/product/D010.png', 750, 650, 0, '2001-01-01 00:00:00', 0, '2018-09-22 07:06:33'),
(98, 'D011', 10, '<!DOCTYPE html>\n<html>\n<head>\n</head>\n<body>\n<ul>\n<li>K&iacute;ch thước: D45 x C45 (cm)</li>\n</ul>\n</body>\n</html>', 3000000, 'http://creatorvn.com/images/product/D011-1.png', 750, 712, 0, '2001-01-01 00:00:00', 0, '2018-09-22 07:09:01'),
(99, 'GB027', 2, '<!DOCTYPE html>\n<html>\n<head>\n</head>\n<body>\n<ul>\n<li style=\"width: 595px; padding-right: 10px; padding-left: 10px; color: #666666; font-family: -apple-system, BlinkMacSystemFont, \'Segoe UI\', Roboto, \'Helvetica Neue\', Arial, sans-serif, \'Apple Color Emoji\', \'Segoe UI Emoji\', \'Segoe UI Symbol\'; font-size: 15px; background-color: #ffffff;\">K&iacute;ch thước: D240 x C70 x S90</li>\n<li style=\"width: 595px; padding-right: 10px; padding-left: 10px; color: #666666; font-family: -apple-system, BlinkMacSystemFont, \'Segoe UI\', Roboto, \'Helvetica Neue\', Arial, sans-serif, \'Apple Color Emoji\', \'Segoe UI Emoji\', \'Segoe UI Symbol\'; font-size: 15px; background-color: #ffffff;\">Khung gỗ dầu 100%</li>\n<li style=\"width: 595px; padding-right: 10px; padding-left: 10px; color: #666666; font-family: -apple-system, BlinkMacSystemFont, \'Segoe UI\', Roboto, \'Helvetica Neue\', Arial, sans-serif, \'Apple Color Emoji\', \'Segoe UI Emoji\', \'Segoe UI Symbol\'; font-size: 15px; background-color: #ffffff;\">Ch&acirc;n sắt</li>\n</ul>\n</body>\n</html>', 15500000, 'http://creatorvn.com/images/product/GB027.png', 550, 510, 0, '2001-01-01 00:00:00', 0, '2018-09-22 08:23:55'),
(100, 'GB028', 2, '<!DOCTYPE html>\n<html>\n<head>\n</head>\n<body>\n<ul>\n<li style=\"width: 595px; padding-right: 10px; padding-left: 10px; color: #666666; font-family: -apple-system, BlinkMacSystemFont, \'Segoe UI\', Roboto, \'Helvetica Neue\', Arial, sans-serif, \'Apple Color Emoji\', \'Segoe UI Emoji\', \'Segoe UI Symbol\'; font-size: 15px; background-color: #ffffff;\">K&iacute;ch thước: D240 x C70 x S90</li>\n<li style=\"width: 595px; padding-right: 10px; padding-left: 10px; color: #666666; font-family: -apple-system, BlinkMacSystemFont, \'Segoe UI\', Roboto, \'Helvetica Neue\', Arial, sans-serif, \'Apple Color Emoji\', \'Segoe UI Emoji\', \'Segoe UI Symbol\'; font-size: 15px; background-color: #ffffff;\">Khung gỗ dầu 100%</li>\n</ul>\n</body>\n</html>', 12000000, 'http://creatorvn.com/images/product/GB028.png', 870, 612, 0, '2001-01-01 00:00:00', 0, '2018-09-22 08:35:51'),
(101, 'GB029', 2, '<!DOCTYPE html>\n<html>\n<head>\n</head>\n<body>\n<ul>\n<li style=\"padding-right: 10px; padding-left: 10px; background-color: #ffffff; color: #666666; width: 595px; font-family: -apple-system, BlinkMacSystemFont, \'Segoe UI\', Roboto, \'Helvetica Neue\', Arial, sans-serif, \'Apple Color Emoji\', \'Segoe UI Emoji\', \'Segoe UI Symbol\'; font-size: 15px;\">K&iacute;ch thước: D240 x C70 x S90</li>\n<li style=\"padding-right: 10px; padding-left: 10px; background-color: #ffffff; color: #666666; width: 595px; font-family: -apple-system, BlinkMacSystemFont, \'Segoe UI\', Roboto, \'Helvetica Neue\', Arial, sans-serif, \'Apple Color Emoji\', \'Segoe UI Emoji\', \'Segoe UI Symbol\'; font-size: 15px;\">Khung gỗ dầu 100%</li>\n<li style=\"padding-right: 10px; padding-left: 10px; background-color: #ffffff; color: #666666; width: 595px; font-family: -apple-system, BlinkMacSystemFont, \'Segoe UI\', Roboto, \'Helvetica Neue\', Arial, sans-serif, \'Apple Color Emoji\', \'Segoe UI Emoji\', \'Segoe UI Symbol\'; font-size: 15px;\">Ch&acirc;n gỗ tự nhi&ecirc;n</li>\n</ul>\n</body>\n</html>', 13000000, 'http://creatorvn.com/images/product/GB029.png', 670, 600, 0, '2001-01-01 00:00:00', 0, '2018-09-22 08:37:45'),
(102, 'GB030', 2, '<!DOCTYPE html>\n<html>\n<head>\n</head>\n<body>\n<ul>\n<li style=\"padding-right: 10px; padding-left: 10px; background-color: #ffffff; color: #666666; width: 595px; font-family: -apple-system, BlinkMacSystemFont, \'Segoe UI\', Roboto, \'Helvetica Neue\', Arial, sans-serif, \'Apple Color Emoji\', \'Segoe UI Emoji\', \'Segoe UI Symbol\'; font-size: 15px;\">K&iacute;ch thước: D(&Oslash;)300 x C80 x S85</li>\n<li style=\"padding-right: 10px; padding-left: 10px; background-color: #ffffff; color: #666666; width: 595px; font-family: -apple-system, BlinkMacSystemFont, \'Segoe UI\', Roboto, \'Helvetica Neue\', Arial, sans-serif, \'Apple Color Emoji\', \'Segoe UI Emoji\', \'Segoe UI Symbol\'; font-size: 15px;\">Khung gỗ dầu 100%</li>\n</ul>\n</body>\n</html>', 16000000, 'http://creatorvn.com/images/product/GB030.png', 550, 400, 0, '2001-01-01 00:00:00', 0, '2018-09-22 08:42:22'),
(103, 'GB031', 2, '<!DOCTYPE html>\n<html>\n<head>\n</head>\n<body>\n<ul>\n<li style=\"width: 595px; padding-right: 10px; padding-left: 10px; color: #666666; font-family: -apple-system, BlinkMacSystemFont, \'Segoe UI\', Roboto, \'Helvetica Neue\', Arial, sans-serif, \'Apple Color Emoji\', \'Segoe UI Emoji\', \'Segoe UI Symbol\'; font-size: 15px; background-color: #ffffff;\">K&iacute;ch thước: D240 x C75 x S90</li>\n<li style=\"width: 595px; padding-right: 10px; padding-left: 10px; color: #666666; font-family: -apple-system, BlinkMacSystemFont, \'Segoe UI\', Roboto, \'Helvetica Neue\', Arial, sans-serif, \'Apple Color Emoji\', \'Segoe UI Emoji\', \'Segoe UI Symbol\'; font-size: 15px; background-color: #ffffff;\">Khung gỗ dầu 100%</li>\n</ul>\n</body>\n</html>', 14000000, 'http://creatorvn.com/images/product/GB031.png', 750, 550, 0, '2001-01-01 00:00:00', 0, '2018-09-22 08:45:00'),
(104, 'GB032', 2, '<!DOCTYPE html>\n<html>\n<head>\n</head>\n<body>\n<ul>\n<li style=\"width: 595px; padding-right: 10px; padding-left: 10px; color: #666666; font-family: -apple-system, BlinkMacSystemFont, \'Segoe UI\', Roboto, \'Helvetica Neue\', Arial, sans-serif, \'Apple Color Emoji\', \'Segoe UI Emoji\', \'Segoe UI Symbol\'; font-size: 15px; background-color: #ffffff;\">K&iacute;ch thước: D200 x C55 x S45</li>\n<li style=\"width: 595px; padding-right: 10px; padding-left: 10px; color: #666666; font-family: -apple-system, BlinkMacSystemFont, \'Segoe UI\', Roboto, \'Helvetica Neue\', Arial, sans-serif, \'Apple Color Emoji\', \'Segoe UI Emoji\', \'Segoe UI Symbol\'; font-size: 15px; background-color: #ffffff;\">Khung gỗ dầu 100%</li>\n</ul>\n</body>\n</html>', 4000000, 'http://creatorvn.com/images/product/GB032.png', 510, 430, 0, '2001-01-01 00:00:00', 0, '2018-09-22 08:46:58'),
(105, 'GB033', 2, '<!DOCTYPE html>\n<html>\n<head>\n</head>\n<body>\n<ul>\n<li style=\"width: 595px; padding-right: 10px; padding-left: 10px; color: #666666; font-family: -apple-system, BlinkMacSystemFont, \'Segoe UI\', Roboto, \'Helvetica Neue\', Arial, sans-serif, \'Apple Color Emoji\', \'Segoe UI Emoji\', \'Segoe UI Symbol\'; font-size: 15px; background-color: #ffffff;\">K&iacute;ch thước: D200 x C75 x S90</li>\n<li style=\"width: 595px; padding-right: 10px; padding-left: 10px; color: #666666; font-family: -apple-system, BlinkMacSystemFont, \'Segoe UI\', Roboto, \'Helvetica Neue\', Arial, sans-serif, \'Apple Color Emoji\', \'Segoe UI Emoji\', \'Segoe UI Symbol\'; font-size: 15px; background-color: #ffffff;\">Khung gỗ dầu 100%</li>\n</ul>\n</body>\n</html>', 9000000, 'http://creatorvn.com/images/product/GB033.png', 540, 350, 0, '2001-01-01 00:00:00', 0, '2018-09-22 08:51:06'),
(106, 'GB034', 2, '<!DOCTYPE html>\n<html>\n<head>\n</head>\n<body>\n<ul>\n<li style=\"padding-right: 10px; padding-left: 10px; background-color: #ffffff; color: #666666; width: 595px; font-family: -apple-system, BlinkMacSystemFont, \'Segoe UI\', Roboto, \'Helvetica Neue\', Arial, sans-serif, \'Apple Color Emoji\', \'Segoe UI Emoji\', \'Segoe UI Symbol\'; font-size: 15px;\">K&iacute;ch thước: D220 x C80 x S90</li>\n<li style=\"padding-right: 10px; padding-left: 10px; background-color: #ffffff; color: #666666; width: 595px; font-family: -apple-system, BlinkMacSystemFont, \'Segoe UI\', Roboto, \'Helvetica Neue\', Arial, sans-serif, \'Apple Color Emoji\', \'Segoe UI Emoji\', \'Segoe UI Symbol\'; font-size: 15px;\">Khung gỗ dầu 100%</li>\n</ul>\n</body>\n</html>', 12000000, 'http://creatorvn.com/images/product/GB034.png', 550, 360, 0, '2001-01-01 00:00:00', 0, '2018-09-22 08:52:09'),
(107, 'GB035', 2, '<!DOCTYPE html>\n<html>\n<head>\n</head>\n<body>\n<ul>\n<li style=\"padding-right: 10px; padding-left: 10px; background-color: #ffffff; color: #666666; width: 595px; font-family: -apple-system, BlinkMacSystemFont, \'Segoe UI\', Roboto, \'Helvetica Neue\', Arial, sans-serif, \'Apple Color Emoji\', \'Segoe UI Emoji\', \'Segoe UI Symbol\'; font-size: 15px;\">K&iacute;ch thước: D240 x C70 x S90</li>\n<li style=\"padding-right: 10px; padding-left: 10px; background-color: #ffffff; color: #666666; width: 595px; font-family: -apple-system, BlinkMacSystemFont, \'Segoe UI\', Roboto, \'Helvetica Neue\', Arial, sans-serif, \'Apple Color Emoji\', \'Segoe UI Emoji\', \'Segoe UI Symbol\'; font-size: 15px;\">Khung gỗ dầu 100%</li>\n</ul>\n</body>\n</html>', 14000000, 'http://creatorvn.com/images/product/GB035.png', 750, 700, 0, '2001-01-01 00:00:00', 0, '2018-09-22 08:53:47'),
(108, 'GB036', 2, '<!DOCTYPE html>\n<html>\n<head>\n</head>\n<body>\n<ul>\n<li style=\"padding-right: 10px; padding-left: 10px; background-color: #ffffff; color: #666666; width: 595px; font-family: -apple-system, BlinkMacSystemFont, \'Segoe UI\', Roboto, \'Helvetica Neue\', Arial, sans-serif, \'Apple Color Emoji\', \'Segoe UI Emoji\', \'Segoe UI Symbol\'; font-size: 15px;\">K&iacute;ch thước: D300 x C75 x S95</li>\n<li style=\"padding-right: 10px; padding-left: 10px; background-color: #ffffff; color: #666666; width: 595px; font-family: -apple-system, BlinkMacSystemFont, \'Segoe UI\', Roboto, \'Helvetica Neue\', Arial, sans-serif, \'Apple Color Emoji\', \'Segoe UI Emoji\', \'Segoe UI Symbol\'; font-size: 15px;\">Khung gỗ dầu 100%</li>\n</ul>\n</body>\n</html>', 20000000, 'http://creatorvn.com/images/product/GB036.png', 890, 790, 0, '2001-01-01 00:00:00', 0, '2018-09-22 08:54:37'),
(109, 'GB037', 2, '<!DOCTYPE html>\n<html>\n<head>\n</head>\n<body>\n<ul>\n<li style=\"padding-right: 10px; padding-left: 10px; background-color: #ffffff; color: #666666; width: 595px; font-family: -apple-system, BlinkMacSystemFont, \'Segoe UI\', Roboto, \'Helvetica Neue\', Arial, sans-serif, \'Apple Color Emoji\', \'Segoe UI Emoji\', \'Segoe UI Symbol\'; font-size: 15px;\">K&iacute;ch thước: D240 x C80 x S90</li>\n<li style=\"padding-right: 10px; padding-left: 10px; background-color: #ffffff; color: #666666; width: 595px; font-family: -apple-system, BlinkMacSystemFont, \'Segoe UI\', Roboto, \'Helvetica Neue\', Arial, sans-serif, \'Apple Color Emoji\', \'Segoe UI Emoji\', \'Segoe UI Symbol\'; font-size: 15px;\">Khung gỗ dầu 100%</li>\n</ul>\n</body>\n</html>', 14000000, 'http://creatorvn.com/images/product/GB037.png', 850, 550, 0, '2001-01-01 00:00:00', 0, '2018-09-22 08:57:22'),
(110, 'GB038', 2, '<!DOCTYPE html>\n<html>\n<head>\n</head>\n<body>\n<ul>\n<li style=\"padding-right: 10px; padding-left: 10px; background-color: #ffffff; color: #666666; width: 595px; font-family: -apple-system, BlinkMacSystemFont, \'Segoe UI\', Roboto, \'Helvetica Neue\', Arial, sans-serif, \'Apple Color Emoji\', \'Segoe UI Emoji\', \'Segoe UI Symbol\'; font-size: 15px;\">K&iacute;ch thước: D240 x C75 x S90</li>\n<li style=\"padding-right: 10px; padding-left: 10px; background-color: #ffffff; color: #666666; width: 595px; font-family: -apple-system, BlinkMacSystemFont, \'Segoe UI\', Roboto, \'Helvetica Neue\', Arial, sans-serif, \'Apple Color Emoji\', \'Segoe UI Emoji\', \'Segoe UI Symbol\'; font-size: 15px;\">Khung gỗ dầu 100%</li>\n</ul>\n</body>\n</html>', 12000000, 'http://creatorvn.com/images/product/GB038.png', 520, 320, 0, '2001-01-01 00:00:00', 0, '2018-09-22 08:58:18'),
(111, 'GB039', 2, '<!DOCTYPE html>\n<html>\n<head>\n</head>\n<body>\n<ul>\n<li style=\"padding-right: 10px; padding-left: 10px; background-color: #ffffff; color: #666666; width: 595px; font-family: -apple-system, BlinkMacSystemFont, \'Segoe UI\', Roboto, \'Helvetica Neue\', Arial, sans-serif, \'Apple Color Emoji\', \'Segoe UI Emoji\', \'Segoe UI Symbol\'; font-size: 15px;\">K&iacute;ch thước: D240 x C80 x S92</li>\n<li style=\"padding-right: 10px; padding-left: 10px; background-color: #ffffff; color: #666666; width: 595px; font-family: -apple-system, BlinkMacSystemFont, \'Segoe UI\', Roboto, \'Helvetica Neue\', Arial, sans-serif, \'Apple Color Emoji\', \'Segoe UI Emoji\', \'Segoe UI Symbol\'; font-size: 15px;\">Khung gỗ dầu 100%</li>\n</ul>\n</body>\n</html>', 15000000, 'http://creatorvn.com/images/product/GB039.png', 890, 680, 0, '2001-01-01 00:00:00', 0, '2018-09-22 09:01:40'),
(112, 'GB040', 2, '<!DOCTYPE html>\n<html>\n<head>\n</head>\n<body>\n<ul>\n<li style=\"padding-right: 10px; padding-left: 10px; background-color: #ffffff; color: #666666; width: 595px; font-family: -apple-system, BlinkMacSystemFont, \'Segoe UI\', Roboto, \'Helvetica Neue\', Arial, sans-serif, \'Apple Color Emoji\', \'Segoe UI Emoji\', \'Segoe UI Symbol\'; font-size: 15px;\">K&iacute;ch thước: D240 x C80 x S92</li>\n<li style=\"padding-right: 10px; padding-left: 10px; background-color: #ffffff; color: #666666; width: 595px; font-family: -apple-system, BlinkMacSystemFont, \'Segoe UI\', Roboto, \'Helvetica Neue\', Arial, sans-serif, \'Apple Color Emoji\', \'Segoe UI Emoji\', \'Segoe UI Symbol\'; font-size: 15px;\">Khung gỗ dầu 100%</li>\n</ul>\n</body>\n</html>', 14000000, 'http://creatorvn.com/images/product/GB040.png', 870, 670, 0, '2001-01-01 00:00:00', 0, '2018-09-22 09:02:22'),
(113, 'GB041', 2, '<!DOCTYPE html>\n<html>\n<head>\n</head>\n<body>\n<ul>\n<li style=\"padding-right: 10px; padding-left: 10px; background-color: #ffffff; color: #666666; width: 595px; font-family: -apple-system, BlinkMacSystemFont, \'Segoe UI\', Roboto, \'Helvetica Neue\', Arial, sans-serif, \'Apple Color Emoji\', \'Segoe UI Emoji\', \'Segoe UI Symbol\'; font-size: 15px;\">K&iacute;ch thước: D(270&amp;170) x C80 x S90</li>\n<li style=\"padding-right: 10px; padding-left: 10px; background-color: #ffffff; color: #666666; width: 595px; font-family: -apple-system, BlinkMacSystemFont, \'Segoe UI\', Roboto, \'Helvetica Neue\', Arial, sans-serif, \'Apple Color Emoji\', \'Segoe UI Emoji\', \'Segoe UI Symbol\'; font-size: 15px;\">Khung gỗ dầu 100%</li>\n</ul>\n</body>\n</html>', 20000000, 'http://creatorvn.com/images/product/GB041.png', 987, 876, 0, '2001-01-01 00:00:00', 0, '2018-09-22 09:04:08');
INSERT INTO `sanpham` (`id`, `tensp`, `maloaisp`, `dacta`, `gia`, `hinh`, `soluongviews`, `soluongban`, `giamgia`, `ngaykt`, `isdeleted`, `created`) VALUES
(114, 'GL011', 4, '<!DOCTYPE html>\n<html>\n<head>\n</head>\n<body>\n<ul>\n<li style=\"width: 595px; padding-right: 10px; padding-left: 10px; color: #666666; font-family: -apple-system, BlinkMacSystemFont, \'Segoe UI\', Roboto, \'Helvetica Neue\', Arial, sans-serif, \'Apple Color Emoji\', \'Segoe UI Emoji\', \'Segoe UI Symbol\'; font-size: 15px; background-color: #ffffff;\">K&iacute;ch thước: D(300x170) x C80x S90</li>\n<li style=\"width: 595px; padding-right: 10px; padding-left: 10px; color: #666666; font-family: -apple-system, BlinkMacSystemFont, \'Segoe UI\', Roboto, \'Helvetica Neue\', Arial, sans-serif, \'Apple Color Emoji\', \'Segoe UI Emoji\', \'Segoe UI Symbol\'; font-size: 15px; background-color: #ffffff;\">Khung gỗ dầu 100%</li>\n<li style=\"width: 595px; padding-right: 10px; padding-left: 10px; color: #666666; font-family: -apple-system, BlinkMacSystemFont, \'Segoe UI\', Roboto, \'Helvetica Neue\', Arial, sans-serif, \'Apple Color Emoji\', \'Segoe UI Emoji\', \'Segoe UI Symbol\'; font-size: 15px; background-color: #ffffff;\">Gi&aacute; Đ&ocirc;n: 2,500,000</li>\n</ul>\n</body>\n</html>', 15000000, 'http://creatorvn.com/images/product/GL011.png', 980, 800, 0, '2001-01-01 00:00:00', 0, '2018-09-22 14:25:23'),
(115, 'GL012', 4, '<!DOCTYPE html>\n<html>\n<head>\n</head>\n<body>\n<ul style=\"margin-bottom: 10px; color: #333333; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; white-space: nowrap; background-color: #f9f9f9;\">\n<li style=\"width: 595px; padding-right: 10px; padding-left: 10px; color: #666666; font-family: -apple-system, BlinkMacSystemFont, \'Segoe UI\', Roboto, \'Helvetica Neue\', Arial, sans-serif, \'Apple Color Emoji\', \'Segoe UI Emoji\', \'Segoe UI Symbol\'; font-size: 15px; background-color: #ffffff;\">K&iacute;ch thước: D(300x350) x C85x S92</li>\n<li style=\"width: 595px; padding-right: 10px; padding-left: 10px; color: #666666; font-family: -apple-system, BlinkMacSystemFont, \'Segoe UI\', Roboto, \'Helvetica Neue\', Arial, sans-serif, \'Apple Color Emoji\', \'Segoe UI Emoji\', \'Segoe UI Symbol\'; font-size: 15px; background-color: #ffffff;\">Khung gỗ dầu 100%</li>\n</ul>\n</body>\n</html>', 26000000, 'http://creatorvn.com/images/product/GL012.png', 870, 650, 0, '2001-01-01 00:00:00', 0, '2018-09-22 14:30:35'),
(116, 'GL013', 4, '<!DOCTYPE html>\n<html>\n<head>\n</head>\n<body>\n<ul style=\"margin-bottom: 10px; color: #333333; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; white-space: nowrap; background-color: #f9f9f9;\">\n<li style=\"width: 595px; padding-right: 10px; padding-left: 10px; color: #666666; font-family: -apple-system, BlinkMacSystemFont, \'Segoe UI\', Roboto, \'Helvetica Neue\', Arial, sans-serif, \'Apple Color Emoji\', \'Segoe UI Emoji\', \'Segoe UI Symbol\'; font-size: 15px; background-color: #ffffff;\">K&iacute;ch thước: D(300x170) x C85x S92</li>\n<li style=\"width: 595px; padding-right: 10px; padding-left: 10px; color: #666666; font-family: -apple-system, BlinkMacSystemFont, \'Segoe UI\', Roboto, \'Helvetica Neue\', Arial, sans-serif, \'Apple Color Emoji\', \'Segoe UI Emoji\', \'Segoe UI Symbol\'; font-size: 15px; background-color: #ffffff;\">Khung gỗ dầu 100%</li>\n</ul>\n</body>\n</html>', 14000000, 'http://creatorvn.com/images/product/GL013.png', 990, 870, 0, '2001-01-01 00:00:00', 0, '2018-09-22 14:32:12'),
(117, 'GL014', 4, '<!DOCTYPE html>\n<html>\n<head>\n</head>\n<body>\n<ul style=\"margin-bottom: 10px; color: #333333; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; white-space: nowrap; background-color: #f9f9f9;\">\n<li style=\"width: 595px; padding-right: 10px; padding-left: 10px; color: #666666; font-family: -apple-system, BlinkMacSystemFont, \'Segoe UI\', Roboto, \'Helvetica Neue\', Arial, sans-serif, \'Apple Color Emoji\', \'Segoe UI Emoji\', \'Segoe UI Symbol\'; font-size: 15px; background-color: #ffffff;\">K&iacute;ch thước: D(300x170) x C85x S92</li>\n<li style=\"width: 595px; padding-right: 10px; padding-left: 10px; color: #666666; font-family: -apple-system, BlinkMacSystemFont, \'Segoe UI\', Roboto, \'Helvetica Neue\', Arial, sans-serif, \'Apple Color Emoji\', \'Segoe UI Emoji\', \'Segoe UI Symbol\'; font-size: 15px; background-color: #ffffff;\">Khung gỗ dầu 100%</li>\n</ul>\n</body>\n</html>', 18000000, 'http://creatorvn.com/images/product/GL014.png', 850, 590, 0, '2001-01-01 00:00:00', 0, '2018-09-22 14:33:48'),
(118, 'GL015', 4, '<!DOCTYPE html>\n<html>\n<head>\n</head>\n<body>\n<ul>\n<li style=\"width: 595px; padding-right: 10px; padding-left: 10px; font-size: 15px; white-space: nowrap; color: #666666; font-family: -apple-system, BlinkMacSystemFont, \'Segoe UI\', Roboto, \'Helvetica Neue\', Arial, sans-serif, \'Apple Color Emoji\', \'Segoe UI Emoji\', \'Segoe UI Symbol\'; background-color: #ffffff;\">K&iacute;ch thước: D(300x350) x C85x S92</li>\n<li style=\"width: 595px; padding-right: 10px; padding-left: 10px; font-size: 15px; white-space: nowrap; color: #666666; font-family: -apple-system, BlinkMacSystemFont, \'Segoe UI\', Roboto, \'Helvetica Neue\', Arial, sans-serif, \'Apple Color Emoji\', \'Segoe UI Emoji\', \'Segoe UI Symbol\'; background-color: #ffffff;\">Khung gỗ dầu 100%</li>\n</ul>\n</body>\n</html>', 27000000, 'http://creatorvn.com/images/product/GL015.png', 990, 890, 0, '2001-01-01 00:00:00', 0, '2018-09-22 14:37:02'),
(119, 'GL016', 4, '<!DOCTYPE html>\n<html>\n<head>\n</head>\n<body>\n<ul>\n<li style=\"width: 595px; padding-right: 10px; padding-left: 10px; font-size: 15px; white-space: nowrap; color: #666666; font-family: -apple-system, BlinkMacSystemFont, \'Segoe UI\', Roboto, \'Helvetica Neue\', Arial, sans-serif, \'Apple Color Emoji\', \'Segoe UI Emoji\', \'Segoe UI Symbol\'; background-color: #ffffff;\">K&iacute;ch thước: D(280x180) x C85x S92</li>\n<li style=\"width: 595px; padding-right: 10px; padding-left: 10px; font-size: 15px; white-space: nowrap; color: #666666; font-family: -apple-system, BlinkMacSystemFont, \'Segoe UI\', Roboto, \'Helvetica Neue\', Arial, sans-serif, \'Apple Color Emoji\', \'Segoe UI Emoji\', \'Segoe UI Symbol\'; background-color: #ffffff;\">Khung gỗ dầu 100%</li>\n</ul>\n</body>\n</html>', 18000000, 'http://creatorvn.com/images/product/GL016.png', 870, 745, 0, '2001-01-01 00:00:00', 1, '2018-09-22 14:39:37'),
(120, 'GL017', 4, '<!DOCTYPE html>\n<html>\n<head>\n</head>\n<body>\n<ul>\n<li style=\"width: 595px; padding-right: 10px; padding-left: 10px; font-size: 15px; white-space: nowrap; color: #666666; font-family: -apple-system, BlinkMacSystemFont, \'Segoe UI\', Roboto, \'Helvetica Neue\', Arial, sans-serif, \'Apple Color Emoji\', \'Segoe UI Emoji\', \'Segoe UI Symbol\'; background-color: #ffffff;\">K&iacute;ch thước: D(270x170) x C85x S90</li>\n<li style=\"width: 595px; padding-right: 10px; padding-left: 10px; font-size: 15px; white-space: nowrap; color: #666666; font-family: -apple-system, BlinkMacSystemFont, \'Segoe UI\', Roboto, \'Helvetica Neue\', Arial, sans-serif, \'Apple Color Emoji\', \'Segoe UI Emoji\', \'Segoe UI Symbol\'; background-color: #ffffff;\">Khung gỗ dầu 100%</li>\n</ul>\n</body>\n</html>', 14000000, 'http://creatorvn.com/images/product/GL017.png', 850, 810, 0, '2001-01-01 00:00:00', 0, '2018-09-22 14:40:43'),
(121, 'GL018', 4, '<!DOCTYPE html>\n<html>\n<head>\n</head>\n<body>\n<ul>\n<li style=\"width: 595px; padding-right: 10px; padding-left: 10px; font-size: 15px; white-space: nowrap; color: #666666; font-family: -apple-system, BlinkMacSystemFont, \'Segoe UI\', Roboto, \'Helvetica Neue\', Arial, sans-serif, \'Apple Color Emoji\', \'Segoe UI Emoji\', \'Segoe UI Symbol\'; background-color: #ffffff;\">K&iacute;ch thước: D(250x250) x C90 x S92</li>\n<li style=\"width: 595px; padding-right: 10px; padding-left: 10px; font-size: 15px; white-space: nowrap; color: #666666; font-family: -apple-system, BlinkMacSystemFont, \'Segoe UI\', Roboto, \'Helvetica Neue\', Arial, sans-serif, \'Apple Color Emoji\', \'Segoe UI Emoji\', \'Segoe UI Symbol\'; background-color: #ffffff;\">Khung gỗ dầu 100%</li>\n</ul>\n</body>\n</html>', 22000000, 'http://creatorvn.com/images/product/GL018.png', 821, 741, 0, '2001-01-01 00:00:00', 0, '2018-09-22 14:42:39'),
(122, 'GL019', 4, '<!DOCTYPE html>\n<html>\n<head>\n</head>\n<body>\n<ul>\n<li style=\"width: 595px; padding-right: 10px; padding-left: 10px; font-size: 15px; white-space: nowrap; color: #666666; font-family: -apple-system, BlinkMacSystemFont, \'Segoe UI\', Roboto, \'Helvetica Neue\', Arial, sans-serif, \'Apple Color Emoji\', \'Segoe UI Emoji\', \'Segoe UI Symbol\'; background-color: #ffffff;\">K&iacute;ch thước: D(250x250) x C90 x S92</li>\n<li style=\"width: 595px; padding-right: 10px; padding-left: 10px; font-size: 15px; white-space: nowrap; color: #666666; font-family: -apple-system, BlinkMacSystemFont, \'Segoe UI\', Roboto, \'Helvetica Neue\', Arial, sans-serif, \'Apple Color Emoji\', \'Segoe UI Emoji\', \'Segoe UI Symbol\'; background-color: #ffffff;\">Khung gỗ dầu 100%</li>\n</ul>\n</body>\n</html>', 22000000, 'http://creatorvn.com/images/product/GL019.png', 888, 778, 0, '2001-01-01 00:00:00', 0, '2018-09-22 14:44:13'),
(123, 'GL020', 4, '<!DOCTYPE html>\n<html>\n<head>\n</head>\n<body>\n<ul>\n<li style=\"width: 595px; padding-right: 10px; padding-left: 10px; font-size: 15px; white-space: nowrap; color: #666666; font-family: -apple-system, BlinkMacSystemFont, \'Segoe UI\', Roboto, \'Helvetica Neue\', Arial, sans-serif, \'Apple Color Emoji\', \'Segoe UI Emoji\', \'Segoe UI Symbol\'; background-color: #ffffff;\">K&iacute;ch thước: D160 x R100 x C60</li>\n<li style=\"width: 595px; padding-right: 10px; padding-left: 10px; font-size: 15px; white-space: nowrap; color: #666666; font-family: -apple-system, BlinkMacSystemFont, \'Segoe UI\', Roboto, \'Helvetica Neue\', Arial, sans-serif, \'Apple Color Emoji\', \'Segoe UI Emoji\', \'Segoe UI Symbol\'; background-color: #ffffff;\">Khung gỗ dầu 100%</li>\n</ul>\n</body>\n</html>', 6000000, 'http://creatorvn.com/images/product/GL020.png', 897, 798, 0, '2001-01-01 00:00:00', 0, '2018-09-22 14:47:06'),
(124, 'GL021', 4, '<!DOCTYPE html>\n<html>\n<head>\n</head>\n<body>\n<ul>\n<li style=\"width: 595px; padding-right: 10px; padding-left: 10px; font-size: 15px; white-space: nowrap; color: #666666; font-family: -apple-system, BlinkMacSystemFont, \'Segoe UI\', Roboto, \'Helvetica Neue\', Arial, sans-serif, \'Apple Color Emoji\', \'Segoe UI Emoji\', \'Segoe UI Symbol\'; background-color: #ffffff;\">K&iacute;ch thước: D180 x S100 x C60</li>\n<li style=\"width: 595px; padding-right: 10px; padding-left: 10px; font-size: 15px; white-space: nowrap; color: #666666; font-family: -apple-system, BlinkMacSystemFont, \'Segoe UI\', Roboto, \'Helvetica Neue\', Arial, sans-serif, \'Apple Color Emoji\', \'Segoe UI Emoji\', \'Segoe UI Symbol\'; background-color: #ffffff;\">Khung gỗ dầu 100%</li>\n</ul>\n</body>\n</html>', 6000000, 'http://creatorvn.com/images/product/GL021.png', 670, 570, 0, '2001-01-01 00:00:00', 0, '2018-09-22 14:50:44'),
(125, 'GL022', 4, '<!DOCTYPE html>\n<html>\n<head>\n</head>\n<body>\n<ul>\n<li style=\"width: 595px; padding-right: 10px; padding-left: 10px; font-size: 15px; white-space: nowrap; color: #666666; font-family: -apple-system, BlinkMacSystemFont, \'Segoe UI\', Roboto, \'Helvetica Neue\', Arial, sans-serif, \'Apple Color Emoji\', \'Segoe UI Emoji\', \'Segoe UI Symbol\'; background-color: #ffffff;\">K&iacute;ch thước: D(200x300) x C90 x S95</li>\n<li style=\"width: 595px; padding-right: 10px; padding-left: 10px; font-size: 15px; white-space: nowrap; color: #666666; font-family: -apple-system, BlinkMacSystemFont, \'Segoe UI\', Roboto, \'Helvetica Neue\', Arial, sans-serif, \'Apple Color Emoji\', \'Segoe UI Emoji\', \'Segoe UI Symbol\'; background-color: #ffffff;\">Khung gỗ dầu 100%</li>\n</ul>\n</body>\n</html>', 30000000, 'http://creatorvn.com/images/product/GL022.png', 990, 897, 0, '2001-01-01 00:00:00', 0, '2018-09-22 14:52:03'),
(126, 'GL023', 4, '<!DOCTYPE html>\n<html>\n<head>\n</head>\n<body>\n<ul>\n<li style=\"width: 595px; padding-right: 10px; padding-left: 10px; font-size: 15px; white-space: nowrap; color: #666666; font-family: -apple-system, BlinkMacSystemFont, \'Segoe UI\', Roboto, \'Helvetica Neue\', Arial, sans-serif, \'Apple Color Emoji\', \'Segoe UI Emoji\', \'Segoe UI Symbol\'; background-color: #ffffff;\">K&iacute;ch thước: D(270x170) x C80x S90</li>\n<li style=\"width: 595px; padding-right: 10px; padding-left: 10px; font-size: 15px; white-space: nowrap; color: #666666; font-family: -apple-system, BlinkMacSystemFont, \'Segoe UI\', Roboto, \'Helvetica Neue\', Arial, sans-serif, \'Apple Color Emoji\', \'Segoe UI Emoji\', \'Segoe UI Symbol\'; background-color: #ffffff;\">Khung gỗ dầu 100%</li>\n</ul>\n</body>\n</html>', 16000000, 'http://creatorvn.com/images/product/GL023.png', 890, 850, 0, '2001-01-01 00:00:00', 0, '2018-09-22 14:53:45'),
(127, 'AC001', 11, '<!DOCTYPE html>\n<html>\n<head>\n</head>\n<body>\n<ul>\n<li style=\"width: 595px; padding-right: 10px; padding-left: 10px; font-size: 15px; white-space: nowrap; color: #666666; font-family: -apple-system, BlinkMacSystemFont, \'Segoe UI\', Roboto, \'Helvetica Neue\', Arial, sans-serif, \'Apple Color Emoji\', \'Segoe UI Emoji\', \'Segoe UI Symbol\'; background-color: #ffffff;\">K&iacute;ch thước: D100 x R80 x C70</li>\n<li style=\"width: 595px; padding-right: 10px; padding-left: 10px; font-size: 15px; white-space: nowrap; color: #666666; font-family: -apple-system, BlinkMacSystemFont, \'Segoe UI\', Roboto, \'Helvetica Neue\', Arial, sans-serif, \'Apple Color Emoji\', \'Segoe UI Emoji\', \'Segoe UI Symbol\'; background-color: #ffffff;\">Khung gỗ dầu 100%</li>\n</ul>\n</body>\n</html>', 7500000, 'http://creatorvn.com/images/product/AC001-1.png', 869, 696, 0, '2001-01-01 00:00:00', 0, '2018-09-22 15:37:45'),
(128, 'AC003', 11, '<!DOCTYPE html>\n<html>\n<head>\n</head>\n<body>\n<ul>\n<li>Khung gỗ dầu 100%</li>\n<li>Ch&acirc;n sắt sơn tĩnh điện</li>\n</ul>\n</body>\n</html>', 8000000, 'http://creatorvn.com/images/product/AC003.png', 875, 775, 0, '2001-01-01 00:00:00', 0, '2018-09-23 15:51:21'),
(129, 'AC004', 11, '<!DOCTYPE html>\n<html>\n<head>\n</head>\n<body>\n<ul>\n<li>Ghế: D90 x S90 x C95 (cm)</li>\n<li>Đ&ocirc;n: D45 x C40 (cm) - Gi&aacute;: 2,800,000</li>\n</ul>\n</body>\n</html>', 8500000, 'http://creatorvn.com/images/product/AC004.png', 1000, 900, 0, '2001-01-01 00:00:00', 0, '2018-09-23 15:58:43');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `taikhoan`
--

CREATE TABLE `taikhoan` (
  `id` int(11) NOT NULL,
  `tenhienthi` char(25) CHARACTER SET latin1 NOT NULL,
  `matkhau` char(25) CHARACTER SET latin1 NOT NULL,
  `vaitro` int(4) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `taikhoan`
--

INSERT INTO `taikhoan` (`id`, `tenhienthi`, `matkhau`, `vaitro`) VALUES
(1, 'admin', 'creatorvn2018', 1),
(2, 'user', 'creatorvn2018', 0);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `theme`
--

CREATE TABLE `theme` (
  `id` int(11) NOT NULL,
  `hinh` varchar(255) DEFAULT NULL,
  `noidung` text,
  `hienthi` int(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `theme`
--

INSERT INTO `theme` (`id`, `hinh`, `noidung`, `hienthi`) VALUES
(1, 'https://creatorvn.com/images/theme/1512123455-Ban-ghe-sofa-go-phong-khach-BGF011.jpg', '<!DOCTYPE html> <html> <head> </head> <body> <div class=\"text-inner text-center\"> <blockquote> <p style=\"text-align: center;\"><span style=\"color: #ffcc00;\">HIỆN THỰC KH&Ocirc;NG GIAN SỐNG MƠ ƯỚC CỦA BẠN</span></p> </blockquote> <h1 class=\"uppercase\" style=\"text-align: center;\"><strong>Sản xuất &amp; thi c&ocirc;ng&nbsp;<span style=\"color: #ffcc00;\">nội thất</span></strong></h1> <p class=\"lead\" style=\"text-align: center;\">Nghi&ecirc;n cứu, lựa chọn nguy&ecirc;n vật liệu v&agrave; đưa ra sự tư vấn tốt nhất cho từng đối tượng kh&aacute;ch h&agrave;ng</p> </div> </body> </html>', 1),
(2, 'https://creatorvn.com/images/theme/19.jpg', '<!DOCTYPE html> <html> <head> </head> <body> <div class=\"text-inner text-center\"> <blockquote> <p style=\"text-align: center;\"><span style=\"color: #ffcc00;\">HIỆN THỰC KH&Ocirc;NG GIAN SỐNG MƠ ƯỚC CỦA BẠN</span></p> </blockquote> <h1 class=\"uppercase\" style=\"text-align: center;\"><strong>Cung cấp &amp; thi c&ocirc;ng&nbsp;<span style=\"color: #ffcc00;\">c&aacute;c loại đ&aacute; hoa cương</span></strong></h1> <p class=\"lead\" style=\"text-align: center;\">Theo xu hướng thiết kế tối giản v&agrave; tinh tế c&ugrave;ng với đ&oacute; l&agrave; thiết kế v&agrave; tạo ra theo &yacute; tưởng của kh&aacute;ch h&agrave;ng.</p> </div> </body> </html>', 1),
(3, 'http://creatorvn.com/images/theme/13.jpg', '<!DOCTYPE html> <html> <head> </head> <body> <div class=\"text-inner text-center\"> <blockquote> <p style=\"text-align: center;\"><span style=\"color: #ffcc00;\">HIỆN THỰC KH&Ocirc;NG GIAN SỐNG MƠ ƯỚC CỦA BẠN</span></p> </blockquote> <h1 class=\"uppercase\" style=\"text-align: center;\"><strong>Đ&oacute;ng mới &amp; bọc nệm&nbsp;<span style=\"color: #ffcc00;\">ghế sofa, xe hơi</span></strong></h1> <p class=\"lead\" style=\"text-align: center;\">Nghi&ecirc;n cứu, lựa chọn nguy&ecirc;n vật liệu v&agrave; đưa ra sự tư vấn tốt nhất cho từng đối tượng kh&aacute;ch h&agrave;ng</p> </div> </body> </html>', 1),
(4, 'https://creatorvn.com/images/theme/15.jpg', '<!DOCTYPE html> <html> <head> </head> <body> <div class=\"text-inner text-center\"> <blockquote> <p style=\"text-align: center;\"><span style=\"color: #ffcc00;\">HIỆN THỰC KH&Ocirc;NG GIAN SỐNG MƠ ƯỚC CỦA BẠN</span></p> </blockquote> <h1 class=\"uppercase\" style=\"text-align: center;\"><strong>Cung cấp tranh trang tr&iacute;&nbsp;<span style=\"color: #ffcc00;\">theo y&ecirc;u cầu</span></strong></h1> <p class=\"lead\" style=\"text-align: center;\">Nghi&ecirc;n cứu, lựa chọn nguy&ecirc;n vật liệu v&agrave; đưa ra sự tư vấn tốt nhất cho từng đối tượng kh&aacute;ch h&agrave;ng</p> </div> </body> </html>', 1),
(5, 'https://creatorvn.com/images/theme/8.jpg', '<!DOCTYPE html> <html> <head> </head> <body> <div class=\"text-inner text-center\"> <blockquote> <p style=\"text-align: center;\"><span style=\"color: #ffcc00;\">HIỆN THỰC KH&Ocirc;NG GIAN SỐNG MƠ ƯỚC CỦA BẠN</span></p> </blockquote> <h1 class=\"uppercase\" style=\"text-align: center;\"><strong>Cung cấp thảm&nbsp;<span style=\"color: #ffcc00;\">nhập khẩu cao cấp</span></strong></h1> <p class=\"lead\" style=\"text-align: center;\">Nghi&ecirc;n cứu, lựa chọn nguy&ecirc;n vật liệu v&agrave; đưa ra sự tư vấn tốt nhất cho từng đối tượng kh&aacute;ch h&agrave;ng.</p> </div> </body> </html>', 1);

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `baiviet`
--
ALTER TABLE `baiviet`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `chitiethoadon`
--
ALTER TABLE `chitiethoadon`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mahd` (`mahd`),
  ADD KEY `chitiethoadon_ibfk_1` (`masp`);

--
-- Chỉ mục cho bảng `haumai`
--
ALTER TABLE `haumai`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `hinh`
--
ALTER TABLE `hinh`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `hoadon`
--
ALTER TABLE `hoadon`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `loaisanpham`
--
ALTER TABLE `loaisanpham`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `sanpham`
--
ALTER TABLE `sanpham`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sanpham_ibfk_1` (`maloaisp`);

--
-- Chỉ mục cho bảng `taikhoan`
--
ALTER TABLE `taikhoan`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `tenhienthi` (`tenhienthi`) USING HASH;

--
-- Chỉ mục cho bảng `theme`
--
ALTER TABLE `theme`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `baiviet`
--
ALTER TABLE `baiviet`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT cho bảng `chitiethoadon`
--
ALTER TABLE `chitiethoadon`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT cho bảng `haumai`
--
ALTER TABLE `haumai`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `hinh`
--
ALTER TABLE `hinh`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=302;

--
-- AUTO_INCREMENT cho bảng `hoadon`
--
ALTER TABLE `hoadon`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT cho bảng `loaisanpham`
--
ALTER TABLE `loaisanpham`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT cho bảng `sanpham`
--
ALTER TABLE `sanpham`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=130;

--
-- AUTO_INCREMENT cho bảng `taikhoan`
--
ALTER TABLE `taikhoan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT cho bảng `theme`
--
ALTER TABLE `theme`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
